<?php
/**
 * Поиск объявлений: список (phone)
 * @var $this BBS
 * @var $list_type integer тип списка BBS::LIST_TYPE_
 * @var $items array объявления
 * @var $showBanners boolean выводить баннеры в списке
 * @var $mapBlock boolean отрисовывать блок карты или только список
 * @var $mapVertical boolean вертикальный вид отображения карты
 */

$lng_fav_in = _te('bbs', 'Добавить в избранное');
$lng_fav_out = _te('bbs', 'Удалить из избранного');
$lng_quick = _t('bbs', 'срочно');

if($list_type == BBS::LIST_TYPE_LIST || $list_type == BBS::LIST_TYPE_GALLERY) {

    echo $this->viewPHP($aData, 'search.list.desktop');

} else if($list_type == BBS::LIST_TYPE_MAP ) {
    if (!BBS::filterVertical()) { ?>
    <? if ($mapBlock) { ?>
    <div class="sr-page__map sr-page__map_mobile visible-phone">
        <div class="sr-page__map_ymap span12">
            <div class="j-search-map-phone" style="height: 300px; width: 100%;"></div>
        </div>
    </div>
    <div class="sr-page__list sr-page__list_mobile j-maplist visible-phone">
    <? } ?>
    <? foreach($items as &$v) { ?>
    <div class="sr-page__list__item<? if($v['svc_marked']){ ?> selected<? } ?>">
        <table>
            <tr>
                <td colspan="2" class="sr-page__list__item_descr">
                    <div class="sr-page__list__item_title"><? if($v['svc_quick']) { ?><span class="label label-warning quickly"><?= $lng_quick ?></span>&nbsp;<? } ?><a href="<?= $v['link'] ?>"><?= $v['title'] ?></a></div>
                    <? if($v['fav']) { ?>
                    <a href="javascript:void(0);" class="item-fav active j-i-fav" data="{id:<?= $v['id'] ?>}" title="<?= $lng_fav_out ?>"><span class="item-fav__star"><i class="fa fa-star j-i-fav-icon"></i></span></a>
                    <? } else { ?>
                    <a href="javascript:void(0);" class="item-fav j-i-fav" data="{id:<?= $v['id'] ?>}" title="<?= $lng_fav_in ?>"><span class="item-fav__star"><i class="fa fa-star-o j-i-fav-icon"></i></span></a>
                    <? } ?>
                </td>
            </tr>
            <tr>
                <td class="sr-page__list__item_date"><?= $v['publicated'] ?></td>
                <td class="sr-page__list__item_price">
                    <? if($v['price_on']) { ?>
                        <?if ($v['price']) { ?><strong><?= $v['price'] ?></strong><? } ?>
                        <?if ($v['price_mod']) { ?><small><?= $v['price_mod'] ?></small><? } ?>
                    <? } ?>
                </td>
            </tr>
        </table>
    </div>
    <? } unset($v); ?>
    <? if ($mapBlock) { ?>
    </div>
    <? } ?>
<? } else { ?>
    <? if ($mapBlock) { ?>
        <div class="visible-phone sr-page__map_ymap j-map">
            <div style="height: 300px; width: 100%;" class="j-search-map-phone"></div>
        </div>
        <div class="visible-phone j-list-<?= bff::DEVICE_PHONE ?>"><div class="j-maplist">
    <? } ?>
    <div class="sr-page__list sr-page__list_mobile visible-phone">
        <? foreach($items as &$v) { ?>
        <div class="sr-page__list__item<? if($v['svc_marked']){ ?> selected<? } ?>">
            <table>
                <tr>
                    <td colspan="2" class="sr-page__list__item_descr">
                        <div class="sr-page__list__item_title"><? if($v['svc_quick']) { ?><span class="label label-warning quickly"><?= $lng_quick ?></span>&nbsp;<? } ?><a href="<?= $v['link'] ?>"><?= $v['title'] ?></a></div>
                        <? if($v['fav']) { ?>
                        <a href="javascript:void(0);" class="item-fav active j-i-fav" data="{id:<?= $v['id'] ?>}" title="<?= $lng_fav_out ?>"><span class="item-fav__star"><i class="fa fa-star j-i-fav-icon"></i></span></a>
                        <? } else { ?>
                        <a href="javascript:void(0);" class="item-fav j-i-fav" data="{id:<?= $v['id'] ?>}" title="<?= $lng_fav_in ?>"><span class="item-fav__star"><i class="fa fa-star-o j-i-fav-icon"></i></span></a>
                        <? } ?>
                    </td>
                </tr>
                <tr>
                    <td class="sr-page__list__item_date"><?= $v['publicated'] ?></td>
                    <td class="sr-page__list__item_price">
                        <? if($v['price_on']) { ?>
                            <?if ($v['price']) { ?><strong><?= $v['price'] ?></strong><? } ?>
                            <?if ($v['price_mod']) { ?><small><?= $v['price_mod'] ?></small><? } ?>
                        <? } ?>
                    </td>
                </tr>
            </table>
        </div>
        <? } unset($v); ?>
    </div>
    <? if ($mapBlock) { ?></div></div><? } ?>
<? } }