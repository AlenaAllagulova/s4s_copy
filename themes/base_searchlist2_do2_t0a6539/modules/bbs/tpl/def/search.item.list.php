<?php
/**
 * Список объявлений: вид строчный список
 * @var $this BBS
 * @var $item array данные объявления
 * @var $opts array доп. параметры
 */
 $lng_quick = _te('bbs', 'срочно');

 \HTML::attributeAdd($opts['attr'], 'class', 'sr-page__item');
 if ($item['svc_marked']) {
     \HTML::attributeAdd($opts['attr'], 'class', 'sr-page__item_selected');
 }

 $address = [];
 if ( ! empty($item['city_title'])) {
    $address[] = $item['city_title'];
 }
 if ( ! empty($item['district_title'])) {
    $address[] = $item['district_title'];
 }
 if ( ! empty($opts['showAddr']) && ! empty($item['addr_addr'])) {
    $address[] = $item['addr_addr'];
 }

?>
<div<?= \HTML::attributes($opts['attr']); ?>>
    <div class="sr-page__item__in">

        <div class="sr-page__item_l">
            <?php if ($item['imgs']): # image ?>
            <a class="sr-page__item__l__in" href="<?= $item['link'] ?>">
                <span class="sr-page__item__l__pic">
                    <?php if ($item['svc_quick']) { ?>
                    <span class="label-lot label_top hidden-phone"><?= $lng_quick ?></span>
                    <?php } ?>
                    <img src="<?= $item['img_m'] ?>" alt="" />
                    <?php if ($item['imgs'] > 1) { ?>
                    <span class="sr-page__item__pic__counter">
                        <?= $item['imgs'] ?>
                    </span>
                    <?php } ?>
                </span>
            </a>
            <?php else: ?>
            <a class="sr-page__item__l__in sr-page__item__l__in_empty__pic" href="<?= $item['link'] ?>">
                <?php if ($item['svc_quick']) { ?>
                    <span class="label-lot label_top hidden-phone"><?= $lng_quick ?></span>
                <?php } ?>
                <img src="<?= $item['img_m:svg'] ?>" alt="" />
            </a>
            <?php endif; ?>
        </div>

        <div class="sr-page__item_r">
            <div class="sr-page__item__body">
                <div class="sr-page__item__category_text">
                    <?php if ($item['svc_quick']) { ?>
                    <span class="label label-warning quickly visible-phone"><?= $lng_quick ?></span>
                    <?php } ?>
                    <?php if ($item['svc_fixed']) { ?>
                    <span class="sr-page__item__top__label"><?= _te('bbs', 'топ') ?></span>
                    <?php } ?>
                    <span><?= $item['cat_title'] ?></span>
                </div>
                <div class="sr-page__item__title">
                    <a href="<?= $item['link'] ?>"><?= $item['title'] ?></a>
                </div>
                <?php if ($item['price_on']): ?>
                <div class="sr-page__item__price__wrap">
                    <div class="sr-page__item__price">
                        <strong><?= $item['price'] ?></strong>
                    </div>
                    <span class="sr-page__item__bagrain grey"><?= $item['price_mod'] ?></span>
                </div>
                <?php endif; ?>
                <?php if ( ! empty($item['descr_list'])): ?>
                <div class="sr-page__item__desc">
                    <?= $item['descr_list'] ?>
                </div>
                <?php endif; ?>
                <div class="sr-page__item__bottom">
                    <?php if ( ! empty($address)): ?>
                    <div class="sr-page__item__address">
                        <?= join(', ', $address) ?>
                    </div>
                    <?php endif; ?>
                    <div class="sr-page__item__date">
                        <?php if ($item['publicated_up']): ?>
                            <span class="ajax-ico grey">
                                <i class="fa fa-refresh"></i>
                                <span class="ajax-in j-tooltip" data-toggle="tooltip" data-container="body" data-placement="bottom" data-html="true" data-original-title="<div class='text-left'><?= _te('search', 'Обновлено: [date]', ['date'=>$item['publicated_last']]); ?></div> <div class='text-left'><?= _te('search', 'Размещено: [date]', ['date'=>$item['publicated']]); ?></div>"><?= $item['publicated_last'] ?></span>
                            </span>
                        <?php else: ?>
                            <span class="grey"><?= $item['publicated'] ?></span>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="sr-page__item__fav">
            <a href="javascript:void(0);" class="item-fav j-tooltip j-i-fav <?php if ($item['fav']) { ?>active<?php } ?>" data="{id:<?= $item['id'] ?>}" data-toggle="tooltip" data-container="body" data-placement="top" data-original-title="<?= ( $item['fav'] ? _te('bbs', 'Удалить из избранного') : _te('bbs', 'Добавить в избранное') ); ?>">
                <i class="fa <?php if ($item['fav']) { ?>fa-star<?php } else { ?>fa-star-o<?php } ?> j-i-fav-icon"></i>
            </a>
        </div>
    </div>
</div>

<div class="spacer"></div>