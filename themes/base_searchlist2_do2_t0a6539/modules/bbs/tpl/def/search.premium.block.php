<?php
/**
 * Блок премиум объявлений
 * @var $this BBS
 * @var $items array объявления
 */
?>

<div class="sr-vip sr-vip-v2 hidden-phone" id="j-bbs-search-premium-block">
    <div class="sr-vip__title"><?= _t('search', 'Премиум объявления'); ?></div>
    <div class="sr-vip__content">

        <?php foreach($items as $item): ?>
        <div class="sr-page__gallery__item sr-page__gallery__item__v2 rel">
            <div class="sr-page__gallery__item__v2__in">
                <div class="sr-page__item__fav">
                    <a href="javascript:void(0);" class="item-fav j-tooltip j-i-fav <?php if ($item['fav']) { ?>active<?php } ?>" data="{id:<?= $item['id'] ?>}" data-toggle="tooltip" data-placement="top" data-original-title="<?= ( $item['fav'] ? _te('bbs', 'Удалить из избранного') : _te('bbs', 'Добавить в избранное') ); ?>">
                        <i class="fa <?php if ($item['fav']) { ?>fa-star<?php } else { ?>fa-star-o<?php } ?> j-i-fav-icon"></i>
                    </a>
                </div>

                <?php if ($item['imgs']): # image ?>
                <a class="sr-page__item__l__in" href="<?= $item['link'] ?>">
                    <span class="sr-page__item__l__pic">
                        <?php if($item['svc_quick']) { ?>
                            <span class="label-lot label_top hidden-phone"><?= _te('bbs', 'срочно') ?></span>
                        <?php } ?>
                        <img src="<?= $item['img_m'] ?>" alt="" />
                        <?php if ($item['imgs'] > 1) { ?>
                        <span class="sr-page__item__pic__counter">
                            <?= $item['imgs'] ?>
                        </span>
                        <?php } ?>
                    </span>
                </a>
                <?php else: ?>
                <a class="sr-page__item__l__in sr-page__item__l__in_empty__pic" href="<?= $item['link'] ?>">
                    <?php if($item['svc_quick']) { ?>
                        <span class="label-lot label_top hidden-phone"><?= _te('bbs', 'срочно') ?></span>
                    <?php } ?>
                    <img src="<?= $item['img_m:svg'] ?>" alt="" />
                </a>
                <?php endif; ?>

                <div class="sr-page__gallery__item_descr">
                    <div class="sr-page__gallery__item_title">
                        <a href="<?= $item['link'] ?>"><?= $item['title'] ?></a>
                    </div>
                    <?php if ($item['price_on']): ?>
                    <p class="sr-page__gallery__item_price">
                        <strong><?= $item['price'] ?></strong>
                        <small><?= $item['price_mod'] ?></small>
                    </p>
                    <?php endif; ?>
                    <div class="sr-page__bottom-info">
                        <?php if ( ! empty($item['city_title'])): ?>
                        <p class="sr-page__item__address">
                            <small>
                                <?= $item['city_title'] ?><?= ! empty($item['district_title']) ? ', '.$item['district_title'] : '' ?>
                            </small>
                        </p>
                        <?php endif; ?>
                        <?php if( ! empty($item['descr_list'])): ?>
                        <div class="sr-page__item__desc extra__content">
                            <p><?= $item['descr_list'] ?></p>
                        </div>
                        <?php endif; ?>
                        <div class="sr-page__item__date extra__content">
                            <?php if ($item['publicated_up']): ?>
                                <span class="ajax-ico grey">
                                    <i class="fa fa-refresh"></i>
                                    <span class="ajax-in j-tooltip" data-toggle="tooltip" data-container="body" data-placement="bottom" data-html="true" data-original-title="<div class='text-left'><?= _te('search', 'Обновлено: [date]', ['date'=>$item['publicated_last']]); ?></div> <div class='text-left'><?= _te('search', 'Размещено: [date]', ['date'=>$item['publicated']]); ?></div>"><?= $item['publicated_last'] ?></span>
                                </span>
                            <?php else: ?>
                                <span class="grey"><?= $item['publicated'] ?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>

        <script>
            <?php js::start(); ?>
            $(function(){
                var premiumBlock = $('#j-bbs-search-premium-block');
                var premiumElems = premiumBlock.find('.sr-page__gallery__item__v2');

                function calcHeight() {
                    premiumElems.each(function (i, el) {
                        var h = $(el).find('.sr-page__gallery__item__v2__in').outerHeight();
                        if (h > 0) {
                            $(el).css('height', h + 'px');}
                    });
                }
                calcHeight();

                premiumBlock.find('.sr-page__gallery__item__v2 img').on('load', function() {
                    calcHeight();
                });
            });
            <?php js::stop(); ?>
        </script>

    </div>
    <div class="sr-vip__footer">
        <a class="small-text" href="<?= Site::url('services') ?>"><?= _te('svc', 'Что такое Премиум объявления?'); ?></a>
    </div>
</div>