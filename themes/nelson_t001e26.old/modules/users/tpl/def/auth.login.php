<?php
/**
 * Авторизация
 * @var $this Users
 * @var $back string URL страницы возврата (после успешной авторизации)
 * @var $providers array провайдеры авторизации через соц. сети
 */
?>
<div class="container_sm">
  <div class="l-content-box">
    <div class="l-content-box-cols">
      <div class="l-content-box-left">
        <div class="l-content-box-in">
          <form class="form-horizontal" id="j-u-login-form" action="">
            <input type="hidden" name="back" value="<?= HTML::escape($back) ?>" />
            <div class="form-group">
              <label class="col-md-3 col-sm-4 control-label" for="j-u-login-email"><?= _t('users', 'Электронная почта') ?><span class="required-mark">*</span></label>
              <div class="col-md-6 col-sm-8">
                <input type="email" name="email" class="form-control" id="j-u-login-email" placeholder="<?= _te('users', 'Введите ваш email') ?>" maxlength="100" autocorrect="off" autocapitalize="off" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 col-sm-4 control-label" for="j-u-login-pass"><?= _t('users', 'Пароль') ?><span class="required-mark">*</span></label>
              <div class="col-md-6 col-sm-8">
                <input type="password" name="pass" class="form-control" id="j-u-login-pass" placeholder="<?= _te('users', 'Введите ваш пароль') ?>" maxlength="100" />
              </div>
            </div>
            <? if(Users::loginRemember()) { ?>
            <div class="form-group">
              <div class="col-md-offset-3 col-sm-offset-4 col-md-9 col-sm-8">
                <div class="checkbox">
                  <label>
                    <input type="checkbox" name="remember"/> <?= _t('users', 'Запомнить меня'); ?>
                  </label>
                </div>
              </div>
            </div>
            <? } ?>
            <div class="row">
              <div class="col-md-offset-3 col-sm-offset-4 col-md-9 col-sm-8">
                <button type="submit" class="btn btn-success j-submit"><?= _t('users', 'Войти на сайт') ?></button>
              </div>
            </div>
          </form>
        </div>
        <?php if ($providers) { ?>
        <div class="l-content-box-in l-content-box-in_md text-center">
          <? foreach($providers as $v) { ?><a href="#" class="btn btn-sm btn-social btn-<?= $v['class'] ?> j-u-login-social-btn" data="{provider:'<?= $v['key'] ?>',w:<?= $v['w'] ?>,h:<?= $v['h'] ?>}"><?= $v['title'] ?></a><? } ?>
        </div>
        <?php } ?>
      </div>
      <div class="l-content-box-sidebar">
        <div class="l-content-box-in l-content-box-in_md text-center">
          <div class="l-content-box-title">
            <?= _t('users', 'Впервые на нашем сайте?') ?>
          </div>
          <a href="<?= Users::url('register') ?>" class="btn btn-primary"><?= _t('users', 'Зарегистрируйтесь') ?></a>
        </div>
        <div class="l-content-box-in l-content-box-in_md text-center">
          <div class="l-content-box-title">
            <?= _t('users', 'Вы забыли свой пароль?') ?>
          </div>
          <a href="<?= Users::url('forgot') ?>" class="btn btn-default"><?= _t('users', 'Восстановить пароль') ?></a>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  <? js::start(); ?>
  $(function(){
    jUserAuth.login(<?= func::php2js(array(
      'login_social_url' => Users::url('login.social'),
      'login_social_return' => $back,
      'lang' => array(
        'email' => _t('users', 'E-mail адрес указан некорректно'),
        'pass' => _t('users', 'Укажите пароль'),
        ),
      )) ?>);
  });
  <? js::stop(); ?>
</script>