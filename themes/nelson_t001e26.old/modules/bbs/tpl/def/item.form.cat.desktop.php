<?php
/**
 * Форма объявления: добавление / редактирование - выбор категории (desktop)
 * @var $this BBS
 * @var $step integer шаг выбора
 * @var $cats array категории
 * @var $parent array данные о категории выше (для шага №2)
 */
?>
<? if($step == 1) { # ШАГ 1 ?>
<ul class="dropdown-menu-list">
  <? foreach($cats as $v){ ?>
  <li>
    <a href="#" class="j-main" data="{id:<?= $v['id'] ?>,pid:<?= $v['pid'] ?>,subs:<?= $v['subs'] ?>,title:'<?= HTML::escape($v['t'], 'js') ?>'}">
      <span class="dropdown-menu-ico">
        <img src="<?= $v['i'] ?>" alt="<?= $v['t'] ?>" />
      </span>
      <span class="l-categories-items-i-name"><?= $v['t'] ?></span>
    </a>
  </li>
  <? } ?>
</ul>
<? } else if($step == 2) { # ШАГ 2 ?>
<div>
  <div class="dropdown-menu-heading">
    <div class="dropdown-menu-heading-container">
      <div class="dropdown-menu-heading-container-img">
        <a href="#" class="j-back" data="{prev:<?= $parent['pid'] ?>}"><img src="<?= $parent['icon'] ?>" alt="" /></a>
      </div>
      <div class="dropdown-menu-heading-container-content">
        <span>
          <a href="#" class="link-ajax j-back" data="{prev:<?= $parent['pid'] ?>}">&laquo; <span><?= ( $parent['main'] ? _t('item-form','Вернуться к основным категориям') : _t('item-form','Вернуться назад') ) ?></span></a>
        </span>
        <div class="dropdown-menu-heading-title"><?= $parent['title'] ?></div>
      </div>
    </div>
  </div>
  <div class="l-categories-list-wrapper">
    <? if($showAll) { array_unshift($cats, array('id'=>$parent['id'],'pid'=>$parent['pid'], 'subs'=>0, 't'=>_t('bbs','Все подкатегории'), 'active'=>false)); } ?>
      <?
      $cats = ( sizeof($cats) > 6 ? array_chunk($cats, round( sizeof($cats) / 2 ) ) : array($cats) );
      foreach($cats as $catsChunk):
        ?><div class="l-categories-list-wrapper-in"><ul class="dropdown-menu-list"><?
      foreach($catsChunk as $v):
        ?><li><a href="#" class="j-sub<? if($v['active']) { ?> active<? } ?>" data="{id:<?= $v['id'] ?>,pid:<?= $parent['id'] ?>,subs:<?= $v['subs'] ?>,title:'<?= HTML::escape($v['t'], 'js') ?>'}"><span><?= $v['t'] ?></span><? if($v['subs']) { ?> &raquo;<? } ?></a></li><?
      endforeach; ?>
    </ul></div>
  <?  endforeach; ?>
  </div>
</div>
<? } ?>