<?php
/**
 * Кабинет пользователя: Избранные (объявления)
 * @var $this BBS
 * @var $empty boolean список пустой
 * @var $items array объявления
 * @var $cats array категории
 * @var $cat_active array текущая категория
 * @var $f array параметры фильтра
 * @var $pgn string постраничная навигация (HTML)
 * @var $pgn_pp array варианты кол-ва на страницу
 * @var $total integer всего объявлений
 * @var $device string текущее устройство bff::DEVICE_
 */
if ($empty) {
  ?>
  <div class="alert alert-info">
    <?= _t('bbs', 'Список избранных объявлений пустой') ?>.<br>
    <?= _t('bbs', 'Перейти на просмотр <a [link_search]>всех объявлений</a>', array('link_search' => 'href="' . BBS::url('items.search') . '"')); ?>
  </div>
  <?php
  return;
} else {
  tpl::includeJS('history', true);
  tpl::includeJS('bbs.items', false);
}
?>
<? if (!User::id()) { ?>
  <div class="l-pageHeading">
    <h1 class="l-pageHeading-title">
      <?= _t('bbs', 'Избранные объявления') ?>
    </h1>
  </div>
<? } ?>

<form action="" id="j-my-favs-form">
  <input type="hidden" name="c" value="<?= $f['c'] ?>" id="j-my-favs-cat-value"/>
  <input type="hidden" name="lt" value="<?= $f['lt'] ?>"/>
  <input type="hidden" name="page" value="<?= $f['page'] ?>"/>
  <input type="hidden" name="pp" value="<?= $f['pp'] ?>" id="j-my-favs-pp-value"/>

  <!-- List Filter -->
  <div class="usr-content-top">
    <div class="usr-content-top-main">
      <ul class="usr-content-top-nav" id="j-my-favs-cat">
        <li class="dropdown">
          <a class="dropdown-toggle j-cat-dropdown" data-toggle="dropdown" href="javascript:void(0);">
            <b class="j-cat-title"><?= $cat_active['title'] ?></b>
            <i class="fa fa-caret-down"></i>
          </a>
          <ul class="dropdown-menu">
            <? foreach ($cats as $v) {
              if (empty($v['sub'])) {
                ?>
                <li><a href="#" data-value="<?= $v['id'] ?>" class="j-cat-option"><?= $v['title'] ?></a></li><?
              } else {
                ?>
                <li class="dropdown-menu-nav-header"><?= $v['title'] ?></li><?
                foreach ($v['sub'] as $vv) {
                  ?>
                  <li><a href="#" data-value="<?= $vv['id'] ?>" class="j-cat-option"><?= $vv['title'] ?></a></li><?
                }
              }
            } ?>
          </ul>
        </li>
      </ul>
    </div>
    <div class="usr-content-top-right">
      <a href="#" class="btn btn-default j-cleanup">
        <i class="fa fa-times"></i> <?= _t('bbs', 'Очистить избранное') ?>
      </a>
    </div>
  </div>

  <!-- Listings -->
  <div class="j-list">
    <div class="j-list-<?= bff::DEVICE_DESKTOP ?> j-list-<?= bff::DEVICE_TABLET ?> j-list-<?= bff::DEVICE_PHONE ?>">
      <?
      echo $this->searchList(bff::DEVICE_DESKTOP, $f['lt'], $items);
      ?>
    </div>
  </div>

  <!-- Pagination -->
  <div class="usr-pagination">
    <? if ($total > 15) { ?>
      <div id="j-my-favs-pp" class="usr-pagination-dropdown dropdown<?= (!$total ? ' hide' : '') ?>">
        <a class="btn btn-default j-pp-dropdown" data-toggle="dropdown" href="#">
          <span class="j-pp-title"><?= $pgn_pp[$f['pp']]['t'] ?></span>
          <b class="caret"></b>
        </a>
        <ul class="dropdown-menu pull-right">
          <? foreach ($pgn_pp as $k => $v): ?>
            <li><a href="#" class="<? if ($k == $f['pp']) { ?>active <? } ?>j-pp-option"
                   data-value="<?= $k ?>"><?= $v['t'] ?></a></li>
          <? endforeach; ?>
        </ul>
      </div>
    <? } ?>
    <div id="j-my-favs-pgn">
      <?= $pgn ?>
    </div>
  </div>

</form>

<script type="text/javascript">
  <?php js::start(); ?>
  $(function () {
    jBBSItemsList.init({
      list: '#j-my-favs-form',
      pgn: '#j-my-favs-pgn',
      pp: '#j-my-favs-pp',
      lang: {},
      ajax: true,
      onInit: function () {
        var self = this;
        self.form.on('click', '.j-cleanup', function (e) {
          nothing(e);
          bff.ajax(self.listMngr.getURL(), {act: 'cleanup', hash: app.csrf_token}, function (r, err) {
            if (r && r.success) {
              location.reload();
            } else {
              app.alert.error(err);
            }
          });
          return false;
        });
      }
    });
  });
  <?php js::stop(); ?>
</script>