<?php
/**
 * Список объявлений: вид карта
 * @var $this BBS
 * @var $item array данные объявления
 * @var $index $index порядковый номер в списке
 */
?>
<div class="sr-list-item sr-list-item-map<?php if ($item['svc_marked']){ ?> selected<?php } ?> j-maplist-item" data-index="<?= $index ?>">
  <div class="sr-list-item-date c-date">
    <?= $item['publicated'] ?>
  </div>
  <?php if ($item['fav']) { ?>
  <a href="javascript:void(0);" class="btn c-fav sr-list-item-fav active j-i-fav" data="{id:<?= $item['id'] ?>}" title="<?= _te('bbs', 'Удалить из избранного') ?>"><i class="fa fa-star j-i-fav-icon"></i></a>
  <?php } else { ?>
  <a href="javascript:void(0);" class="btn c-fav sr-list-item-fav j-i-fav" data="{id:<?= $item['id'] ?>}" title="<?= _te('bbs', 'Добавить в избранное') ?>"><i class="fa fa-star j-i-fav-icon"></i></a>
  <?php } ?>
  <div class="sr-list-item-in">
    <? if($item['svc_quick']) { ?><span class="sr-glItem-label label-md label-urgent"><?= _t('bbs', 'срочно') ?></span><? } ?>
    <span class="sr-list-item-img">
      <a href="<?= $item['link'] ?>" title="<?= $item['title'] ?>" class="sr-glItem-img<? if($item['imgs'] > 1) { ?> sr-glItem-img_multiple<? } ?>">
        <img src="<?= $item['img_s'] ?>" alt="<?= $item['title'] ?>" />
      </a>
    </span>
    <span class="sr-list-item-content">
      <span class="sr-list-item-heading">
        <span class="sr-list-item-heading-title"><a href="<?= $item['link'] ?>" title="<?= $item['title'] ?>"><?= $item['title'] ?></a></span>
      </span>
      <span class="sr-glItem-subtext">
        <span class="sr-glItem-subtext-i"><?= $item['cat_title'] ?></span>
      </span>
      <span class="sr-list-item-region">
        <? if( ! empty($item['city_title'])): ?><i class="fa fa-map-marker"></i> <?= $item['city_title'] ?><?= ! empty($item['district_title']) ? ', '.$item['district_title'] : ''?><? endif; ?>
      </span>
    </span>
    <? if($item['price_on']) { ?>
    <span class="list-item-right">
      <span class="c-price sr-list-item-price">
        <?= $item['price'] ?>
      </span>
      <span class="c-price-sub"><?= $item['price_mod'] ?></span>
    </span>
    <? } ?>
  </div>
</div>