<?php
/**
 * Поиск объявлений: фильтр категорий (desktop, tablet)
 * @var $this BBS
 * @var $step integer шаг
 * @var $cats array категории
 * @var $parent array данные о категории выше (шаг №2)
 * @var $total integer общее кол-во объявлений (шаг №1)
 */
?>
<? if($step == 1) { # STEP 1 ?>
<div class="l-categories">
  <div class="dropdown-menu-heading">
    <div class="dropdown-menu-heading-title"><?= _t('filter','Выберите категорию') ?></div>
  </div>
    <ul class="dropdown-menu-list">
      <li>
        <span class="hidden-link" data-link="<?= BBS::url('items.search') ?>" data="{id:0,pid:0,title:'<?= HTML::escape(_t('filter','Все категории'), 'js') ?>'}">
          <?= _t('filter','смотреть все объявления') ?>
          <span class="dropdown-menu-count">(<?= number_format($total, 0, '.', ' ') ?>)</span>
        </span>
      </li>
      <? foreach($cats as $v){ ?>
      <li>
        <span data-link="<?= $v['l'] ?>" class="hidden-link j-main" data="{id:<?= $v['id'] ?>,subs:<?= $v['subs'] ?>,title:'<?= HTML::escape($v['t'], 'js') ?>'}">
          <span class="dropdown-menu-ico">
            <img src="<?= $v['i'] ?>" alt="<?= $v['t'] ?>" />
          </span>
          <span class="l-categories-items-i-name"><?= $v['t'] ?></span>
        </span>
      </li>
      <? } ?>
    </ul>
</div>
<? } else if($step == 2) { # STEP 2 ?>
<div class="l-categories">
  <div class="dropdown-menu-heading">
    <div class="dropdown-menu-heading-container">
      <div class="dropdown-menu-heading-container-content">
        <span>
          <? if( $parent['main'] ) { ?>
          <a href="#" class="link-ajax j-back" data="{prev:0}">
            &laquo; <span><?= _t('filter','Вернуться назад') ?></span>
          </a>
          <? } else { ?>
          <a href="#" class="link-ajax j-back" data="{prev:<?= $parent['pid'] ?>}">&laquo; <span><?= _t('filter','Вернуться назад') ?></span></a>
          <? } ?>
        </span>
        <div class="dropdown-menu-heading-title"><?= $parent['title'] ?></div>
        <span>
            <?php if ($parent['items'] > 0) { ?>
            <a href="<?= $parent['link'] ?>" class="j-f-cat-desktop-step2-parent"><?= number_format($parent['items'], 0, '.', ' ') ?></a>&nbsp;<?= tpl::declension($parent['items'], _t('filter','объявление;объявления;объявлений'), false) ?>
            <?php } else { ?>
            <span data-link="<?= $parent['link'] ?>" class="hidden-link j-f-cat-desktop-step2-parent"><?= number_format($parent['items'], 0, '.', ' ') ?></span>&nbsp;<?= tpl::declension($parent['items'], _t('filter','объявление;объявления;объявлений'), false) ?>
            <?php } ?>
        </span>
      </div>
    </div>
  </div>
  <div class="l-categories-list-wrapper">
    <?
      $cols = 1; $colsClass = 12; $catsTotal = sizeof($cats);
      foreach (array(24=>1,25=>2,60=>3,300=>4) as $k=>$v) {
        if ($catsTotal<=$k) { $cols = $v; $colsClass = (12 / $v); break; }
      }
      $cats = ( $cols > 1 ? array_chunk($cats, ceil( $catsTotal / $cols ) ) : array($cats) );
      foreach($cats as $catsChunk):
        ?><div class="l-categories-list-wrapper-in"><ul class="dropdown-menu-list"><?
      foreach($catsChunk as $v):
        ?><li><a href="<?= $v['l'] ?>" class="j-sub<? if($v['active']) { ?> active<? } ?>" data="{id:<?= $v['id'] ?>,pid:<?= $parent['id'] ?>,subs:<?= $v['subs'] ?>,lvl:<?= $v['lvl'] ?>,title:'<?= HTML::escape($v['t'], 'js') ?>'}"><span class="cat-name"><?= $v['t'] ?></span><? if($v['subs']) { ?> &raquo;<? } ?></a></li><?
      endforeach; ?>
    </ul></div>
    <?  endforeach; ?>
  </div>
</div>
<? } ?>