<?php
/**
 * Поиск магазинов: layout
 * @var $this Shops
 * @var $listTypes array типы списка
 * @var $isMap boolean текущий тип списка - карта
 * @var $items array список магазинов
 * @var $f array параметры фильтра
 * @var $cat array параметры текущей категории
 * @var $show_open_link boolean отображать ссылку "открыть магазин"
 */

tpl::includeJS(array('history'), true);

extract($f, EXTR_REFS | EXTR_PREFIX_ALL, 'f'); # параметры фильтра => переменные с префиксом f_

if ($isMap) {
  Geo::mapsAPI(false);
  if (Geo::mapsType() == Geo::MAPS_TYPE_GOOGLE) {
    tpl::includeJS('markerclusterer/markerclusterer', false);
  }
}

?>
<?= tpl::getBreadcrumbs($cat['crumbs'], false, 'breadcrumb'); ?>

<div class="l-content l-content_sm">
      <div class="container">
        <div class="l-mainLayout">

      <!-- Content -->
      <div class="l-mainLayout-content<? if(DEVICE_DESKTOP_OR_TABLET && ! $isMap && ($bannerRight = Banners::view('shops_search_right')) ) { ?> has-sidebar<? } ?>">
        <div id="j-shops-search-list">
          <div class="l-pageHeading">
            <h1 class="l-pageHeading-title"><?= ( $f_c > 0 ? $cat['titleh1'] : ( ! empty($f_q) ? _t('shops', 'Результаты поиска по запросу "[query]"', array('query'=>$f_q)) : (!empty($cat['titleh1']) ? $cat['titleh1'] : _t('shops', 'Все магазины')) ) ) ?></h1>
          </div>
          <div class="sr-listTop">
            <?php
            // List View
            if( ! empty($items) ) { ?>
            <div class="sr-listTop-item" id="j-f-listtype">
              <? foreach($listTypes as $k=>$v) {
                ?><a href="javascript:void(0);" data="{id:<?= $k ?>}" data-id="<?= $k ?>" class="sr-listTop-btn j-type<? if($v['a']){ ?> active<? } ?>"><i class="<?= $v['i'] ?>"></i> <span class="visible-lg-inline"><?= $v['t'] ?></span></a><?
              } ?>
            </div>
            <?php } ?>
          </div>
          
          <!-- Search Results -->
          <div class="j-list-<?= bff::DEVICE_DESKTOP ?> j-list-<?= bff::DEVICE_TABLET ?>">
            <?= $this->searchList(bff::DEVICE_DESKTOP, $f_lt, $items, $num_start); ?>
          </div>
          
          <!-- Pagination -->
          <div id="j-shops-search-pgn">
            <?= $pgn ?>
          </div>

        </div><!-- /#j-shops-search-list -->

      </div><!-- /.l-mainLayout-content -->

      <? if(DEVICE_DESKTOP_OR_TABLET && ! $isMap && ($bannerRight = Banners::view('shops_search_right')) ) { ?>
      <!-- Sidebar -->
      <div class="l-mainLayout-sidebar">
        <? if($show_open_link): ?>
          <div class="l-mainLayout-sidebar-item">
            <a href="<?= Shops::url('my.open') ?>" class="btn btn-block btn-info">
              <i class="icon-plus icon-white"></i> <?= _t('shops', 'Открыть магазин'); ?>
            </a>
          </div>
        <? endif; ?>
        <div class="l-banner-v">
          <?= $bannerRight ?>
        </div>
      </div>
      <? } ?>

    </div><!-- /.l-mainLayout -->
  </div><!-- /.container -->
</div><!-- /.l-content -->

<div class="l-seoText">
  <? if($f['page'] <= 1 && isset($cat['seotext'])) echo $cat['seotext'] ?>
</div>

<script type="text/javascript">
  <? js::start(); ?>
  <?
  if ($isMap) {
    foreach($items as &$v) { unset($v['descr']); } unset($v);
  } else {
    $items = array();
  }
  ?>
  $(function(){
    jShopsSearch.init(<?= func::php2js(array(
      'lang'=>array(
        'map_toggle_open' => _t('shops', 'больше карты'),
        'map_toggle_close' => _t('shops', 'меньше карты'),
        'map_content_loading' => _t('shops', 'Загрузка, подождите...'),
        'map_show_items' => _t('shops', 'Показать объявления'),
        ),
      'listtype' => $listTypes,
      'items'    => $items,
      'defaultCoords' => Geo::mapDefaultCoords(true),
      'ajax'     => false,
      )) ?>);
  });
  <? js::stop(); ?>
</script>
<?

# актуализируем данные формы поиска
# формируемой позже в фаблоне /tpl/filter.php
$this->searchFormData($f);