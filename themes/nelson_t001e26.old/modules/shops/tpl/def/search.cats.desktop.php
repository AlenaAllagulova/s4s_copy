<?php
/**
 * Поиск магазинов: фильтр категорий (desktop, tablet)
 * @var $this Shops
 * @var $step integer шаг
 * @var $cats array категории
 * @var $parent array данные о категории выше (шаг №2)
 * @var $total integer общее кол-во магазинов (шаг №1)
 */

$lang_shops = _t('shops','магазин;магазина;магазинов');
?>
<? if($step == 1) { # ШАГ 1 ?>
<div class="l-categories">
  <div class="dropdown-menu-heading">
    <div class="dropdown-menu-heading-title"><?= _t('shops','Выберите категорию') ?></div>
  </div>
    <ul class="dropdown-menu-list">
      <li>
        <a href="<?= Shops::url('search') ?>" class="j-all" data="{id:0,pid:0,title:'<?= HTML::escape(_t('shops','Все категории'), 'js') ?>'}">
          <?= _t('filter','смотреть все магазины') ?>
          <span class="dropdown-menu-count">(<? echo ( $total>=1000 ? number_format($total, 0, '', ' ') : $total), '&nbsp;', tpl::declension($total, $lang_shops, false) ?>)</span>
        </a>
      </li>
      <? foreach($cats as $v){ ?>
      <li>
        <a href="<?= $v['l'] ?>" class="j-main" data="{id:<?= $v['id'] ?>,subs:<?= $v['subs'] ?>,title:'<?= HTML::escape($v['t'], 'js') ?>'}">
          <span class="dropdown-menu-ico">
            <img src="<?= $v['i'] ?>" alt="<?= $v['t'] ?>">
          </span>
          <span class="l-categories-items-i-name"><?= $v['t'] ?></span>
        </a>
      </li>
      <? } ?>
    </ul>
</div>
<? } else if($step == 2) { # ШАГ 2 ?>
<div class="l-categories">
  <div class="dropdown-menu-heading">
    <div class="dropdown-menu-heading-container">
      <div class="dropdown-menu-heading-container-content">
        <span class="nowrap">
          <? if( $parent['main'] ) { ?>
          <a href="#" class="link-ajax j-back" data="{prev:0}">
            &laquo; <span><?= _t('filter','Вернуться к основным категориям') ?></span>
          </a>
          <? } else { ?>
          <a href="#" class="link-ajax j-back" data="{prev:<?= $parent['pid'] ?>}">&laquo; <span><?= _t('filter','Вернуться назад') ?></span></a>
          <? } ?>
        </span>
        <div class="dropdown-menu-heading-title"><?= $parent['title'] ?></div>
        <span><? echo '<a href="'.$parent['link'].'" data="{id:'.$parent['id'].',pid:'.$parent['pid'].',subs:1,title:\''.HTML::escape($parent['title'], 'js').'\'}" class="j-parent">'.number_format($parent['shops'], 0, '.', ' ').'</a>&nbsp;', tpl::declension($parent['shops'], $lang_shops, false) ?></span>
      </div>
    </div>
  </div>
  <div class="l-categories-list-wrapper">
    <?
    $cats = ( sizeof($cats) > 6 ? array_chunk($cats, round( sizeof($cats) / 2 ) ) : array($cats) );
    foreach($cats as $catsChunk):
      ?><div class="l-categories-list-wrapper-in"><ul class="dropdown-menu-list"><?
    foreach($catsChunk as $v):
      ?><li><a href="<?= $v['l'] ?>" class="j-sub<? if($v['active']) { ?> active<? } ?>" data="{id:<?= $v['id'] ?>,pid:<?= $parent['id'] ?>,subs:<?= ($cut2levels ? 0 : $v['subs']) ?>,title:'<?= HTML::escape($v['t'], 'js') ?>'}"><span class="cat-name"><?= $v['t'] ?></span><? if($v['subs'] && ! $cut2levels) { ?> &raquo;<? } ?></a></li><?
    endforeach; ?>
    </ul></div>
  <?  endforeach; ?>
  </div>
</div>
<? } ?>