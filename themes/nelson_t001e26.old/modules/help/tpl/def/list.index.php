<?php
/**
 * Помощь: главная
 * @var $this Help
 * @var $breadCrumbs array хлебные крошки
 * @var $titleh1 string заголовок H1 или пустая строка
 * @var $items array список
 * @var $favs array избранные вопросы
 * @var $seotext string SEO-текст
 */
?>

<?= tpl::getBreadcrumbs($breadCrumbs); ?>

<div class="l-content">
  <div class="container">
    <div class="l-pageHeading">
      <h1 class="l-pageHeading-title"><?= (!empty($titleh1) ? $titleh1 : _t('help', 'Помощь по проекту')) ?></h1>
    </div>
    
    <div class="hl-list">
      <? foreach($items as &$v){ ?>
      <? if ( ! empty($v['subcats'])) { ?>
      <div class="hl-list-category">
        <h2 class="hl-list-title"><?= $v['title'] ?></h2>
        <ul class="hl-list-items">
          <? foreach($v['subcats'] as $c) { ?>
          <li><a href="<?= Help::url('cat', array('keyword'=>$c['keyword'])) ?>"><?= $c['title'] ?></a></li>
          <? } ?>
        </ul>
      </div>
      <? } else if($v['questions']) { ?>
      <div class="hl-list-category">
        <h2 class="hl-list-title"><?= $v['title'] ?></h2>
        <ul class="hl-list-items">
          <? foreach($v['questions_list'] as $q) { ?>
          <li><a href="<?= Help::urlDynamic($q['link']) ?>"><?= $q['title'] ?></a></li>
          <? } ?>
        </ul>
      </div>
      <? } ?>
      <? } unset($v); ?>

      <? if( ! empty($favs)) { ?>
      <div class="hl-list-category">
        <h2 class="hl-list-title"><?= _t('help', 'Частые вопросы') ?></h2>
        <ul class="hl-list-items">
          <? foreach($favs as $v) { ?>
          <li><a href="<?= Help::urlDynamic($v['link']) ?>"><?= $v['title'] ?></a></li>
          <? } ?>
        </ul>
      </div>
      <? } ?>

    </div>

    <? if(!empty($seotext)) { ?>
    <div class="l-seoText">
      <?= $seotext ?>
    </div>
    <? } ?>
  </div>
</div>
