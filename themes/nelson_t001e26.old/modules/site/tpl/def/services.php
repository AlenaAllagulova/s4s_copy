<?php
/**
 * Страница "Услуги"
 * @var $this Site
 * @var $titleh1 string заголовок H1
 * @var $svc_bbs array данные об услугах для объявлений
 * @var $svc_shops array данные об услугах для магазинов
 * @var $shop_opened boolean открыт ли у текущего пользователя магазин
 * @var $shop_promote_url string URL на страницу продвижения магазина
 * @var $shop_open_url string URL на страницу открытия магазина
 * @var $user_logined boolean авторизован ли пользователь
 * @var $seotext string SEO-текст
 */
?>
<div class="l-content">
  <div class="container">
    <div class="l-pageHeading">
      <h1 class="l-pageHeading-title"><?= (!empty($titleh1) ? $titleh1 : _t('svc', 'Как продать быстрее?')) ?></h1>
    </div>
    <div class="container-mobile">
      <div class="l-content-box">
        <ul class="nav nav-tabs has-first mrgt15" role="tablist">
          <li class="active"><a href="#tab-ads" aria-controls="tab-ads" role="tab" data-toggle="tab"><?= _t('svc', 'Объявления') ?></a></li>
          <li><a href="#tab-shops" aria-controls="tab-shops" role="tab" data-toggle="tab"><?= _t('svc', 'Магазин') ?></a></li>
        </ul>
        <div class="tab-content">
          
          <!-- Ads tab -->
          <div role="tabpanel" class="tab-pane active" id="tab-ads">
            <div class="l-content-box-in">
              <? foreach($svc_bbs as $v) { ?>
              <div class="l-serviceItem">
                <div class="l-serviceItem-img">
                  <img src="<?= $v['icon_b'] ?>" class="hidden-xs" alt="" />
                  <img src="<?= $v['icon_s'] ?>" class="visible-xs" alt="" />
                </div>
                <div class="l-serviceItem-content">
                  <h3 class="l-serviceItem-title"><?= $v['title_view'] ?></h3>
                  <?= nl2br($v['description_full']) ?>
                </div>
              </div>
              <? } ?>
            </div>
            <div class="l-serviceAds">
              <div class="l-serviceAds-item">
                <div class="l-serviceAds-item-title">
                  <?= _t('svc', 'Подайте новое объявление и сделайте его заметным') ?>
                </div>
                <a class="btn btn-success btn-lg" href="<?= BBS::url('item.add') ?>"> <i class="fa fa-plus"></i> <?= _t('svc', 'Добавить объявление') ?></a>
              </div>
              <? if($user_logined) { ?>
              <div class="l-serviceAds-item">
                <div class="l-serviceAds-item-title">
                  <?= _t('svc', 'Рекламируйте уже существующие объявления') ?>
                </div>
                <a class="btn btn-info btn-lg" href="<?= BBS::url('my.items') ?>"><i class="fa fa-user"></i> <?= _t('svc', 'Мои объявления') ?></a>
              </div>
              <? } ?>
            </div>
          </div><!-- /#tab-ads -->

          <!-- Shops tab -->
          <div role="tabpanel" class="tab-pane" id="tab-shops">
            <div class="l-content-box-in">
              <? foreach($svc_shops as $v) { ?>
              <div class="l-serviceItem">
                <div class="l-serviceItem-img">
                  <img src="<?= $v['icon_b'] ?>" class="hidden-xs" alt="" />
                  <img src="<?= $v['icon_s'] ?>" class="visible-xs" alt="" />
                </div>
                <div class="l-serviceItem-content">
                  <h3 class="l-serviceItem-title"><?= $v['title_view'] ?></h3>
                  <?= nl2br($v['description_full']) ?>
                </div>
              </div>
              <? } ?>
            </div>
            <div class="l-serviceAds">
              <? if ($shop_opened) { ?>
              <div class="l-serviceAds-item">
                <div class="l-serviceAds-item-title">
                  <?= _t('shops', 'Рекламируйте свой магазин') ?>
                </div>
                <a class="btn btn-success btn-lg" href="<?= $shop_promote_url ?>"><i class="fa fa-arrow-up"></i> <?= _t('shops', 'Рекламировать') ?></a>
              </div>
              <? } else { ?>
              <div class="l-serviceAds-item">
                <div class="l-serviceAds-item-title">
                  <?= _t('shops', 'Откройте свой магазин') ?>
                </div>
                <a class="btn btn-success btn-lg" href="<?= $shop_open_url ?>"><i class="fa  fa-plus white"></i> <?= _t('shops', 'Открыть магазин') ?></a>
              </div>
              <? } ?>
            </div>
          </div><!-- /#tab-shops -->

        </div>
      </div><!-- /.l-content-box -->
    </div><!-- /.container-mobile -->
  </div><!-- /.container -->
</div><!-- /.l-content -->

<? if (!empty($seotext)) { ?>
<div class="l-seoText">
  <?= $seotext ?>
</div>
<? } ?>