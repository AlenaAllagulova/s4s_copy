<?php
/**
 * Главная страница
 * @var $this Site
 * @var $titleh1 string заголовок H1
 * @var $centerBlock string центральный блок
 * @var $last string блок последних / премиум объявлений (HTML)
 * @var $seotext string SEO-текст
 */
?>

<div class="l-content l-content_white"> 
  <div class="container">

  <?php if(DEVICE_DESKTOP_OR_TABLET) { tpl::includeJS('site.index', false, 1); ?>

    <?php if( ! empty($titleh1) ) { ?>
    <div class="index-title">
      <h1><?= $titleh1; ?></h1>
    </div>
    <?php } ?>

    <?= $centerBlock ?>

    <? if($banner = Banners::view('site_index_last_before')) { ?>
    <div class="l-banner-h mrg0">
      <?= $banner; ?>
    </div>
    <? } ?>
  </div><!-- /.container -->
</div><!-- /.l-content -->
<?= // blog/index.last.block.php
$lastBlog
?>
<div class="l-content l-content_white">
  <div class="container">
    <?= // bbs/index.last.block.php
      $last
    ?>
    <? if($banner = Banners::view('site_index_last_after')) { ?>
    <div class="l-banner-h">
      <?= $banner; ?>
    </div>
    <? } ?>
    <div class="l-seoText"><?= $seotext; ?></div>

  <?php } #DEVICE_DESKTOP_OR_TABLET ?>

  <? if (DEVICE_PHONE) { ?>
    
    <? if($banner = Banners::view('index_mobile')) { ?>
    <div class="l-banner-m">
      <?= $banner; ?>
    </div>
    <? } ?>

    <!-- Mobile Home Page Categories -->
    <div class="mobile-container">
      <div id="j-f-cat-phone-index-step1">
        <?= BBS::i()->catsList('index', bff::DEVICE_PHONE, 0); ?>
      </div>
      <div id="j-f-cat-phone-index-step2" class="hide"></div>
    </div>

    <?= $last ?>
  <? } ?>
  </div><!-- /.container -->
</div><!-- /.l-content -->
