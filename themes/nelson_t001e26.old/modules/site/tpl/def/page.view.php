<?php
/**
 * Статические страницы: просмотр
 * @var $this Site
 * @var $title string заголовок
 * @var $content string содержание
 */
?>

<?= tpl::getBreadcrumbs(array(array('title'=>$title,'active'=>true))); ?>

<div class="l-content">
  <div class="container">
    <div class="l-pageHeading">
      <h1 class="l-pageHeading-title"><?= $title ?></h1>
    </div>
    <div class="container-mobile">
      <div class="l-content-box">
        <div class="l-content-box-in">
          <?= $content ?>
        </div>
      </div>
    </div>
  </div>
</div>