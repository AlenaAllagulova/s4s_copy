<?php
/**
 * Сообщение: успешно
 * @var $message string текст сообщения
 */
?>
<div class="alert alert-success text-success">
  <?= $message; ?>
</div>