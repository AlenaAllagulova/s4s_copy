<?php
/**
 * Website Header
 */
$url = array(
  'item.add'      => BBS::url('item.add'),
  'user.login'    => Users::url('login'),
  'user.register' => Users::url('register'),
  'user.logout'   => Users::url('logout'),
  );
?>

<div class="l-header">
  <div class="l-header-left">
    <a href="<?= bff::urlBase() ?>" class="l-header-logo">
      <img src="<?= Site::logoURL('header') ?>" alt="<?= HTML::escape(Site::titleHeader()) ?>" />
    </a>
    <a href="#header-nav" data-toggle="collapse" class="l-header-nav-toggle collapsed">
      <span class="l-header-nav-toggle-bar l-header-nav-toggle-bar1"></span>
      <span class="l-header-nav-toggle-bar l-header-nav-toggle-bar2"></span>
      <span class="l-header-nav-toggle-bar l-header-nav-toggle-bar3"></span>
    </a>
    <nav class="l-header-nav collapse" id="header-nav">
      <? $aMainMenu = Sitemap::view('main'); foreach($aMainMenu as $k=>$v) { ?>
        <a href="<?= $v['link'] ?>"<?= ($v['target'] === '_blank' ? ' target="_blank"' : '') ?> <? if($v['a']) { ?> class="active"<? } ?>><?= $v['title'] ?></a>
        <? } ?>
    </nav>
  </div>
  <div class="l-header-right">
    <? if( ! User::id() ) { $favsCounter = BBS::i()->getFavorites(0, true); ?>
      <!-- Guest User -->
      <div class="l-header-item"><a href="<?= $url['user.login'] ?>" class="l-header-btn"><i class="fa fa-user l-header-btn-ico"></i> <span class="l-header-btn-text  hidden-xs"><?= _t('users', 'Войдите') ?></span></a></div><div class="l-header-item"><a href="<?= $url['user.register'] ?>" class="l-header-btn"><i class="fa fa-pencil l-header-btn-ico"></i> <span class="l-header-btn-text hidden-xs"><?= _t('users', 'Зарегистрируйтесь') ?></span></a></div>
      <? if($favsCounter){ ?>
        <div class="l-header-item">
          <a href="<?= BBS::url('my.favs') ?>" class="l-header-btn">
            <i class="fa fa-star l-header-btn-ico"></i>
            <span class="l-header-btn-text j-cnt-fav"><?= $favsCounter ?></span>
          </a>
        </div>
      <? } ?>
    <? } else { $userMenu = Users::i()->my_header_menu(); ?>
      <!-- Logged User -->
      <div class="l-header-item l-header-item_user dropdown">
        <a href="#" class="l-header-btn" data-toggle="dropdown">
          <i class="fa fa-user l-header-btn-ico"></i>
          <span class="l-header-btn-text hidden-xs"><?= tpl::truncate($userMenu['user']['name'], 20) ?></span>
          <b class="l-header-btn-text caret"></b>
        </a>
        <ul class="dropdown-menu dropdown-menu-dark notrounded">
          <? foreach($userMenu['menu'] as $k=>$v): if( $v == 'D' ) { ?>
            <li class="divider"></li>
            <? } else { ?>
            <li><a href="<?= $v['url'] ?>"><i class="<?= $v['i'] ?>"></i> <?= $v['t'] ?></a></li>
          <? } endforeach; ?>
        </ul>
      </div>
      <div class="l-header-item">
        <a href="<?= $userMenu['menu']['favs']['url'] ?>" class="l-header-btn">
          <i class="fa fa-star l-header-btn-ico"></i>
          <span class="l-header-btn-text<?= ( ! $userMenu['user']['cnt_items_fav'] ? ' hide' : '' ) ?> j-cnt-fav"><?= $userMenu['user']['cnt_items_fav'] ?></span>
        </a>
      </div>
      <div class="l-header-item">
        <a href="<?= $userMenu['menu']['messages']['url'] ?>" class="l-header-btn">
          <i class="fa fa-comment l-header-btn-ico"></i>
          <span class="l-header-btn-text<?= ( ! $userMenu['user']['cnt_internalmail_new'] ? ' hide' : '' ) ?> j-cnt-msg"><?= $userMenu['user']['cnt_internalmail_new'] ?></span>
        </a>
      </div>
    <? } ?>
    <div class="l-header-item">
      <a href="<?= $url['item.add'] ?>" class="l-header-btn l-header-btn-add"><i class="fa fa-plus white l-header-btn-ico"></i> <span class="l-header-btn-text visible-lg"><?= _t('header', 'Добавить объявление') ?></span></a>
    </div>
  </div>
</div>
