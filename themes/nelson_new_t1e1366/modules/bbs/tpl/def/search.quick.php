<?php

/**
 * Быстрый поиск объявлений: список (desktop, tablet)
 * @var $this BBS
 * @var $items array объявления
 */

$lang_photo = _te('bbs', 'фото');

?>
<? foreach($items as $v){ ?>
<a href="<?= $v['link'] ?>" class="l-filter-qsearch-item">
  <span class="l-filter-qsearch-item-title">
    <span class="l-filter-qsearch-item-title-name"><?= $v['title'] ?></span>
    <? if($v['price_on']) { ?><span class="l-filter-qsearch-item-title-price"><?= tpl::itemPrice($v['price'], $v['price_curr'], $v['price_ex']) ?></span><? } ?>
  </span>
  <span class="l-filter-qsearch-item-img">
    <?
    foreach ($v['img'] as $i):
      echo '<img src="'.$i.'" title="'.$v['title'].' '.$lang_photo.'" />';
    endforeach;
    ?>
  </span>
</a>
<? } ?>