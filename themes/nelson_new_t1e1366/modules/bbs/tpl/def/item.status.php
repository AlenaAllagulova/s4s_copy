<?php
/**
 * Статус объявления: результат добавления / редактирования
 * @var $this BBS
 * @var $state string статус
 * @var $user array данные пользователя
 * @var $new_user boolean новый пользователь
 * @var $item array данные об объявлении
 */

$urlAdd = BBS::url('item.add');

switch($state)
{
    # Добавление + публикация
  case 'new.publicated': {
    if( BBS::premoderation() ) {
      ?>
      <p>
        <?= _t('item-form', 'После проверки модератором ваше объявление будет опубликовано') ?>
      </p>
      <?
    } else {
      ?>
      <p>
        <?= _t('item-form', 'Теперь вы можете <a [link_view]>просмотреть ваше объявление</a> или <a [link_add]>добавить ещё одно</a>',
          array(
            'link_view'=>'href="'.$item['link'].'?from=add"',
            'link_add'=>'href="'.$urlAdd.'"'
            )) ?>
          </p>
          <? }
          ?>
          <ul class="list-unstyled">
            <li>
              <span><?= _t('item-form', '<a [link_add]>Добавить объявление</a> в ту же категорию', array('link_add'=>'href="'.$urlAdd.'?cat='.$item['cat_id'].'"')); ?></span>
            </li>
            <? if (User::id()){ ?>
            <li>
              <span><?= _t('item-form', 'Назад к <a [link_mylist]>списку своих объявлений</a>', array('link_mylist'=>'href="'.BBS::url('my.items', array('from'=>'add')).'"')); ?></span>
            </li>
            <? } ?>
            <li>
              <span><?= _t('item-form', 'Назад на <a [link_home]>главную страницу</a>', array('link_home'=>'href="'.bff::urlBase().'"')); ?></span>
            </li>
          </ul>
          <?
        } break;

    # Добавление от неавторизованного пользователя
        case 'new.notactivated': {
          ?>
          <p>
           <?= _t('item-form', 'Ваше объявление сохранено, но ещё не опубликовано.') ?><br />
           <?= _t('item-form', 'Мы выслали вам письмо на адрес <b>[email]</b> со ссылкой для активации, пожалуйста, проверьте вашу почту.', array('email' => $user['email'])) ?><br />
           <?= _t('item-form', 'Если вы не видите письма, проверьте папку Спам, а также правильность написания вашего адреса.') ?>
         </p>
         <ul class="list-unstyled">
          <li>
            <span><?= _t('item-form', '<a [link_add]>Добавить еще одно</a> объявление', array('link_add'=>'href="'.$urlAdd.'"')); ?></span>
          </li>
          <li>
            <span><?= _t('item-form', '<a [link_add]>Добавить объявление</a> в ту же категорию', array('link_add'=>'href="'.$urlAdd.'?cat='.$item['cat_id'].'"')); ?></span>
          </li>
          <li>
            <span><?= _t('item-form', 'Назад на <a [link_home]>главную страницу</a>', array('link_home'=>'href="'.bff::urlBase().'"')); ?></span>
          </li>
        </ul>
        <?
      } break;

    # Добавление + активация телефона
      case 'new.notactivated.phone': {
        tpl::includeJS('users.auth', false, 4);
        $phone_change_allowed = !$new_user;
        ?>
        <div class="text-center">
          <p>
           <?= _t('item-form', 'Ваше объявление сохранено, но ещё не опубликовано.') ?>
         </p>
         <p>
           <?= _t('users', 'На номер [phone] отправлен код активации.', array('phone'=>'<strong id="j-u-register-phone-current-number">+'.$user['phone_number'].'</strong>')) ?>
         </p>
         <? if($phone_change_allowed) { ?>
         <p>
           <?= _t('users', 'Не получили код подтверждения? Возможно ваш номер написан с ошибкой.') ?>
         </p>
         <? } ?>
       </div>
       <div id="j-u-register-phone-block-code">
         <form action="" class="form-horizontal mrgt30">
           <div class="form-group">
             <label for="phone-code-input" class="col-md-5 col-sm-4 control-label"><?= _t('users', 'Код подтверждения') ?></label>
             <div class="col-md-2 col-sm-4 mrgb10">
               <input type="text" class="form-control j-u-register-phone-code-input" id="phone-code-input" placeholder="<?= _te('users', 'Введите код') ?>" />
             </div>
             <div class="col-md-5 col-sm-4">
               <button type="submit" class="btn btn-default j-u-register-phone-code-validate-btn"><?= _t('users', 'Подтвердить') ?></button>
             </div>
           </div>
         </form>

         <div class="text-center">
           <ul class="list-unstyled">
             <? if($phone_change_allowed) { ?>
             <li class="mrgb10"><a href="#" class="link-ajax j-u-register-phone-change-step1-btn"><span><?= _t('users', 'Изменить номер телефона') ?></span></a></li>
             <? } ?>
             <li class="mrgb10"><a href="#" class="link-ajax j-u-register-phone-code-resend-btn"><span><?= _t('users', 'Выслать новый код подтверждения') ?></span></a></li>
           </ul>
         </div>

      </div>
      <? if($phone_change_allowed) { ?>
      <div id="j-u-register-phone-block-phone" style="display: none;">

        <form action="" class="form-horizontal mrgt30">
          <div class="form-group">
            <label class="col-sm-4 control-label"><?= _t('users', 'Номер телефона') ?></label>
            <div class="col-sm-4 mrgb10">
              <?= Users::i()->registerPhoneInput(array('name'=>'phone', 'id'=>'j-u-register-phone-input', 'value'=>'+'.$user['phone_number'])) ?>
            </div>
            <div class="col-sm-4">
              <button type="button" class="btn btn-default j-u-register-phone-change-step2-btn"><?= _t('users', 'Выслать код') ?></button>
            </div>
          </div>
        </form>

      </div>

      <? } ?>
      <script type="text/javascript">
        <? js::start(); ?>
        $(function(){
          jUserAuth.registerPhone(<?= func::php2js(array(
            'lang' => array(
              'resend_success' => _t('users', 'Код подтверждения был успешно отправлен повторно'),
              'change_success' => _t('users', 'Код подтверждения был отправлен на указанный вами номер'),
              ),
            )) ?>);
        });
        <? js::stop(); ?>
      </script>
      <?
    } break;

    # Отредактировали (без изменения статуса)
    case 'edit.normal': {
      ?>
      <p>
        <?= _t('item-form', 'Теперь вы можете <a [link_view]>просмотреть ваше объявление</a> или <a [link_add]>добавить ещё одно</a>',
          array(
            'link_view'=>'href="'.$item['link'].'?from=edit"',
            'link_add'=>'href="'.$urlAdd.'"'
            )) ?>
          </p>
          <ul class="list-unstyled">
            <li>
              <span><?= _t('item-form', '<a [link_add]>Добавить объявление</a> в ту же категорию', array('link_add'=>'href="'.$urlAdd.'?cat='.$item['cat_id'].'"')); ?></span>
            </li>
            <li>
              <span><?= _t('item-form', 'Назад к <a [link_mylist]>списку своих объявлений</a>', array('link_mylist'=>'href="'.BBS::url('my.items', array('from'=>'edit')).'"')); ?></span>
            </li>
          </ul>
          <?
        } break;

    # Опубликовали
        case 'edit.publicated': {
          ?>
          <p>
            <?= _t('item-form', 'Теперь вы можете <a [link_view]>просмотреть ваше объявление</a> или <a [link_add]>добавить ещё одно</a>',
              array(
                'link_view'=>'href="'.$item['link'].'?from=edit"',
                'link_add'=>'href="'.$urlAdd.'"'
                )) ?>
              </p>
              <ul class="list-unstyled">
                <li>
                  <span><?= _t('item-form', 'Назад к <a [link_mylist]>списку своих объявлений</a>', array('link_mylist'=>'href="'.BBS::url('my.items', array('from'=>'edit')).'"')); ?></span>
                </li>
              </ul>
              <?
            } break;

    # Сняли с публикации
            case 'edit.publicated.out': {
              ?>
              <p>
                <?= _t('item-form', 'Теперь ваше объявление недоступно для просмотра') ?>
              </p>
              <ul class="list-unstyled">
                <li>
                  <span><?= _t('item-form', 'Перейти на просмотр <a [link_mylist]>списка своих объявлений</a>', array('link_mylist'=>'href="'.BBS::url('my.items', array('from'=>'edit')).'"')); ?></span>
                </li>
                <li>
                  <span><?= _t('item-form', 'Назад на <a [link_home]>главную страницу</a>', array('link_home'=>'href="'.bff::urlBase().'"')); ?></span>
                </li>
              </ul>
              <?
            } break;

    # Редактирование заблокированного объявления
            case 'edit.blocked.wait': {
              ?>
              <p>
                <?= _t('item-form', 'После повторной проверки модератором объявление будет опубликовано') ?>
              </p>
              <ul class="list-unstyled">
                <li>
                  <span><?= _t('item-form', 'Перейти на просмотр <a [link_mylist]>списка своих объявлений</a>', array('link_mylist'=>'href="'.BBS::url('my.items', array('from'=>'edit')).'"')); ?></span>
                </li>
                <li>
                  <span><?= _t('item-form', 'Назад на <a [link_home]>главную страницу</a>', array('link_home'=>'href="'.bff::urlBase().'"')); ?></span>
                </li>
              </ul>
              <?
            } break;

    # Успешно активировали услугу / пакет услуг
            case 'promote.success': {
              ?>
              <p>
                <?= _t('bbs', 'Вы успешно активировали услугу для объявления: <br /><a [link]>[title]</a>', array(
                  'link' => 'href="'.$item['link'].'"',
                  'title' => $item['title'],
                  )) ?>

                </p>
                <ul class="list-unstyled">
                  <? if ( $from == 'my' ) { ?>
                  <li>
                    <span><?= _t('bbs', 'Вернуться к <a [my_link]>списку своих объявлений</a>', array('my_link'=>'href="'.BBS::url('my.items').'"')) ?></span>
                  </li>
                  <? } ?>
                  <li>
                    <span><?= _t('item-form', 'Назад на <a [link_home]>главную страницу</a>', array('link_home'=>'href="'.bff::urlBase().'"')); ?></span>
                  </li>
                </ul>
                <?
              } break;
            }