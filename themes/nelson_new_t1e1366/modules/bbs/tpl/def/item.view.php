<?php

/**
 * Просмотр объявления: layout
 * @var $this BBS
 * @var $id integer ID объявления
 * @var $cats array категории объявления
 * @var $owner bool просматривает владелец объявления
 * @var $share_code string код шаринга в соц. сетях
 * @var $moderation boolean объявление находится на модерации и просматривается модератором
 * Статус объявления:
 * @var $is_publicated boolean объявление публикуется
 * @var $is_publicated_out boolean объявление снято с публикации
 * @var $is_blocked boolean объявление заблокировано
 * @var $is_soon_left boolean объявление публикуется и скоро истекает его срок публикации
 * @var $is_business boolean тип владельца - бизнес
 * @var $is_map boolean выводить карту
 * @var $contacts array контактные данные
 * @var $lang array текстовые фразы
 */

tpl::includeJS('bbs.view', false, 7);

tpl::includeJS('fancybox/jquery.fancybox.pack', false);
tpl::includeJS('fancybox/helpers/jquery.fancybox-thumbs', false);
tpl::includeCSS('/js/fancybox/jquery.fancybox', false);
tpl::includeCSS('/js/fancybox/helpers/jquery.fancybox-thumbs', false);

tpl::includeJS('fotorama/fotorama', false, '4.6.4.1');
tpl::includeCSS('/js/fotorama/fotorama', false);

if ($is_map) {
  Geo::mapsAPI(false);
}
?>

<?= tpl::getBreadcrumbs($cats, true, 'breadcrumb'); ?>

<div class="l-content">
  <div class="container container_sm" id="j-view-container" itemscope itemtype="http://schema.org/Product">

    <?php if ($is_publicated_out): ?>
      <div class="alert alert-info">
        <?= _t('bbs', 'Объявление снято с публикации') ?>
        <?= tpl::date_format2($status_changed) ?>
        <?php if ($owner) { ?>
          <div class="alert-controls">
            <a href="#" class="btn btn-info j-item-publicate"><i
                class="fa fa-refresh white"></i> <?= _t('bbs', 'Опубликовать снова') ?></a>
          </div>
        <?php } ?>
      </div>
    <?php endif; ?>
    <?php if ($owner && $is_publicated && $is_soon_left): ?>
      <div class="alert alert-info">
        <?= _t('bbs', 'Объявление публикуется') ?>
        <?= _t('bbs', 'до [date]', array('date' => tpl::date_format2($publicated_to))) ?>
        <div class="alert-controls">
          <a href="#" class="btn btn-info j-item-refresh"><i
              class="fa fa-refresh white"></i> <?= _t('bbs', 'Продлить') ?></a>
        </div>
      </div>
    <?php endif; ?>
    <?php if ($is_blocked): ?>
      <div class="alert alert-danger">
        <?= _t('bbs', 'Объявление было заблокировано модератором.') ?><br/>
        <?= _t('bbs', 'Причина блокировки:') ?>&nbsp;<strong><?= $blocked_reason ?></strong>
      </div>
    <?php endif; ?>
    <?php if ($is_publicated && BBS::premoderation() && !$moderated && !$moderation): ?>
      <div class="alert alert-warning">
        <?= _t('bbs', 'Данное объявление находится на модерации.') ?><br/>
        <?= _t('bbs', 'После проверки оно будет опубликовано') ?>
      </div>
    <?php endif; ?>

    <div class="ad-top">
      <div class="ad-top-left">
        <ul class="ad-top-info">
          <li>
            <i class="fa fa-map-marker"></i> <?= $city_title_delivery ?>
          </li>
          <li>
            <i
              class="fa fa-calendar"></i> <?= _t('view', '[date], номер: [id]', array('date' => tpl::date_format2($created), 'id' => $id)) ?>
          </li>
        </ul>
      </div>
      <div class="ad-top-right">
        <ul class="ad-top-func">
          <?php if(bff::servicesEnabled() && BBS::itemViewPromoteAvailable($owner)) { ?>
          <li>
            <a href="<?= $promote_url ?>" class="btn btn-promo"><i class="fa fa-rocket btn-ico"></i> <span
                class="hidden-sm hidden-xs"><?= _t('view', 'Продвинуть объявление') ?></span></a>
          </li>
          <?php } ?>
          <li>
            <a href="#" class="btn btn-iconic has-tooltip" title="<?= _te('view', 'Поделиться с другом') ?>"
               data-container="body" id="j-v-send4friend-desktop-link"><i class="fa fa-share"></i></a>
            <div class="dropdown-menu ad-top-func-dropdown" id="j-v-send4friend-desktop-popup">
              <div class="dropdown-menu-in">
                <form action="">
                  <div class="input-group">
                    <input type="text" name="email" class="form-control j-required"
                           placeholder="<?= _te('', 'E-mail') ?>"/>
                    <input type="hidden" name="item_id" value="<?= $id ?>"/>
                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-default j-submit"><?= _t('', 'Отправить') ?></button>
                    </span>
                  </div>
                </form>
              </div>
            </div>
          </li>
          <?php if (!$owner) { ?>
            <li>
              <a href="#" class="btn btn-iconic has-tooltip" title="<?= _te('view', 'Пожаловаться') ?>"
                 data-container="body" id="j-v-claim-desktop-link"><i class="fa fa-warning"></i></a>
              <!-- Cimplain Dropdown -->
              <div class="dropdown-menu ad-top-func-dropdown" id="j-v-claim-desktop-popup">
                <div class="dropdown-menu-heading">
                  <div class="dropdown-menu-heading-title">
                    <?= _t('item-claim', 'Укажите причины, по которым вы считаете это объявление некорректным') ?>:
                  </div>
                </div>
                <div class="dropdown-menu-in">
                  <form action="">
                    <?php foreach ($this->getItemClaimReasons() as $k => $v): ?>
                      <div class="checkbox">
                        <label><input type="checkbox" class="j-claim-check" name="reason[]"
                                      value="<?= $k ?>"/> <?= $v ?> </label>
                      </div>
                    <?php endforeach; ?>
                    <div class="form-group j-claim-other" style="display: none;">
                      <label for="actions-complaint-other"><?= _t('item-claim', 'Оставьте ваш комментарий') ?></label>
                      <textarea name="comment" rows="3" class="form-control" id="actions-complaint-other"
                                autocapitalize="off"></textarea>
                    </div>
                    <?php if (!User::id()): ?>
                      <div class="form-group">
                      <?php if (Site::captchaCustom('bbs-item-view')): ?>
                        <?php bff::hook('captcha.custom.view', 'bbs-item-view', __FILE__); ?>
                      <?php else: ?>
                        <label
                          for="actions-complaint-captcha"><?= _t('item-claim', 'Введите результат с картинки') ?></label>
                        <div class="row">
                          <div class="col-xs-6">
                            <input type="text" name="captcha" class="form-control required" id="actions-complaint-captcha"
                                   value="" pattern="[0-9]*"/>
                          </div>
                          <div class="col-xs-6">
                            <img src="" alt="" class="j-captcha"
                                 onclick="$(this).attr('src', '<?= tpl::captchaURL() ?>&rnd='+Math.random())"/>
                          </div>
                        </div>
                      <?php endif; ?>
                      </div>
                    <?php endif; ?>
                    <button type="submit"
                            class="btn btn-danger j-submit"><?= _t('item-claim', 'Отправить жалобу') ?></button>
                  </form>
                </div>
              </div>
            </li><!-- /.ad-actions-item -->
          <?php } ?>
          <li>
            <a href="?print=1" target="_blank" class="btn btn-iconic has-tooltip"
               title="<?= _te('view', 'Распечатать') ?>" data-container="body"><i class="fa fa-print"></i></a>
          </li>
          <?php if (!$is_publicated_out) { ?>
            <li>
              <?php if ($fav) { ?>
                <a href="#" class="btn c-fav active j-i-fav has-tooltip" data="{id:<?= $id ?>}"
                   title="<?= $lang['fav_out'] ?>" data-original-title="<?= $lang['fav_out'] ?>"
                   data-container="body"><span class="item-fav__star"><i class="fa fa-star j-i-fav-icon"></i></span></a>
              <?php } else { ?>
                <a href="#" class="btn c-fav j-i-fav has-tooltip" data="{id:<?= $id ?>}" title="<?= $lang['fav_in'] ?>"
                   data-original-title="<?= $lang['fav_in'] ?>" data-container="body"><span class="item-fav__star"><i
                      class="fa fa-star-o j-i-fav-icon"></i></span></a>
              <?php } ?>
            </li>
          <?php } ?>
        </ul>
      </div>
    </div>

    <div class="container-mobile">
      <div class="l-content-box">
        <div class="l-content-box-heading">
          <div class="l-content-box-heading-l">
            <?php if ($svc_quick) { ?>
              <span class="label-md label-urgent"><?= _t('bbs', 'срочно') ?></span>&nbsp;
            <?php } ?>
            <h1 class="l-content-box-heading-title" itemprop="name">
              <?= $title ?>
            </h1>
          </div>
          <?php if ($price_on) { ?>
          <div class="l-content-box-heading-price">
            <div class="ad-price"<? if($price) { ?> itemprop="offers" itemscope itemtype="http://schema.org/Offer"<? } ?>>
              <?php if ($price) { ?>
                <?= $price ?>
                <?php if($price_value > 0) { ?>
                    <span itemprop="priceCurrency" content="<?= mb_strtoupper(Site::currencyData($price_curr, 'keyword')) ?>"></span>
                    <span itemprop="price" content="<?= number_format($price_value, 2, '.', '') ?>"></span>
                <?php } ?>
              <?php } ?>
              <?php if ($price_mod) { ?>
                <div class="ad-price-mod"><?= $price_mod ?></div>
              <?php } ?>
            </div>
          </div>
          <?php } ?>
        </div>
        <?php if (!$is_publicated_out) { ?>
          <div class="l-content-box-cols">
            <div class="l-content-box-left">
              <div class="l-content-box-in">

                <div class="ad-images fotorama" id="j-view-images" data-auto="false" data-controlsonstart="false">
                  <?
                  $i = 0;
                  foreach ($images as $v) { ?>
                    <div data-img="<?= $v['url_view'] ?>" data-thumb="<?= $v['url_small'] ?>" data-alt="<?= $v['t'] ?>"
                         class="j-view-images-frame">
                      <a href="javascript:;" class="ad-images-zoom j-zoom" data-zoom="<?= $v['url_zoom'] ?>"
                         data-index="<?= $i++; ?>"><i class="fa fa-search"></i></a>
                    </div>
                  <?php }
                  echo $this->itemVideo()->viewFotorama($video_embed);
                  if ($is_map) { ?>
                    <div data-thumb="<?= bff::url('/img/map_marker.gif') ?>" class="j-view-images-frame j-map">
                    <div id="j-view-map" style="height:<?= (DEVICE_DESKTOP ? '450' : '350') ?>px; width: 100%;"></div>
                    </div><?php } ?>
                </div>

                <?php if ($is_map) { ?>
                  <div class="ad-address">
                    <span class="ad-address-attr"><?= _t('view', 'Адрес') ?>:</span>
                    <span class="ad-address-val"><?= $city_title ?>, <?
                      if ($district_id && !empty($district_data['title'])) {
                        echo _t('view', 'район [district]', array('district' => $district_data['title'])) . ', ';
                      } ?><?
                      if ($metro_id && !empty($metro_data['title'])) {
                        echo _t('view', 'метро [station]', array('station' => $metro_data['title'])) . ', ';
                      } ?><?= $addr_addr ?>,
                    <a href="#" class="ajax"
                       onclick="return jView.showMap(event);"><span><?= _t('view', 'показать на карте') ?></span></a>
                  </span>
                  </div>
                <?php } elseif ($cat_addr && $metro_id && !empty($metro_data['title'])) { ?>
                  <div class="ad-address">
                    <span class="ad-address-attr"><?= _t('view', 'Адрес') ?>:</span>
                    <span class="ad-address-val"><?= $city_title ?>, <?
                      if ($district_id && !empty($district_data['title'])) {
                        echo _t('view', 'район [district]', array('district' => $district_data['title'])) . ', ';
                      }
                      ?>
                      <?= _t('view', 'метро [station]', array('station' => $metro_data['title'])); ?>
                  </span>
                  </div>
                <?php } # is_map  ?>
              </div>
              <div class="l-content-box-in">
                <?php if ($dynprops) { ?>
                  <div class="ad-dynprops"><?= $dynprops ?></div>
                <?php } ?>
                <div class="ad-descr" itemprop="description"><?= nl2br($descr) ?></div>
              </div>

              <?php if (!$is_publicated_out && !$owner && (!$moderation || ($moderation && !$moderated))) {
                /* Contact Form */
                ?>
                <div class="l-content-box-in">
                  <a name="contact-form"></a>
                  <div class="l-blockHeading">
                    <div class="l-blockHeading-title"><?= _t('view', 'Свяжитесь с автором объявления') ?></div>
                  </div>
                  <div class="ad-contact-block">
                    <div class="ad-contact-name">
                      <?php if ($is_shop): ?>
                        <a href="<?= $shop['link'] ?>"><?= $shop['title'] ?></a>
                      <?php else: ?>
                        <?= $name ?>
                      <?php endif; ?>
                    </div>
                    <div class="ad-contact-phones j-v-contacts-expand-block">
                      <?php $contactsExpand = false;
                      if (!empty($contacts['phones'])) { ?>
                        <div class="j-c-phones">
                        <?php foreach ($contacts['phones'] as $v): ?>
                          <div class="ad-contact-phones-item">
                            <span><?= $v ?></span>
                            <?php if (!$contactsExpand) { $contactsExpand = true ?>
                              <a href="#" class="link-ajax j-v-contacts-expand-link">
                                <span><?= _t('view', 'Показать контакты') ?></span>
                              </a>
                            <?php } ?><br/>
                          </div>
                        <?php endforeach; ?>
                        </div>
                      <?php } ?>
                      <?php if (!empty($contacts['contacts'])): ?>
                        <?php foreach (Users::contactsFields($contacts['contacts']) as $contact): ?>
                          <div class="ad-contact-item">
                            <span class="ad-contact-item-title"><?= $contact['title'] ?></span>
                            <span class="ad-contact-item-content j-c-<?= $contact['key'] ?>">
                                <span><?= tpl::contactMask($contact['value']) ?></span>
                                <?php if (!$contactsExpand) { $contactsExpand = true ?>
                                  <a href="#" class="link-ajax j-v-contacts-expand-link">
                                    <span><?= _t('view', 'Показать контакты') ?></span>
                                  </a>
                                <?php } ?>
                            </span>
                          </div>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </div>

                    <form action="?act=contact-form" id="j-view-contact-form">
                      <?= Users::i()->writeForm('j-view-contact-form') ?>
                    </form>

                  </div>
                </div>
              <?php } ?>


              <?php if (DEVICE_PHONE) { ?>
                <div class="l-content-box-in">
                  <?php // Item owner block
                  echo View::template('item.view.owner', $aData, 'bbs');
                  ?>
                </div>
              <?php } ?>

              <?php if ($comments) { ?>
                <div class="l-content-box-in">
                  <div class="l-comments" id="j-comments-block">
                    <?php
                    /* Comments */
                    echo $comments;
                    ?>
                  </div>
                </div>
              <?php } ?>
              <?php if ($similar && $is_publicated && !$moderation) { ?>
                <div class="l-content-box-in">
                  <div class="ad-similar">
                    <?= $similar; ?>
                  </div>
                  <? if ($bannerSimilar = Banners::view('bbs_view_similar_bottom', array('cat' => $cat_id, 'region' => $city_id))) { ?>
                    <div class="l-banner">
                      <div class="l-banner__content">
                        <?= $bannerSimilar; ?>
                      </div>
                    </div>
                  <?php } ?>
                </div><!-- /.l-content-box-in -->
              <?php } ?>
            </div><!-- /.l-content-box-left -->

            <?php if (DEVICE_DESKTOP_OR_TABLET) { ?>
              <div class="l-content-box-sidebar">
                <?= // Item owner block
                View::template('item.view.owner', $aData, 'bbs');
                ?>

                <?php if ($bannerRight = Banners::view('bbs_view_right', array('cat' => $cat_id, 'region' => $city_id))) { ?>
                  <div class="l-content-box-in">
                    <div class="l-banner-v">
                      <?= $bannerRight ?>
                    </div>
                  </div>
                <?php } # $bannerRight ?>
              </div><!-- /.l-content-box-sidebar -->
            <?php } ?>

          </div><!-- /.l-content-box-cols -->
        <?php } # ! $is_publicated_out ?>
      </div><!-- /.l-content-box -->
    </div><!-- /.container-mobile -->
  </div><!-- /#j-view-container -->
</div><!-- /.l-content -->

<?php if ($owner): ?>

  <div class="l-ownerBlock" id="j-v-owner-panel">
    <div class="container hidden-xs">
      <div class="l-ownerBlock-top">
        <div class="l-ownerBlock-content">
          <div class="l-ownerBlock-content-l">
            <a href="<?= ($from == 'my' ? 'javascript:history.back();' : BBS::url('my.items')) ?>" class="link-ico"><i
                class="fa fa-chevron-left"></i> <span><?= _t('view', 'Назад в Мои объявления') ?></span></a>
          </div>
          <div class="l-ownerBlock-content-r">
            <a href="#" class="j-panel-actions-toggler" data-state="hide">
              <span class="j-toggler-state"><span><?= _t('view', 'Закрыть') ?></span> <i class="fa fa-chevron-down"></i></span>
              <span class="j-toggler-state hide"><span><?= _t('view', 'Показать') ?></span> <i
                  class="fa fa-chevron-up"></i></span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="l-ownerBlock-main j-panel-actions">
      <div class="container">
        <div class="l-ownerBlock-content">
          <div class="l-ownerBlock-content-l">
            <div class="l-ownerBlock-content-link">
              <a href="<?= BBS::url('item.edit', array('id' => $id, 'from' => 'view')) ?>" class="link-ico"><i
                  class="fa fa-edit"></i> <span><?= _t('view', 'Изменить информацию') ?></span></a>
            </div>
            <?php if ($is_publicated) { ?>
              <div class="l-ownerBlock-content-link">
                <a href="#" class="link-ico j-item-unpublicate"><i class="fa fa-times"></i>
                  <span><?= _t('view', 'Снять с публикации') ?></span></a>
              </div>
            <?php } ?>
            <?php if (!$is_publicated || $is_soon_left) { ?>
              <div class="l-ownerBlock-content-link">
                <a href="#" class="link-ico link-red j-item-delete"><i
                    class="fa <?= $is_soon_left ? 'fa-trash-o' : 'fa-times' ?>"></i>
                  <span><?= _t('view', 'Удалить') ?></span></a>
              </div>
            <?php } ?>
          </div>
          <div class="l-ownerBlock-content-r">
            <a href="<?= InternalMail::url('item.messages', array('item' => $id)) ?>" class="btn btn-sm btn-default"><i
                class="fa fa-envelope"></i> <?= $messages_total ?><span
                class="hidden-xs"> <?= tpl::declension($messages_total, _t('view', 'Сообщение;Сообщения;Сообщений'), false) ?></span></a>
            <?php if ($is_publicated_out) { ?>
              <a href="#" class="btn btn-sm btn-info j-item-publicate"><i class="fa fa-arrow-up white"></i><span
                  class="hidden-xs"> <?= _t('view', 'Активировать') ?></span></a>
            <?php } ?>
            <?php if ($is_publicated && bff::servicesEnabled()) { ?>
              <a href="<?= $promote_url ?>" class="btn btn-sm btn-success"><?= _t('view', 'Рекламировать') ?></a>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>

<?php endif; # owner ?>
<script type="text/javascript">
  <?php js::start(); ?>
  $(function () {
    jView.init(<?= func::php2js(array(
      'lang' => array(
        'sendfriend' => array(
          'email' => _t('', 'E-mail адрес указан некорректно'),
          'success' => _t('', 'Сообщение было успешно отправлено'),
        ),
        'claim' => array(
          'reason_checks' => _t('item-claim', 'Укажите причину жалобы'),
          'reason_other' => _t('item-claim', 'Опишите причину подробнее'),
          'captcha' => _t('', 'Введите результат с картинки'),
          'success' => _t('item-claim', 'Жалоба успешно принята'),
        ),
      ),
      'item_id' => $id,
      'addr_lat' => $addr_lat,
      'addr_lon' => $addr_lon,
      'claim_other_id' => BBS::CLAIM_OTHER,
      'mod' => ($moderation ? BBS::moderationUrlKey($id) : ''),
      'msg_success' => !empty($msg_success) ? $msg_success : '',
      'msg_error' => !empty($msg_error) ? $msg_error : '',
    )) ?>);
  });

  // views stat popup
  $(function () {
    var statPopup = false, $container = $('#j-v-viewstat-desktop-popup-container');
    $('#j-v-viewstat-desktop-link-2').on('click', function (e) {
      nothing(e);
      if (statPopup === false) {
        bff.ajax('?act=views-stat', {}, function (data, errors) {
          if (data && data.success) {
            $container.html(data.popup);
            bff.st.includeJS('d3.v3.min.js', function () {
              jView.viewsChart('#j-v-viewstat-desktop-popup-chart', data.stat.data, data.lang);
              $('.j-popup', $container).modal();
              statPopup = true;
            });
          } else {
            app.alert.error(errors);
          }
        });
      } else {
        $('.j-popup', $container).modal();
      }
    });
  });

  <?php js::stop(); ?>
</script>