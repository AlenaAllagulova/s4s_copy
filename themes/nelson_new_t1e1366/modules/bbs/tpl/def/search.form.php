<?php
/**
 * Поиск объявлений: форма поиска (layout)
 * @var $this BBS
 * @var $f array параметры фильтра
 * @var $catID integer ID текущей категории
 * @var $catData array данные о текущей категории
 * @var $catACTIVE boolean является ли текущая категория выбранной
 * @var $catACTIVE_STEP integer текущий уровень выбора категории: 1,2
 * @var $filterDesktopBlock string блок фильтров desktop/tablet версии (HTML)
 * @var $filterPhoneBlock string блок фильтров phone версии (HTML)
 * @var $filterVertical boolean включен вертикальный фильтр
 */

tpl::includeJS(array('bbs.search'), false, 7);
extract($f, EXTR_REFS | EXTR_PREFIX_ALL, 'f');
?>


<noindex>
  <form id="j-f-form" action="<?= BBS::url('items.search', $catData) ?>" method="get">
    <div class="l-filter">
      <input type="hidden" name="c" value="<?= $f_c ?>"<? if( ! $f_c ) { ?> disabled="disabled"<? } ?> />
      <input type="hidden" name="ct" value="<?= $f_ct ?>" />
      <input type="hidden" name="lt" value="<?= $f_lt ?>" />
      <input type="hidden" name="sort" value="<?= $f_sort ?>" />
      <input type="hidden" name="page" value="<?= $f_page ?>" />
      
      <div class="l-filter-form-wrap">
        <div class="l-filter-form-left">
          <div class="l-filter-form">
            <?= // Region Select
              View::template('filter.region');
            ?>

            <?= // Category Select
              View::template('search.form.categories', $aData, 'bbs');
            ?>

            <!-- input -->
            <div class="l-filter-form-search dropdown">
              <input type="text" name="q" id="j-f-query" class="l-filter-form-input" onkeyup="$(this).next().val($(this).val());" placeholder="<?= _te('bbs','Поиск объявлений...') ?>" value="<?= HTML::escape($f_q) ?>" maxlength="80" />
              <input type="text" name="mq" autocomplete="off" value="<?= HTML::escape($f_q) ?>" maxlength="80" style="display: none;" />
              <div id="j-search-quick-dd" class="l-filter-qsearch dropdown-menu">
                <div class="f-qsearch__results j-search-quick-dd-list"></div>
              </div>
            </div>
            
            <div class="l-filter-form-item">
              <button type="submit" class="l-filter-form-submit j-submit"><i class="fa fa-search"></i> <?= _t('bbs','Найти') ?></button>
            </div>

          </div>
        </div>
        <? if(DEVICE_DESKTOP_OR_TABLET && $catACTIVE && !$filterVertical) { ?>
        <!-- Desktop Filter Toggle -->
        <div class="l-filter-form-right">
          <div class="l-filter-form-right-item">
            <a href="#j-f-desktop" data-toggle="collapse" class="l-filter-form-toggle collapsed"><i class="fa fa-cog"></i> <?= _t('filter', 'Фильтр') ?></a>
          </div>
        </div>
        <? } ?>
        <? if (DEVICE_PHONE && $catACTIVE && !$filterVertical) { ?>
        <!-- Mobile Filter -->
        <div class="l-filter-m">
          <span class="f-catfiltermob__content__title">
            <a class="l-filter-form-toggle collapsed" href="javascript:void(0);"><i class="fa fa-cog"></i> <?= ( $f_filter_active ? _t('bbs', 'Изменить настройки фильтра') : _t('bbs', 'Фильтровать результаты') ) ?></a>
          </span>
          <div class="l-filter-m-content hide" id="j-f-phone">
            <?= $filterPhoneBlock; ?>
            <button type="button" class="btn btn-small btn-success j-submit"><?= _t('bbs', 'Отфильтровать') ?></button>
            <button type="button" class="btn btn-small btn-default j-cancel"><?= _t('bbs', 'Отменить') ?></button>
          </div>
        </div>
        <? } ?>
      </div>
    </div><!-- /.l-filter -->
    <? if(DEVICE_DESKTOP_OR_TABLET && $catACTIVE && !$filterVertical) { ?>
    <!-- Desktop & Tablet Filter -->
    <div class="collapse" id="j-f-desktop">
      <div class="l-filter-options">
        <?= $filterDesktopBlock; ?>
      </div>
    </div>
    <? } ?>
  </form>
</noindex>
