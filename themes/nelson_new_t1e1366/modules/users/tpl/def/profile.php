<?php
  /**
   * Профиль пользователя (layout)
   * @var $this Users
   * @var $user array данные пользователя
   * @var $is_owner boolean профиль просматривает его владелец
   */
  ?>

<div class="l-content" id="j-shops-v-container">
  <div class="container container_sm">
    <div class="l-pageHeading">
      <h1 class="l-pageHeading-title"><?= (!empty($titleh1) ? $titleh1 : _t('bbs', 'Объявления пользователя')) ?></h1>
    </div>
    <div class="container-mobile">
      <div class="l-content-box">
        <div class="l-content-box-cols">
          <div class="l-content-box-left">
            <? if (DEVICE_PHONE) {
              // Owner Block (mobile)
              View::template('profile.owner', $aData, 'users');
            } ?>
            <div class="l-content-box-in">
              <?= $content ?>
            </div>
          </div>
          <?php if (DEVICE_DESKTOP_OR_TABLET) { ?>
          <div class="l-content-box-sidebar">
            <?= // Owner block
              View::template('profile.owner', $aData, 'users');
            ?>
            <?php if ($bannerRight = Banners::view('users_profile_right')) { ?>
            <div class="l-content-box-in">
              <?= $bannerRight ?>
            </div>
            <?php } ?>
          <?php } ?>
          </div>
        </div>
      </div>
  </div>
</div>

<script type="text/javascript">
<? js::start(); ?>
$(function(){
  var _process = false;
  $('.j-user-profile-c-toggler').on('click touchstart', function(e){
    nothing(e); if(_process) return;
    var $link = $(this);
    bff.ajax(bff.ajaxURL('users','user-contacts'), {hash:app.csrf_token, ex:'<?= $user['user_id_ex'] ?>-<?= $user['user_id'] ?>'},
      function(data, errors) {
        if(data && data.success) {
            if (data.hasOwnProperty('phones')) {
                $('.j-user-profile-c-phones').html(data['phones']);
            }
            if (data.hasOwnProperty('contacts')) {
                for(var c in data.contacts) {
                    if (data.contacts.hasOwnProperty(c)) {
                        $('.j-user-profile-c-' + c).html(data.contacts[c]);
                    }
                }
            }
            $link.remove();
        } else {
          app.alert.error(errors);
        }
      }, function(p){ _process = p; }
      );
  });
});
<? js::stop(); ?>
</script>