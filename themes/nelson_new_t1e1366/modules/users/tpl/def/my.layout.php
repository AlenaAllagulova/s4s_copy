<?php
/**
 * Кабинет пользователя (Основное меню)
 * @var $this Users
 * @var $user array данные пользователя
 * @var $tabs array табы
 * @var $shop_open array ссылка на форму открытия магазина или FALSE (магазин уже был открыт)
 * @var $content string содержание текущего раздела кабинета (HTML)
 */
?>

<div class="l-content">
  <div class="container">
    <?php if ($user){ ?>
    <div class="l-pageHeading">
      <h1 class="l-pageHeading-title"><?= _t('users', 'Кабинет пользователя') ?>: <small><?= $user['name'] ?></small></h1>
    </div>
    <?php } ?>
    <div class="usr-box<?php if ($user){ ?> has-sidebar<?php } ?>">
      <?php if ($user){ ?>
      <div class="usr-box-sidebar">
        <button type="button" data-toggle="collapse" data-target="#user-tabs" class="usr-box-sidebar-toggle collapsed">
          <i class="fa fa-chevron-down"></i>
          <?= _t('users', 'Меню') ?>
        </button>
        <ul class="usr-box-sidebar-nav collapse" id="user-tabs">
          <? foreach($tabs as $k=>$v) { if(empty($v['t'])) continue; ?>
          <li<? if( ! empty($v['active']) ){ ?> class="active"<? } ?>>
            <a href="<?= $v['url'] ?>">
              <? if( ! empty($v['counter']) ) { ?>
                <span class="usr-box-sidebar-nav-cnt">
                    <?php if ($k === 'messages') { ?>
                        +<?= $v['counter']; ?>
                    <?php } else if ($k === 'bill') { ?>
                        (<?= $v['counter']; ?>)
                    <? } else { ?>
                        <?= $v['counter']; ?>
                    <?php } ?>
                </span>
              <? } ?>
              <?= $v['t'] ?>
            </a>
          </li>
          <? } ?>
          <? if($shop_open) { ?>
            <li<? if($shop_open['active']){ ?> class="active"<? } ?>>
              <a href="<?= $shop_open['url'] ?>">
                <i class="fa fa-plus u-cabinet__main-navigation__shop-open"></i>
                <?= _t('users', 'Открыть магазин') ?>
              </a>
            </li>
          <? } ?>
        </ul>
      </div>
      <?php } ?>
      <div class="usr-box-content">
        <?= $content ?>
      </div>
    </div>
  </div>
</div>