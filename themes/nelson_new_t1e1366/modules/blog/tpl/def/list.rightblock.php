<?php
/**
 * Блог: список постов - правый блок
 * @var $this Blog
 * @var $categories array список категорий (если включены)
 * @var $tags array список тегов (если включены)
 * @var $favs array список избранных постов
 */
?>

<div class="l-mainLayout-sidebar-box">
  <? // Categories
  if(Blog::categoriesEnabled() && ! empty($categories)) { ?>
  <div class="l-mainLayout-sidebar-item">
    <div class="l-mainLayout-sidebar-item-title"><?= _t('blog', 'Категории') ?></div>
    <ul class="l-mainLayout-sidebar-nav">
      <? foreach($categories as &$v) { ?>
      <? if($v['active']) { ?>
      <li class="active"><a class="l-mainLayout-sidebar-nav-remove" href="<?= Blog::url('index') ?>"><i class="fa fa-times"></i></a> <span><?= $v['title'] ?></span></li>
      <? } else { ?><li><a href="<?= $v['link'] ?>"><?= $v['title'] ?></a></li><? } ?>
      <? } unset($v); ?>
    </ul>
  </div>
  <? } ?>

  <? // Tags
  if(Blog::tagsEnabled() && ! empty($tags)) { ?>
  <div class="l-mainLayout-sidebar-item">
    <div class="l-mainLayout-sidebar-item-title"><?= _t('blog', 'Теги') ?></div>
    <div class="l-mainLayout-sidebar-item-in">
      <div class="c-tags">
        <? foreach($tags as &$v) { ?>
        <? if($v['active']) { ?>
        <a href="<?= Blog::url('index') ?>" class="c-tag active"><?= $v['tag'] ?> <i class="fa fa-times"></i></a>
        <? } else { ?><a href="<?= $v['link'] ?>" class="c-tag"><?= $v['tag'] ?></a><? } ?>
        <? } unset($v); ?>
      </div>
    </div>
  </div>
  <? } ?>

  <? // Favorites
  if ( ! empty($favs)) { ?>
  <div class="l-mainLayout-sidebar-item">
    <div class="l-mainLayout-sidebar-item-title"><?= _t('blog', 'Избранные') ?></div>
    <div class="l-mainLayout-sidebar-item-in">
      <ul class="list-unstyled">
        <? foreach($favs as &$v) { ?>
        <li><a href="<?= $v['link'] ?>"><?= $v['title'] ?></a></li>
        <? } ?>
      </ul>
    </div>
  </div>
  <? } ?>
</div>
<? // Banner
if($bannerRight = Banners::view('blog_search_right')) { ?>
<div class="l-banner-v">
  <?= $bannerRight ?>
</div>
<? } ?>


