<?php
/**
 * Страница магазина: layout
 * @var $this Shops
 * @var $shop array данные о магазине
 * @var $has_owner boolean у магазина есть владелец
 * @var $is_owner boolean является ли текущий пользователь владельцем просматриваемого магазина
 * @var $share_code string код шаринга в соц. сетях
 * @var $url_promote string URL на страницу продвижения магазина
 * @var $url_promote_visible boolean отображать ссылку продвижения магазина
 * @var $request_form_visible boolean отображать форму подачи заявки на представительство
 */
tpl::includeJS('shops.view', false, 5);
?>

<?= tpl::getBreadcrumbs($breadcrumbs); ?>

<div class="l-content" id="j-shops-v-container">
  <div class="container container_sm">
    <div class="l-pageHeading">
      <div class="l-pageHeading-in">
        <div class="l-pageHeading-in-l">
          <h1 class="l-pageHeading-title"><?= $shop['title'] ?></h1>
        </div>
        <div class="l-pageHeading-in-r">
          <ul class="ad-top-func">
            <?php if ($url_promote_visible) { ?>
            <li>
              <a href="<?= $url_promote ?>" class="btn btn-promo"><i class="fa fa-rocket btn-ico"></i> <span><?= _t('shops', 'Продвинуть магазин') ?></span></a>
            </li>
            <?php } ?>
            <li>
              <a href="#" class="btn btn-iconic has-tooltip" title="<?= _te('shops', 'Поделиться с другом') ?>" data-container="body" id="j-shop-view-send4friend-desktop-link"><i class="fa fa-share"></i></a>
              <div class="dropdown-menu ad-top-func-dropdown" id="j-shop-view-send4friend-desktop-popup">
                <div class="dropdown-menu-in">
                  <form action="">
                    <div class="input-group">
                      <input type="text" name="email" class="form-control j-required" placeholder="<?= _te('', 'E-mail') ?>">
                      <input type="hidden" name="item_id" value="<?= $shop['id'] ?>" />
                      <span class="input-group-btn">
                        <button type="submit" class="btn btn-default j-submit"><?= _t('', 'Отправить') ?></button>
                      </span>
                    </div>
                  </form>
                </div>
              </div>
            </li>
            <?php if( ! $is_owner) { ?>
            <li>
              <a href="#" class="btn btn-iconic has-tooltip" title="<?= _te('shops', 'Пожаловаться') ?>" data-container="body" id="j-shops-v-claim-desktop-link"><i class="fa fa-warning"></i></a>
              <!-- Cimplain Dropdown -->
              <div class="dropdown-menu ad-top-func-dropdown" id="j-shops-v-claim-desktop-popup">
                <div class="dropdown-menu-heading">
                  <div class="dropdown-menu-heading-title">
                    <?= _t('shops', 'Укажите причины, по которым вы считаете этот магазин некорректным') ?>:
                  </div>
                </div>
                <div class="dropdown-menu-in">
                  <form action="">
                    <?php foreach($this->getShopClaimReasons() as $k=>$v): ?>
                    <div class="checkbox">
                      <label><input type="checkbox" class="j-claim-check" name="reason[]" value="<?= $k ?>" /> <?= $v ?> </label>
                    </div>
                    <?php endforeach; ?>
                    <div class="form-group j-claim-other" style="display: none;">
                      <label for="actions-complaint-other"><?= _t('shops', 'Оставьте ваш комментарий') ?></label>
                        <textarea name="comment" rows="3" class="form-control" id="actions-complaint-other" autocapitalize="off"></textarea>
                    </div>
                    <? if( ! User::id() ): ?>
                      <?php if (Site::captchaCustom('shops-view')): ?>
                        <?php bff::hook('captcha.custom.view', 'shops-view', __FILE__); ?>
                      <?php else: ?>
                        <div class="form-group">
                          <label for="actions-complaint-captcha"><?= _t('shops', 'Введите результат с картинки') ?></label>
                          <div class="row">
                            <div class="col-xs-6">
                              <input type="text" name="captcha" class="form-control required" id="actions-complaint-captcha" value="" pattern="[0-9]*" />
                            </div>
                            <div class="col-xs-6">
                              <img src="" alt="" class="j-captcha" onclick="$(this).attr('src', '<?= tpl::captchaURL() ?>&rnd='+Math.random())" />
                            </div>
                          </div>
                        </div>
                      <? endif; endif; ?>
                    <button type="submit" class="btn btn-danger j-submit"><?= _t('shops', 'Отправить жалобу') ?></button>
                  </form>
                </div>
              </div>
            </li><!-- /.ad-actions-item -->
            <?php } ?>
          </ul>
        </div>
      </div>
    </div><!-- /.l-pageHeading -->
    <div class="container-mobile">
      <div class="l-content-box">
        <div class="l-content-box-cols">
          <div class="l-content-box-left">
            <div class="l-content-box-in">
              <?= $shop['descr']; ?>
            </div>
            <? if ($request_form_visible) { ?>
            <div class="l-content-box-in">
              <p><?= _t('shops', 'Если вы являетесь представителем этого магазины вы можете получить доступ к управлению информацией о магазине и размещению объявлений от его имени <a [request_form_link]>подав заявку</a>.', array(
                      'request_form_link' => ' href="#" class="link-ajax-nospan" id="j-shop-view-request-form-toggler"',
                      )) ?></p>
              <div id="j-shop-view-request-form-block" style="display: none;">
                  <div class="help-block"><?= _t('shops', 'Укажите ваши контактные данные и мы с вами свяжемся') ?></div>
                  <form action="" class="j-form">
                    <? if ( ! User::id()) { ?>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <input type="text" name="name" class="form-control j-required" placeholder="<?= _te('shops', 'Ваше имя') ?>" maxlength="50">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <input type="tel" name="phone" class="form-control j-required" placeholder="<?= _te('shops', 'Ваш телефон') ?>" maxlength="50">
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <input type="email" name="email" class="form-control j-required" placeholder="<?= _te('shops', 'Ваш email-адрес') ?>" maxlength="100" autocorrect="off" autocapitalize="off">
                        </div>
                      </div>
                    </div>
                    <? } ?>
                    <div class="form-group">
                      <textarea name="description" rows="4" class="form-control j-required j-description" placeholder="<?= _te('shops', 'Расскажите как вы связаны с данным магазином') ?>"></textarea>
                    </div>
                    <div class="help-block j-description-maxlength hidden-xs small"></div>
                    <button type="submit" class="btn btn-default"><i class="fa fa-envelope"></i> <?= _t('shops', 'Отправить заявку') ?></button>
                    <div class="clearfix"></div>
                  </form>
                </div>
            </div>
            <? } ?>
            <?php if (DEVICE_PHONE) { ?>
            <div class="l-content-box-in">
              <?= // Owner block
                View::template('view.owner', $aData, 'shops');
              ?>
            </div>
            <? } ?>
            <? if($has_owner) { ?>
            <ul class="nav nav-tabs has-first">
              <? foreach($tabs as $v) { ?>
              <li<? if($v['a']){ ?> class="active"<? } ?>><a href="<?= $v['url'] ?>"><?= $v['t'] ?></a></li>
              <? } ?>
            </ul>
            <? } # has_owner ?>
            <div class="pd15">
              <? // bbs/shop.items.php
              echo $content ?>
            </div>
          </div><!-- /.l-content-box-left -->
          <?php if (DEVICE_DESKTOP_OR_TABLET) { ?>
          <div class="l-content-box-sidebar">
            <?= // Owner block
              View::template('view.owner', $aData, 'shops');
            ?>
            <? if ($bannerRight = Banners::view('shops_view_right', array('region'=>$shop['reg3_city']))) { ?>
              <div class="l-content-box-in">
                <div class="l-banner-v">
                <?= $bannerRight ?>
                </div>
              </div>
            <? } ?>
          </div><!-- /.l-content-box-sidebar -->
          <?php } ?>
        </div><!-- /.l-content-box-cols -->
      </div><!-- /.l-content-box -->
    </div><!-- /.container-mobile -->
  </div><!-- /.container -->
</div><!-- /.l-content -->


<div class="l-mainLayout" >

  <!-- Content -->
  <div class="l-mainLayout-content has-sidebar">
    
    

    

    

  </div><!-- /.l-mainLayout-content -->
  
  <? if (DEVICE_DESKTOP_OR_TABLET) { ?>
  <!-- Sidebar -->
  <div class="l-mainLayout-sidebar">

    

  </div><!-- /.l-mainLayout-sidebar -->
  <? } # DEVICE_DESKTOP_OR_TABLET ?>

</div><!-- /.l-mainLayout -->

<script type="text/javascript">
  <? js::start(); ?>
  $(function(){
    jShopsView.init(<?= func::php2js(array(
      'lang'=>array(
        'request' => array(
          'success' => _t('shops', 'Ваша заявка была успешно отправлена'),
          'maxlength_symbols_left' => _t('', '[symbols] осталось'),
          'maxlength_symbols' => _t('', 'знак;знака;знаков'),
          ),
        'sendfriend'=>array(
          'email' => _t('','E-mail адрес указан некорректно'),
          'success' => _t('','Сообщение было успешно отправлено'),
          ),
        'claim' => array(
          'reason_checks' => _t('shops','Укажите причину жалобы'),
          'reason_other' => _t('shops','Опишите причину подробнее'),
          'captcha' => _t('','Введите результат с картинки'),
          'success' => _t('shops','Жалоба успешно принята'),
          ),
        ),
      'id'=>$shop['id'], 'ex'=>$shop['id_ex'].'-'.$shop['id'],
      'claim_other_id'=>Shops::CLAIM_OTHER,
      'addr_map' => ($shop['addr_map']),
      'addr_lat' => $shop['addr_lat'],
      'addr_lon' => $shop['addr_lon'],
      'request_url' => Shops::url('request'),
      )) ?>);
  });
  <? js::stop(); ?>
</script>