<?php
/**
 * Помощь: просмотр вопроса
 * @var $this Help
 * @var $breadCrumbs array хлебные крошки
 * @var $title string заголовок
 * @var $content string содержание (HTML)
 * @var $questions_other array список похожих вопросов
 */
?>

<?= tpl::getBreadcrumbs($breadCrumbs); ?>

<div class="l-content">
  <div class="container">
    <div class="l-pageHeading">
      <h1 class="l-pageHeading-title"><?= $title ?></h1>
    </div>

    <div class="container-mobile">
      <div class="l-content-box">
        <div class="l-content-box-in l-content-text">
          <?= $content ?>
        </div>
      </div>
    </div>
    <? if ( ! empty($questions_other)) { ?>
    <div class="hl-list-category mrgt30">
      <h2 class="hl-list-title"><?= _t('help', 'Другие вопросы из этого раздела') ?></h2>
      <ul class="hl-list-items">
        <? foreach($questions_other as $v) { ?>
        <li><a href="<?= $v['link'] ?>"><?= $v['title'] ?></a></li>
        <? } ?>
      </ul>
    </div>
    <? } ?>
  </div>
</div>