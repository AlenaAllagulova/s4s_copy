<?php

class Theme_Nelson_new_t1e1366 extends Theme
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'extension_id'  => 't1e13665920c86c5031b0b2d579042906cf8b4de',
            'theme_title'   => 'Nelson2',
            'theme_version' => '1.0.0',
            'theme_parent'  => 'nelson_t001e26',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            //
        ));
        
        $this->cssEdit(array(
            static::CSS_FILE_CUSTOM => ['path' => $this->path('/static/css/custom.css', false), 'save' => true],
        ));
    }

    /**
     * Запуск темы
     */
    protected function start()
    {
        tpl::includeJS('site.index', false, 1);
             $this->css('css/owl.carousel.css');
    
        $this->js('js/site.index.js');
        $this->js('js/owl.carousel.min.js');
        # Логотипы:
        bff::hookAdd('site.logo.url.header', function($url){
            $logo = $this->configImages('logo', 'view');
            return (!empty($logo) ? $logo : $url);
        });
        bff::hookAdd('site.logo.url.header.short', function($url){
            $logo = $this->configImages('logo.short', 'view');
            return (!empty($logo) ? $logo : $url);
        });
    }
}