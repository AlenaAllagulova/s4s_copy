<?php
/**
 * Постраничная навигация: стандартный вид
 * @var $pages array ссылки на страницы
 * @var $first array первая страница
 * @var $last array последняя страница
 * @var $total integer общее кол-во страниц
 * @var $settings array дополнительные настройки
 */

# данные отсутствуют (общее их кол-во == 0)
if( $total <= 1 || ! $settings['pages'] ) return;

?>
<!-- START pagination.standart -->
<div class="pagination-wrap">
  <? if( $settings['pageto'] ) { ?>
  <form onsubmit="return false;" class="form-inline pager-input hidden-xs">
    <div class="form-group">
      <label for="page-num"><?= _t('', 'Перейти на страницу') ?></label>
      <input type="text" id="page-num" class="j-pgn-goto form-control" placeholder="№">
    </div>
  </form>
  <? } ?>

  <div class="j-pgn-pages">
    <ul class="pagination">
      <li<? if(!$prev) { ?> class="disabled"<? } ?>><a<? if($prev){ echo $prev; } else { ?> href="javascript:void(0);"<? } ?>><i class="fa fa-angle-left"></i> <?= _t('pgn', 'Предыдущая') ?></a></li>
      <? if($first) { ?><li class="hidden-xs"><a<?= $first['attr'] ?>><?= $first['page'] ?></a></li><li><a<?= $first['dots'] ?>>...</a></li><? } ?>
      <? foreach($pages as $v) { ?>
      <li class="hidden-xs<? if($v['active']){ ?> active<? } ?>"><a<?= $v['attr'] ?>><?= $v['page'] ?></a></li>
      <? } ?>
      <? if($last) { ?><li class="hidden-xs"><a<?= $last['dots'] ?>>...</a></li><? if ($settings['pagelast']) { ?><li><a<?= $last['attr'] ?>><?= $last['page'] ?></a></li><? } } ?>
      <li<? if(!$next) { ?> class="disabled"<? } ?>><a<? if($next){ echo $next; } else { ?> href="javascript:void(0);"<? } ?>><?= _t('pgn', 'Следующая') ?> <i class="fa fa-angle-right"></i></a></li>
    </ul>
  </div>
</div>

<!-- END pagination.standart -->