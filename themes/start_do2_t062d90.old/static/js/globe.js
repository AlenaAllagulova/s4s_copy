var width = 700,
    height = 700,
    sens = 0.5,
    rotate = [-80, -10],
    velocity = [.003, 0],
    scale0 = (width - 1) / 0.8 / Math.PI,
    time = Date.now(),
    mousenter = false,
    focused,
    clickDuration = 0;

//Setting projection
var aspect = width / height,
    chart = d3.select('#chart');




var projection = d3.geo.orthographic()
    .scale(245)
    .rotate([170, 0])
    .translate([width / 2, height / 2])
    .clipAngle(90);





var path = d3.geo.path()
    .projection(projection);
//Setup zoom behavior
var zoom = d3.behavior.zoom()
//  .translate([width / 2, height / 2])
//a    .scale(scale0)
//a    .scaleExtent([scale0, 8 * scale0])
//al    .on("zoom", zoomed);
//SVG container

var svg = d3.select("#chart").append("svg")
    .attr("width", width)
    .attr("height", height);

//Adding water

svg
    .call(zoom)
    .call(zoom.event);


svg.append("path")
    .datum({type: "Sphere"})
    .attr("class", "water")
    .attr("d", path);

function zoomed() {
    projection
    // .translate(zoom.translate())
    //a    .scale(zoom.scale());

    svg.selectAll("path")
        .attr("d", path);
}

var countryTooltip = d3.select("body").append("div").attr("class", "countryTooltip"),
    countryList = d3.select(".select_w").append("select").attr("name", "countries").attr('class', 'country-select');


queue()
    .defer(d3.json, "https://s4s.ru/world-110m.json")
    .defer(d3.csv, "https://s4s.ru/world-110m-country-names.csv")
    .await(ready);

//Main function

function ready(error, world, countryData) {


    var countryById = {},
        countries = topojson.feature(world, world.objects.countries).features;

    countryData.forEach(function(d) {

        countryById[d.id] = d.name;
        var option = countryList.append("option");
        option.text(d.name);
        option.property("value", d.id);

    });


    //Adding countries to select

    //Drawing countries on the globe
    var world = svg.selectAll("path.land")
        .data(countries)
        .enter().append("path")
        .attr("class", "land")
        .attr("d", path)
        .attr("id", function(d,i){ return 'land' + i;})


        //Drag event

        .call(d3.behavior.drag()
            .origin(function() { var r = projection.rotate(); return {x: r[0] / sens, y: -r[1] / sens}; })
            .on('dragstart', function() {
                mousenter = true;
            })
            .on('dragend', function(d) {
                console.log(d);
                clickDuration = 0;

                setTimeout( function() {
                    mousenter = false;
                }, 100);


                if(countryById[d.id] == 'Russian Federation') {
                    console.log('russia');
                    return false;
                }
            })
            .on("drag", function(d) {
                clickDuration++;
                console.log(clickDuration);
                rotate = projection.rotate();
                projection.rotate([d3.event.x * sens, -d3.event.y * sens, rotate[2]]);
                svg.selectAll("path.land").attr("d", path);
                svg.selectAll(".focused").classed("focused", focused = false);
            }))
        .on('mouseup', function(d) {
            console.log('pppp');
            if(countryById[d.id] == 'Russian Federation' && clickDuration < 1) {
                $('.flex_container').hide();
                $('.map_wrapper').show();
            } else {
                return false;
            }
        })

        //Mouse events

        .on("mouseover", function(d) {
            countryTooltip.html(countryById[d.id])
                .style("left", (d3.event.pageX + 7) + "px")
                .style("top", (d3.event.pageY - 15) + "px")
                .style("display", "block")
                .style("opacity", 1);

        })
        .on("mouseout", function(d) {
            countryTooltip.style("opacity", 0)
                .style("display", "none");
        })
        .on("mousemove", function(d) {
            countryTooltip.style("left", (d3.event.pageX + 7) + "px")
                .style("top", (d3.event.pageY - 15) + "px");
        });


    $('.show_globe').on('click', function(){
        $('.flex_container').show();
        $('.map_wrapper').hide()

    });




    //Country focus on option select

    d3.select("select").on("change", function() {

        var focusedCountry = country(countries, this),
        p = d3.geo.centroid(focusedCountry);
        projection.rotate(rotate);

        svg.selectAll(".focused").classed("focused", focused = false);

        // Globe rotating

        (function transition() {
            d3.transition()
                .duration(2500)
                .tween("rotate", function() {
                    var r = d3.interpolate(projection.rotate(), [-p[0], -p[1]]);
                    return function(t) {
                        projection.rotate(r(t));
                        svg.selectAll("path").attr("d", path)
                            .classed("focused", function(d, i) { return d.id == focusedCountry.id ? focused = d : false; });
                    };
                })
        })();
    });

    function country(cnt, sel) {
        for(var i = 0, l = cnt.length; i < l; i++) {
            if(cnt[i].id == sel.value) {return cnt[i];}
        }
    };




    var feature = svg.selectAll("path");
    function globeRotate() {
       d3.timer(function () {
           if (mousenter) {
               console.log('mousenter');
               return false;
           }
            var dt = Date.now() - time;
            projection.rotate([rotate[0] + velocity[0] * dt, rotate[1] + velocity[1] * dt]);
            feature.attr("d", path);

        });


    }

    globeRotate();

};