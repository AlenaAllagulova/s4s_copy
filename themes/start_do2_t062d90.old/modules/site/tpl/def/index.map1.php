<?php
/**
 * Главная страница
 * @var $this Site
 * @var $titleh1 string заголовок H1
 * @var $centerBlock string центральный блок
 * @var $last string блок последних / премиум объявлений (HTML)
 * @var $seotext string SEO-текст
 */
?>

<div class="row">

    <div class="col-md-8">
        <?php if (empty($map)) { ?>
            <div class="index-map__nomap"><?= _t('site','Для данного региона карта еще недоступна.') ?></div>
        <?php } else { ?>

            <div class="select_w"></div>
            <div class="flex_container">
                <div class="globe_container" style="width:700px; height: 700px;">
                    <svg id="chart"
                         preserveAspectRatio="xMidYMid meet">
                    </svg>
                </div>
            </div>
            <div id="map_russia" class="map_wrapper">
                <?= $map ?>
                <button  class="show_globe btn btn-primary"><?= _t('site','Показать глобус') ?></button>
            </div>

        <?php } ?>

    </div>



    <div class="col-md-4">
    
        <?php if($banner = Banners::view('site_index_right')) { ?>
            <div class="l-banner-v">
                <?= $banner; ?>
            </div>
        <?php } ?>


    </div>



