<?php
/**
 * Восстановление пароля: Шаг 1
 * @var $this Users
 * @var $social integer инициировано ли восстановление на этапе авторизации через соц. сеть
 */
?>

<div class="container_sm">
  <div class="l-content-box">
    <div class="l-content-box-cols">
      <div class="l-content-box-left">
        <div class="l-content-box-in">
          <form action="" id="j-u-forgot-start-form-<?= bff::DEVICE_DESKTOP ?>" class="form-horizontal">
            <input type="hidden" name="social" value="<?= $social ?>" />
            <div class="form-group">
              <label for="j-u-forgot-start-desktop-email" class="col-md-3 col-sm-4 control-label"><?= _t('users', 'Электронная почта') ?></label>
              <div class="col-md-6 col-sm-8">
                <input class="form-control j-required" type="email" name="email" id="j-u-forgot-start-desktop-email" placeholder="<?= _te('users', 'Введите ваш email') ?>" maxlength="100" autocorrect="off" autocapitalize="off" />
              </div>
            </div>
            <div class="row">
              <div class="col-md-offset-3 col-sm-offset-4 col-md-9 col-sm-8">
                <button type="submit" class="btn btn-success"><?= _t('users', 'Восстановить пароль') ?></button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="l-content-box-sidebar">
        <div class="l-content-box-in l-content-box-in_md text-center">
          <div class="l-content-box-title">
            <?= _t('users', 'Впервые на нашем сайте?') ?>
          </div>
          <a href="<?= Users::url('register') ?>" class="btn btn-primary"><?= _t('users', 'Зарегистрируйтесь') ?></a>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  <? js::start(); ?>
  $(function(){
    jUserAuth.forgotStart(<?= func::php2js(array(
      'lang' => array(
        'email' => _t('users', 'E-mail адрес указан некорректно'),
        'success' => _t('users', 'На ваш электронный ящик были высланы инструкции по смене пароля.'),
        ),
      )) ?>);
  });
  <? js::stop(); ?>
</script>