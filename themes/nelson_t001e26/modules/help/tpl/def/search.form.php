<?php
/**
 * Помощь: форма поиска
 * @var $this Help
 * @var $f array параметры фильтра
 */

extract($f, EXTR_REFS | EXTR_PREFIX_ALL, 'f');

?>

<div class="l-filter">
  <noindex>
    <form id="j-f-form" action="<?= Help::url('search') ?>" method="get">
      <? if(bff::$event == 'search') { ?><input type="hidden" name="page" value="<?= $f['page'] ?>" /><? } ?>
      <div class="l-filter-form-wrap">
        <div class="l-filter-form">
          <div class="l-filter-form-search dropdown">
            <input type="text" name="q" id="j-f-query" class="l-filter-form-input" placeholder="<?= _te('help','Поиск вопросов...') ?>" autocomplete="off" value="<?= HTML::escape($f_q) ?>" maxlength="80">
            <div id="j-search-quick-dd" class="l-filter-qsearch dropdown-menu">
              <div class="f-qsearch__results j-search-quick-dd-list"></div>
            </div>
          </div>
          <div class="l-filter-form-item">
            <button type="submit" class="l-filter-form-submit j-submit"><i class="fa fa-search"></i> <?= _t('help','Найти') ?></button>
          </div>
        </div>
      </div>
    </form>
  </noindex>
</div><!-- /.l-filter -->