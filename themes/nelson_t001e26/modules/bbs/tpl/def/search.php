<?php

    /**
     * Поиск объявлений: layout
     * @var $this BBS
     * @var $f array параметры фильтра
     * @var $listTypes array доступные типы списка
     * @var $sortTypes array доступные типы сортировки
     * @var $isMap boolean текущий вид тип списка - карта
     * @var $filterVertical boolean включен вертикальный фильтр
     * @var $cat array параметры текущей категории
     * @var $items array данные объявлений
     * @var $rss array параметры RSS ссылки
     * @var $catsBlock string блок подкатегорий (HTML)
     * @var $premiumBlock string блок премиум-объявлений (HTML)
     * @var $relinkBlock string блок перелинковки (HTML)
     */

    tpl::includeJS(array('history'), true);
    if ($isMap) {
        Geo::mapsAPI(false);
        if (Geo::mapsType() == Geo::MAPS_TYPE_GOOGLE) {
            tpl::includeJS('markerclusterer/markerclusterer', false);
        }
    }

    extract($f, EXTR_REFS | EXTR_PREFIX_ALL, 'f');

    $rightBanner = Banners::view('bbs_search_right', array('cat'=>$cat['id'], 'region'=>$f['region'])); # Баннер (справа)
    $rightBlock = $filterVertical || $rightBanner || ! empty($premiumBlock);
    $isMapVertical = false;
    if ($rightBlock && $isMap) {
        $isMapVertical = true;
        $isMap = false;
        config::set('bbs-map-vertical', true);
    }
?>

<?= tpl::getBreadcrumbs($cat['crumbs'], false, 'breadcrumb'); ?>

<div class="l-content l-content_sm">
  <div class="container">
    <?= $catsBlock ?>

    <div class="l-mainLayout">

      <!-- Content -->
      <div class="l-mainLayout-content<? if (DEVICE_DESKTOP_OR_TABLET && $rightBlock) {  ?> has-sidebar<? } ?>">
        <div id="j-bbs-search-list">
          <div class="l-pageHeading">
            <h1 class="l-pageHeading-title"><?= ( $f_c > 0 ? $cat['titleh1'] : ( ! empty($f_q) ? _t('search', 'Результаты поиска по запросу "[query]"', array('query'=>$f_q)) : (!empty($cat['titleh1']) ? $cat['titleh1'] : _t('search', 'Поиск объявлений')) ) ) ?></h1>
          </div><!-- ./l-pageHeading -->
          
          <div class="sr-listTop">
            <? if( sizeof($cat['types']) > 1 ) { ?>
              <ul class="sr-listTop-item sr-listTop-item_sm">
                <? foreach($cat['types'] as $k=>$v) { ?>
                <li class="<? if($k == $f_ct) { ?>active<? } ?>">
                  <a href="javascript:void(0);" class="j-f-cattype-desktop sr-listTop-btn" data="{id:<?= $v['id'] ?>,title:'<?= HTML::escape($v['title'], 'js') ?>'}" data-id="<?= $v['id'] ?>">
                  <?= $v['title'] ?>
                  </a></li>
                <? } ?>
              </ul>
            <? } ?>
            <?
            // List View
            if( ! empty($items) ) { ?>
            <div class="sr-listTop-item" id="j-f-listtype">
              <? foreach($listTypes as $k=>$v) {
                ?><a href="javascript:void(0);" data="{id:<?= $k ?>}" data-id="<?= $k ?>" class="sr-listTop-btn j-type<? if($v['a']){ ?> active<? } ?>"><i class="<?= $v['i'] ?>"></i> <span class="visible-lg-inline"><?= $v['t'] ?></span></a><?
              } ?>
            </div>
            <? } ?>
            <?
            // List Sort
            if( sizeof($sortTypes) > 1 && DEVICE_DESKTOP_OR_TABLET ) { ?>
            <div class="sr-listTop-item sr-listTop-item_sm sr-listTop-item_right hidden-xs">
              <span class="dropdown inline-block">
                <a class="sr-listTop-btn dropdown-toggle" id="j-f-sort-dd-link" data-current="<?= $f_sort ?>" href="javascript:void(0);">
                  <span class="lnk"><?= $sortTypes[$f_sort]['t'] ?></span> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu pull-right" id="j-f-sort-dd">
                  <? foreach($sortTypes as $k=>$v) { ?><li><a href="javascript:void(0);" class="j-f-sort" data="{key:'<?= $k ?>',title:'<?= HTML::escape($v['t'], 'js') ?>'}"><?= $v['t'] ?></a></li><? } ?>
                </ul>
              </span>
            </div>
            <? } ?>
          </div><!-- /.sr-listTop -->

          <? if($filterVertical && DEVICE_PHONE) { ?>
          <div class="l-filterAside-m">
            <button class="btn btn-default btn-block l-filterAside-m-toggle collapsed" data-toggle="collapse" data-target="#j-filter-vertical-tablet"><i class="fa fa-cog"></i> <?= _t('filter', 'Фильтр'); ?></button>
            <div class="l-filterAside collapse" id="j-filter-vertical-tablet">
              <?= $filterVerticalBlock; ?>
            </div>
          </div>
          <? } ?>
          
          <!-- Search Results -->
          <div class="j-list-<?= bff::DEVICE_DESKTOP ?> j-list-<?= bff::DEVICE_TABLET ?> j-list-<?= bff::DEVICE_PHONE ?>">
            <?= $this->searchList(bff::DEVICE_DESKTOP, $f_lt, $items, array('numStart' => $num_start, 'showBanners' => true)); ?>
          </div>
          <? if ( ! empty($rss) && DEVICE_DESKTOP) { ?>
            <!-- RSS Subscription -->
            <div class="rssSubscribe hidden-xs">
              <a href="<?= $rss['link'] ?>" class="ico" target="_blank" rel="nofollow"><i class="fa fa-rss"></i> <span><?= _t('bbs', 'Подписка через RSS на "[title]"', array('title' => $rss['title'])); ?></span></a>
            </div>
          <? } ?>
          <!-- Pagination -->
          <div id="j-bbs-search-pgn">
            <?= $pgn ?>
          </div>
        </div> <!-- /#j-bbs-search-list -->
        
      </div><!-- /.l-mainLayout-content -->

      <? if (DEVICE_DESKTOP_OR_TABLET && $rightBlock) {  ?>
        <!-- Sidebar -->
        <div class="l-mainLayout-sidebar">
          <? if ($filterVertical) { ?>
            <div class="l-pageHeading">
              <div class="l-pageHeading-title"><?= _t('filter','Фильтр') ?></div>
            </div>
            <div class="l-filterAside" id="j-filter-vertical-desktop">
              <?= $filterVerticalBlock; ?>
            </div>
          <? $filterVerticalBlock = ''; } ?>
          <?= ! empty($premiumBlock) ? $premiumBlock : '' ?>
          <? if ($rightBanner) { ?>
          <div class="l-banner-v">
            <?= $rightBanner ?>
          </div>
          <? } ?>
        </div>
      <? } ?>

    </div><!-- /.l-mainLayout -->

    <? if(DEVICE_DESKTOP_OR_TABLET) {
      // bbs/search.relink.block.php 
      echo $relinkBlock;
    } ?>
    <div class="l-info">
      <? if($f['page'] <= 1 && ! empty($cat['seotext'])) echo $cat['seotext'] ?>
    </div>

  </div><!-- /.container -->
</div><!-- /.l-content -->

<script type="text/javascript">
<? js::start(); ?>
    $(function(){
        jBBSSearch.init(<?= func::php2js(array(
            'lang'=>array(
                'range_from' => _t('filter','от'),
                'range_to'   => _t('filter','до'),
                'btn_reset'  => _t('filter','Не важно'),
                'map_toggle_open' => _t('search', 'больше карты'),
                'map_toggle_close' => _t('search', 'меньше карты'),
                'metro_declension' => _t('filter','станция;станции;станций'),
            ),
            'cattype'  => $cat['types'],
            'cattype_ex' => BBS::CATS_TYPES_EX,
            'listtype' => $listTypes,
            'sort'     => $sortTypes,
            'items'    => ( $isMap || $isMapVertical ? $items : array() ),
            'defaultCoords' => Geo::mapDefaultCoords(true),
            'isVertical' => $filterVertical,
            'isMapVertical' => $isMapVertical,
            'ajax'     => (bff::isIndex() ? false : true),
            'filterDropdownMargin' => 5,
        )) ?>);
    });
<? js::stop(); ?>
</script>
<?

# актуализируем данные формы поиска
# формируемой позже в фаблоне /tpl/filter.php
$this->searchFormData($f);