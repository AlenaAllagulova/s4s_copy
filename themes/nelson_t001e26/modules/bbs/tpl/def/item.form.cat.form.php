<?php
/**
 * Форма объявления: добавление / редактирование - параметры формы зависимые от настроек категории
 * @var $this BBS
 * @var $edit boolean редактирование (true), добавление (false)
 * @var $item array данные об объявлении
 * @var $types array типы объявления (продам/куплю)
 * @var $price boolean цена true/false
 * @var $price_label string название поля "цена"
 * @var $price_sett array настройки цены
 * @var $price_curr_selected integer ID текущей выбранной валюты цены
 * @var $dp string HTML блок дин. свойств
 */

# Тип размещения ("Тип объявления") ?>
<div class="form-group" <? if( empty($types) || sizeof($types) == 1) { ?>style="display: none;"<? } ?>>
  <label class="col-sm-3 control-label"><?= _t('item-form', 'Тип объявления') ?><span class="required-mark">*</span></label>
  <div class="col-md-6 col-sm-9">
    <?  if( is_array($types) ) {
     foreach($types as $v) { ?>
     <div class="radio-inline">
       <label><input name="cat_type" value="<?= $v['id'] ?>" type="radio" class="j-cat-type j-required" <? if( $item['cat_type'] == $v['id'] ) { ?> checked="checked"<? } ?> /> <?= $v['title'] ?></label>
     </div>
     
     <? }
   } ?>
 </div>
</div>
<?

# Цена
if( ! empty($price) ) { ?>
<div class="form-group j-price-block">
  <label class="col-sm-3 control-label"><?= $price_label ?><span class="required-mark">*</span></label>
  <div class="col-md-6 col-sm-9">
    <? if( $price_sett['ex'] > 0 ) { ?>
    <? if($price_sett['ex'] & BBS::PRICE_EX_AGREED) { ?>
      <div class="radio">
        <label>
          <input class="j-price-var" type="radio" name="price_ex[1]" value="<?= BBS::PRICE_EX_AGREED ?>" <? if($item['price_ex'] & BBS::PRICE_EX_AGREED) { ?> checked="checked" <? } ?> /> <?= _t('item-form', 'Договорная') ?>
        </label>
      </div>
    <? } ?>
    <? if($price_sett['ex'] & BBS::PRICE_EX_FREE) { ?>
      <div class="radio">
        <label>
          <input class="j-price-var" type="radio" name="price_ex[1]" value="<?= BBS::PRICE_EX_FREE ?>" <? if($item['price_ex'] & BBS::PRICE_EX_FREE) { ?> checked="checked" <? } ?> /> <?= _t('item-form', 'Бесплатно') ?>
        </label>
      </div>
    <? } ?>
    <? if($price_sett['ex'] & BBS::PRICE_EX_EXCHANGE) { ?>
      <div class="radio">
        <label>
          <input class="j-price-var" type="radio" name="price_ex[1]" value="<?= BBS::PRICE_EX_EXCHANGE ?>" <? if($item['price_ex'] & BBS::PRICE_EX_EXCHANGE) { ?> checked="checked" <? } ?> /> <?= _t('item-form', 'Обмен') ?>
        </label>
      </div>
    <? } ?>
    <div class="radio"<? if($price_sett['ex'] == BBS::PRICE_EX_MOD) { ?> style="display: none;" <? } ?>>
      <label>
      <input type="radio" name="price_ex[1]" value="<?= BBS::PRICE_EX_PRICE ?>" <? if($item['price_ex'] <= BBS::PRICE_EX_MOD) { ?> checked="checked" <? } ?> /> <?= _t('item-form', 'Цена') ?>&nbsp;
      </label>
    </div>
    <? } ?>
    
    <div class="mrgt5">
      <input type="text" name="price" class="form-control input-inline j-price<? if( ! $price_sett['ex'] ){ ?> j-required<? } ?>" value="<?= $item['price'] ?>" pattern="[0-9\.,]*" />
      <select name="price_curr" class="form-control input-inline"><?= Site::currencyOptions($price_curr_selected) ?></select>
      <? if($price_sett['ex'] & BBS::PRICE_EX_MOD) { ?>
      <div class="checkbox">
        <label>
          <input type="checkbox" name="price_ex[2]" class="j-price-mod" value="<?= BBS::PRICE_EX_MOD ?>" <? if($item['price_ex'] & BBS::PRICE_EX_MOD) { ?> checked="checked" <? } ?> />
          <?= ( ! empty($price_sett['mod_title'][LNG]) ? $price_sett['mod_title'][LNG] : _t('item-form', 'Торг возможен') ) ?>
        </label>
      </div>
    <? } ?>
    </div>

  </div>
</div>
<? }

# Дин.свойства
if( ! empty($dp) ) {
  echo $dp;
}