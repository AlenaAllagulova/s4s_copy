// Filter Position in LocalStorage
$('#j-f-desktop').collapse().on('hidden.bs.collapse', function () {
  $('.l-filter-form-toggle').addClass('collapsed');
  localStorage[this.id] = 'true';
}).on('shown.bs.collapse', function () {
  $('.l-filter-form-toggle').removeClass('collapsed');
  localStorage.removeItem(this.id);
}).each(function () {
  if (localStorage[this.id] === 'true') {
    $(this).removeClass('in');
    $('.l-filter-form-toggle').addClass('collapsed');
  }
});

$(function () {

  // Mega Dropdown
  $(document).on('click', '.mega-dropdown', function (e) {
    e.stopPropagation();
  });

  // Sticky Footer
  $(window).on('load resize', function () {
    var footerHeight = $('.j-footer').height();
    $('.page-wrap').css({"margin-bottom": -footerHeight - 1});
    $('.page-wrap-after').css({"height": footerHeight});
  });

  // Tooltips and popovers
  $('.has-tooltip').tooltip();
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();

});