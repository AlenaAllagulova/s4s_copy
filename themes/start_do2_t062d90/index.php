<?php

class Theme_Start_do2_t062d90 extends Theme
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'theme_title'   => 'Стартовая тема',
            'theme_version' => '2.4.2.1',
            'extension_id'  => 't062d9070171056ece3322974c559def61ca11ff',
        ));

        # Настройки логотипа:
        $this->siteLogo([
            'header' => ['config.key'=>'logo'],
            'header.short' => ['config.key'=>'logo.short'],
        ], true);

        # Доступный вид главной:
        $this->siteIndexTemplates([
            'index.default',
            'index.regions',
            'index.map1',
        ]);

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            'device.desktop.responsive' => array(
                'input' => 'sys',
                'default' => false,
            ),
            'bbs.search.filter.vertical' => array(
                'title' => _t('bbs','Вертикальный фильтр в списках'),
                'input' => 'select',
                'type' => TYPE_BOOL,
                'default' => false,
                'options' => array(
                    true => array('title' => _t('','включено')),
                    false => array('title' => _t('','выключено')),
                ),
            ),
            'bbs.index.subcats.limit' => array(
                'title' => _t('bbs','Подкатегории на главной'),
                'description' => _t('bbs','Кол-во видимых подкатегорий на главной'),
                'input' => 'number',
                'default' => 5,
                'min' => 1,
                'max' => 1000,
            ),
            'bbs.index.last.limit' => array(
                'title' => _t('bbs','Блок объявлений на главной'),
                'description' => _t('bbs','Кол-во объявлений в блоке, [num] - скрыть блок', array('num'=>0)),
                'input' => 'number',
                'default' => 10,
                'min' => 0,
                'max' => 30,
            ),
        ));

# ignore-start
        bff::hooksBulk([
            'extensions.themes.'.$this->getName().'.settings.tabs' => function($tabs){
                $tabs['copy-start'] = array(
                    'title' => 'Копирование',
                    'priority' => 2,
                );
                return $tabs;
            },
            'extensions.themes.'.$this->getName().'.settings.tabs.content' => function($data){
                $data = array();
                echo $this->viewPHP($data, 'ignore/copy.form');
            },
            'extensions.themes.'.$this->getName().'.settings.submit' => function($data){
                $isSubmit = $this->input->postget('copy_submit', TYPE_UINT);
                if (empty($isSubmit)) { return; }
                $response = array();
                do {
                    if (!is_writable(PATH_THEMES)) {
                        $this->errors->set('Недостаточно прав для записи в директорию "[dir]"', ['dir'=>str_replace(PATH_BASE,DS,PATH_THEMES)]);
                        break;
                    }
                    $title = $this->input->post('copy_title', TYPE_TEXT);
                    $name = $this->input->post('copy_name', TYPE_NOTAGS);
                    $type = $this->getExtensionType();
                    \bff::dev()->createExtension($title, $name, $type, $this);
                    $response['redirect'] = tplAdmin::adminLink(bff::$event.'&type='.$type);
                } while(false);
                $this->ajaxResponseForm($response);
            }
        ]);
# ignore-stop

        $this->cssEdit([
            static::CSS_FILE_MAIN => ['path' => $this->path('/static/css/main.css', false), 'save' => false],
            static::CSS_FILE_CUSTOM => ['path' => $this->path('/static/css/custom.css', false), 'save' => 'custom'],
        ]);
    }

    /**
     * Запуск темы
     */
    protected function start()
    {
     tpl::includeJS('site.index', false, 1);
             $this->css('css/suggestions.min.css');
    
	$this->js('js/theme.js');
        $this->js('js/jquery.xdomainrequest.min.js');
        $this->js('js/jquery.suggestions.min.js');
    }
}