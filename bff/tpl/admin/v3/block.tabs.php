<?php
/**
 * @var $tabs array
 * @var $opts array
 */
 $attrBlock = array('class'=>'l-box-tabs-wrapper j-tabs');
 if (sizeof($tabs) <= 1 && empty($opts['buttons'])) {
    $attrBlock['style'] = 'display:none;';
 }
 if ( ! empty($opts['attr.block'])) {
    foreach ($opts['attr.block'] as $k=>$v) {
        $attrBlock[$k] = ( isset($attrBlock[$k]) ? $attrBlock[$k] . ' ' . $v : $v );
    }
 }
 if (!isset($opts['attr'])) {
    $opts['attr'] = false;
 }
?>
<div<?= HTML::attributes($attrBlock); ?>>
    <? if (!empty($opts['buttons']) && DEVICE_DESKTOP) { ?>
    <div class="pull-right hidden-xs<?= empty($tabs) ? ' mrgb10' : '' ?>">
        <?php foreach ($opts['buttons'] as $v): ?>
            <a<?= HTML::attributes($v, array('class'=>'btn btn-default btn-sm'), array('icon','title')) ?>><?php if (isset($v['icon'])) { ?><i class="<?= $v['icon'] ?>"></i><? } ?> <?= (isset($v['title']) ? $v['title'] : '') ?></a>
        <?php endforeach; ?>
    </div>
    <?php }
    $activeTitle = '';
    ob_start(); ob_implicit_flush(false); ?>
        <ul class="<?= DEVICE_DESKTOP ? 'l-box-tabs' : 'dropdown-menu' ?> j-box-tabs">
            <?php foreach ($tabs as $k=>$v):
                if (isset($opts['active'])) {
                    if (is_scalar($opts['active'])) {
                        $v['active'] = ($opts['active'] == $k);
                    } else if (is_callable($opts['active'], true)) {
                        $v['active'] = call_user_func($opts['active'], $k, $v);
                    }
                } else {
                    $v['active'] = !empty($v['active']);
                }
                $attr = (is_callable($opts['attr'], true) ? call_user_func($opts['attr'], $k, $v) : $v);
                $attr['data-rotate'] = (!empty($v['rotate']) ? 1 : 0);
            ?>
                <li class="<?php if ($v['active']) { $activeTitle = $v['title']; ?>active<?php } ?>">
                    <a<?= HTML::attributes($attr, array('href'=>'#', 'data-tab'=>$k, 'class'=>'j-tab'), array('title','counter','active')) ?>>
                        <span class="j-tab-title"><?= $v['title'] ?></span><?php if (isset($v['counter'])) { ?> <span class="c-count j-tab-counter"><?= $v['counter'] ?></span><?php } ?>
                    </a>
                    <?php if (isset($opts['edit']) && is_callable($opts['edit'], true)) { echo call_user_func($opts['edit'], $k, $v); } ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php $ulHTML = ob_get_clean(); ?>
    <a href="#" class="l-box-tabs-toggle j-box-tabs-mobile <?= DEVICE_DESKTOP ? 'hidden' : '' ?>" data-toggle="dropdown"><span class="j-box-tabs-active"><?= $activeTitle ?></span> <b class="caret"></b></a>
    <?= $ulHTML; ?>
</div>