<?php
/**
 * Блок: список
 * @var $module string название модуля
 * @var $method string название метода
 * @var $tabs array табы
 * @var $filter array фильтр списка
 * @var $list array список
 * @var $form array форма
 */
if (empty($jsObject)) {
    $jsObject = 'j'.$module.'_'.$method;
}
$cssClass = 'j-'.$module.'-'.$method;
if ( ! isset($action)) {
    $action = bff::input()->getpost('act', TYPE_NOTAGS);
}
if ( ! isset($id)) {
    $id = bff::input()->getpost('id', TYPE_UINT);
}
$rotate = ! empty($rotate);
if ($rotate) {
    tpl::includeJS(array('tablednd'), true);
    if (empty($list['opts']['rotate'])) {
        $list['opts']['rotate'] = true;
    }
}
# tab:
$tab = $list['opts']['tab'] = ($tabs && isset($tabs['opts']['active']) ? $tabs['opts']['active'] : '');
$formTab = ! empty($list['formTab']);
?>
<?php # table only:
if (Request::isAJAX() && ! $formTab) {
    $list['wrap'] = false;
    $list['append'] = ! empty($list['more']) && ! empty($filter['data']['more-count']);
    echo tplAdmin::listTable($list);
    return;
} ?>
<div id="<?= $cssClass ?>-form-block" style="display: none;"><?= ! empty($form) ? $form : '' ?></div>

<?php # list:
    $button = array();
    if (!empty($list['button'])) {
        $button = $list['button'];
        if (!empty($button['action']) && $button['action'] === 'form.add') {
            $button['class'] = 'j-form-url';
            if ( ! isset($button['href'])) {
                $button['href'] = tpl::adminLink($method.'&act=add', $module);
            }
        }
    }
    $cssID = $cssClass.'-list-block';
?>
<?= ! $formTab ? tplAdmin::blockStart((isset($list['lang']['list']) ? $list['lang']['list'] : _t('', 'List')), true, array('id'=>$cssID,'style'=>(!empty($action) && ! empty($form) ? 'display:none;' : '')), $button, (!empty($list['fordev']) ? $list['fordev'] : array())) : '<div id="'.$cssID.'">' ?>
    <?php if ($tabs) { # tabs: ?>
        <?= tplAdmin::tabs(isset($tabs['tabs']) ? $tabs['tabs'] : array(), $tabs['opts']); ?>
    <?php } ?>
    <?php if ($filter) { # filter:
        $filter['tab'] = $tab;
        if ($tabs) {
            if ( ! isset($filter['inputs']['tab'])) {
                $filter['inputs']['tab'] = array('input'=>'hidden', 'attr'=>array('class'=>['j-tab-value']));
            }
        }
        if ( ! isset($filter['inputs']['page'])) {
            $filter['inputs']['page'] = array('input'=>'hidden', 'attr'=>array('class'=>['j-input','j-page-value']));
        }
        if ( ! empty($list['order']) && ! isset($filter['inputs']['order'])) {
            $filter['inputs']['order'] = array('input'=>'hidden', 'attr'=>array('class'=>['j-input','j-order-value']));
        }
        if ( ! empty($list['more'])) {
            $filter['inputs']['more-count'] = array('input'=>'hidden', 'attr'=>array('class'=>['j-input','j-more-counter']));
        }
        echo tplAdmin::listFilter($module, $method, $filter['inputs'], $filter['data'], $filter['opts']);
    } ?>
    <?php if ( ! empty($list['multi'])) { # multi: ?>
    <div class="l-box-filter j-multi-actions" style="display: none;">
        <span class="j-multi-info"></span>&nbsp;&nbsp;
        <?php if (isset($list['multi']['block']) && is_callable($list['multi']['block'], true)) { call_user_func($list['multi']['block']); } ?>
    </div>
    <?php } ?>
    <?php # table:
        $list['wrap'] = true;
        echo tplAdmin::listTable($list);
    ?>
    <?php if ($formTab && isset($list['pgn'])): ?><div class="j-list-pgn"><?= $list['pgn'] ?></div><?php endif; ?>
<?= ! $formTab ? tplAdmin::blockStop(isset($list['pgn']) ? $list['pgn'] : false) : '</div>' ?>
<script type="text/javascript">
    <?php $params = array(
        'tab' => $tab,
        'rotate' => $rotate,
        'form' => array(
            'block'  => '#'.$cssClass.'-form-block',
        ),
    );
    if($formTab) {
        $params['formTab'] = true;
    }
    if (isset($list['more'])) {
        $params['more'] = true;
    }
    if (isset($list['multi']['lang'])) {
        $params['multi']['lang'] = $list['multi']['lang'];
    }
    ?>
    var <?= $jsObject ?> = bff.list('#<?= $cssClass ?>-list-block', <?= func::php2js($params) ?>);
</script>