<?php
/**
 * Блок: список
 * @var $wrap bool полная таблица
 * @var $cols array столбцы
 * @var $row array параметры строки
 * @var $data array данные
 * @var $lang array фразы
 * @var $multi array работа с несколькими записями
 * @var $append boolean выполняется дополнение списка
 * @var $order array настройки сортировки столбцов
 * @var $opts array доп. настройки: tab, rotate
 */
if (empty($cols)) return;

$noData = (!is_array($data) || empty($data));

$rotate = (!empty($opts['rotate']) && !$noData);
if ($useMulti = !empty($multi) && ! $noData) {
    if (!empty($multi['key'])) {
        if (mb_stripos($multi['key'], '[') === false) {
            $multi['key'] .= '[]';
        }
    } else {
        $multi['key'] = 'multi[]';
    }
    if (isset($multi['tab']) && isset($opts['tab'])) {
        $useMulti = is_array($multi['tab']) ? in_array($opts['tab'], $multi['tab']) : $opts['tab'] == $multi['tab'];
    }
}
if ($wrap) {
    ?>
    <div class="l-box-in">
        <table class="table table-condensed table-hover l-table l-table-middle j-list"><?php
}
$colsCounter = 0;
if (empty($append)) {
?>
<thead class="j-list-head">
    <tr class="header<?php if ($rotate) { ?> nodrag nodrop<?php } ?>">
        <?php
            if ($useMulti) {
                ?><th width="20"><label class="checkbox-inline"><input type="checkbox" class="j-multi-all" /></label></th><?php
                $colsCounter++;
            }
            foreach ($cols as $k=>&$v) {
            if (empty($v)) continue;
            # show:
            if (isset($v['tab'])) {
                if (!in_array($opts['tab'], (is_array($v['tab']) ? $v['tab'] : array($v['tab'])))) {
                    continue;
                }
            }
            # attr:
            if (empty($v['attr'])) {
                $v['attr'] = array();
            }
            if (isset($v['width'])) {
                $v['attr']['width'] = $v['width'];
            }
            if (isset($v['align'])) {
                HTML::attributeAdd($v['attr'], 'class', $v['align']);
            }
            $sort = ! $rotate && isset($order['cols'][$k]);
            ?>
            <th<?= HTML::attributes($v['attr']) ?>><?php $colsCounter++;
                if ($sort) {
                    $active = ( ! empty($order['key']) && $order['key'] == $k);
                    if ($active) {
                        $direction = (! empty($order['direction']) && $order['direction'] == 'asc' ? 'desc' : 'asc');
                    }
                    ?><a href="#" class="j-sort" data-id="<?= $k ?>" data-direction="<?= $active ? $direction : $order['cols'][$k] ?>">
                        <?= (isset($v['title']) ? $v['title'] : '') ?><?php
                        if($active) { ?>
                            <i class="fa <?= $direction == 'asc' ? 'fa-caret-down' : 'fa-caret-up' ?>"></i>
                        <?php } ?></a><?php
                } else {
                    echo (isset($v['title']) ? $v['title'] : '');
                }
            ?></th>
        <?php } unset($v); ?>
    </tr>
</thead>
<tbody class="j-list-body"><?php
}
if ( ! $noData) {
    foreach ($data as $k => & $v) {
        $attr = array();
        if (isset($row['attr'])) {
            if (is_array($row['attr'])) {
                $attr = $row['attr'];
            } else {
                if (is_callable($row['attr'], true)) {
                    $attr = call_user_func($row['attr'], $v, $opts);
                }
            }
        }
        if ($rotate) {
            $attr['id'] = 'dnd-'.$v['id'];
        }
        if (isset($more)) {
            HTML::attributeAdd($attr, 'class', 'j-more-item');
        }
        ?><?= isset($row['before']) && is_callable($row['before'], true) ? call_user_func($row['before'], $v, $opts) : '' ?><tr<?= HTML::attributes($attr); ?>>
        <?php
        if ($useMulti) {
            ?><td><label class="checkbox-inline"><input type="checkbox" name="<?= $multi['key'] ?>" value="<?= $v['id'] ?>" class="j-multi-item" /></label></td><?php
        }
        foreach ($cols as & $c) {
            if (empty($c)) continue;
            # show:
            if (isset($c['tab'])) {
                if (!in_array($opts['tab'], (is_array($c['tab']) ? $c['tab'] : array($c['tab'])))) {
                    continue;
                }
            }
            if (!empty($c['render']) && is_callable($c['render'], true)) {
                call_user_func($c['render'], $v, $opts);
            } else {
                ?><td></td><?php
            }
        } unset($c);
        ?></tr><?= isset($row['after']) && is_callable($row['after'], true) ? call_user_func($row['after'], $v, $opts) : '' ?><?php
    } unset($v);
} else { ?>
    <tr class="norecords">
        <td colspan="<?= $colsCounter ?>" style="padding: 25px 0; text-align: center;">
            <?= ( isset($lang['empty']) ? $lang['empty'] : _t('', 'Nothing found') ) ?>
        </td>
    </tr><?php
}
if (empty($append)) {
?>
</tbody>
<?php
}
if ($wrap) {
    ?></table><?php
        if (isset($more)) {
        ?><div class="text-center mrgt20">
            <a href="javascript:" class="btn btn-default <?= empty($more['more']) ? 'hidden' : '' ?> j-more"><?= _t('', 'Show More'); ?> (<?= _t('', 'Total:'); ?><span class="j-total"><?= ! empty($more['total']) ? $more['total'] : '' ?></span>)</a>
        </div><?php }
    ?></div><?php
}