<?php
/**
 * Блок: фильтр списка
 * @var $module string
 * @var $method string
 * @var $filter array
 * @var $data array @ref
 * @var $opts array
 */
 $attrBlock = array('class'=>'l-box-filter j-list-filter');
 $visibleCount = 0;
 foreach ($filter as &$v) {
    if ($v['input'] !== 'hidden') $visibleCount++;
 } unset($v);
 if ( ! $visibleCount) {
    $attrBlock['style'] = 'display:none;';
 }
 if ( ! empty($opts['attr.block'])) {
    foreach ($opts['attr.block'] as $k=>$v) {
        $attrBlock[$k] = ( isset($attrBlock[$k]) ? $attrBlock[$k] . ' ' . $v : $v );
    }
 }
 $noMore = !empty($opts['no-more']) || (\bff::input()->getpost('more', TYPE_UINT) > 0) || ($visibleCount <= 1);
 $rand = func::generator(6);
 $attrMore = function($v, $classes = '') use(& $moreCount, $noMore) {
     $attr = array(
         'class' => $classes
     );
     if (!empty($v['more']) && !$noMore) {
         $attr['class'] .= ' j-more more-hide';
         $moreCount++;
     }
     return HTML::attributes($attr);
 };
?>

<div<?= HTML::attributes($attrBlock); ?>>
    <button type="button" class="l-box-filter-toggle collapsed" data-toggle="collapse" data-target="#filter-collapse-<?= $rand ?>"><?= _t('', 'Filter'); ?></button>

    <div id="filter-collapse-<?= $rand ?>" class="l-box-filter-content collapse">

        <form <?= HTML::attributes((isset($opts['attr.form']) ? $opts['attr.form'] : array('method' => 'get')), array(
                'action' => tplAdmin::adminLink(NULL),
                'method' => 'get',
                'class' => 'form-inline',
            )) ?>>

            <?php if (!empty($module)) { ?>
                <input type="hidden" name="s" value="<?= HTML::escape($module) ?>" class="j-module" />
            <?php } ?>
            <?php if (!empty($method)) { ?>
                <input type="hidden" name="ev" value="<?= HTML::escape($method) ?>" class="j-method" />
            <?php } ?>
            <input type="hidden" name="more" value="<?= ($noMore ? 1 : 0) ?>" class="j-more-state" />

            <?php
            $moreCount = 0;
            foreach($filter as $key=>$v) {
                $content = false;
                $isMore = !empty($v['more']);
                $attributes = array();
                $value = (isset($data[$key]) ? $data[$key] : ( isset($v['value']) ? $v['value'] :''));
                $attr = (isset($v['attr']) ? $v['attr'] : array());
                switch ($v['input']) {
                    case 'hidden': # скрытое поле
                    {
                        $attr = array_merge(array(
                            'type'  => 'hidden',
                            'name'  => $key,
                            'class' => 'j-input',
                            'data-input' => 'hidden',
                            'value' => $value,
                        ), $attr);
                        echo '<input '.HTML::attributes($attr).' />';
                        continue 2; # !
                    } break;
                    case 'text': # однострочный текст
                    {
                        $attr = array_merge(array(
                            'type'  => 'text',
                            'class' => 'form-control input-sm j-input',
                            'data-input' => 'text',
                            'name'  => $key,
                            'value' => $value,
                            'placeholder' => (isset($v['title']) ? $v['title'] : ''),
                        ), $attr);
                        $content = '<input '.HTML::attributes($attr).' />';
                    } break;
                    case 'date': # дата
                    {
                        $attr = array_merge(array(
                            'type'  => 'text',
                            'class' => 'form-control input-sm j-input j-datepicker',
                            'data-input' => 'text',
                            'placeholder' => (isset($v['title']) ? $v['title'] : ''),
                            'name'  => $key,
                            'value' => $value,
                        ), $attr);
                        $content = '<div class="form-group j-list-filter-item"><input '.HTML::attributes($attr).' /></div>';
                        tpl::includeJS('datepicker', true);
                    } break;
                    case 'date-from-to': # дата от-до
                    {
                        if (empty($v['from']['name']) || empty($v['to']['name'])) continue 2;
                        $content = '';
                        if (isset($v['title'])) {
                            ?>
                            <div<?= $attrMore($v, 'form-group j-list-filter-item') ?>><p class="form-control-static"><?= $v['title'] ?></p>
                            <?php
                        }
                        foreach (array($v['from'],$v['to']) as $vv) {
                            $attr2 = array_merge(array(
                                'type'  => 'text',
                                'class' => 'form-control input-sm j-input j-datepicker j-datepicker-clear-btn',
                                'data-input' => 'text',
                                'placeholder' => (isset($vv['title']) ? $vv['title'] : ''),
                                'name'  => $vv['name'],
                                'value' => (isset($data[$vv['name']]) ? $data[$vv['name']] : ( isset($vv['value']) ? $vv['value'] :'')),
                                'style' => 'width:75px;',
                            ), $attr);
                            ?>
                            <div class="form-group relative"><input<?= HTML::attributes($attr2) ?> /></div>
                            <?php
                        }
                        if (isset($v['title'])) { ?></div><?php }
                        tpl::includeJS('datepicker', true);
                        continue 2; # !
                    } break;
                    case 'date-from-to-popup': # дата от-до
                    {
                        if (empty($v['from']['name']) || empty($v['to']['name'])) continue 2;
                        $a = array();
                        foreach (array('from' => $v['from'], 'to' => $v['to']) as $kk => $vv) {
                            $a[$kk] = array_merge(array(
                                'type'  => 'text',
                                'class' => 'form-control input-sm j-input j-datepicker j-datepicker-clear-btn',
                                'data-input' => 'text',
                                'placeholder' => (isset($vv['title']) ? $vv['title'] : ''),
                                'name'  => $vv['name'],
                                'value' => (isset($data[$vv['name']]) ? $data[$vv['name']] : ( isset($vv['value']) ? $vv['value'] :'')),
                            ), $attr);
                        }
                        ?>
                        <div<?= $attrMore($v, 'form-group dropdown j-list-filter-item') ?>>
                            <a href="#" class="btn btn-default btn-sm l-box-filter-btn" data-toggle="dropdown"><?= $v['title'] ?> <b class="caret"></b></a>
                            <div class="dropdown-menu l-box-filter-dropdown mega-dropdown">
                                <div class="dropdown-menu-in">
                                    <div class="form-inline l-box-filter-fromto relative">
                                        <input<?= HTML::attributes($a['from']) ?> />
                                        <span class="hidden-xs">-</span>
                                        <span class="c-spacer10 visible-xs"></span>
                                        <input<?= HTML::attributes($a['to']) ?> />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?
                        tpl::includeJS('datepicker', true);
                        continue 2; # !
                    } break;
                    case 'autocomplete': # autocomplete
                    {
                        if (empty($v['url'])) continue 2;
                        tpl::includeJS('autocomplete', true);
                        $isAutocomplete = true; $rand = mt_rand(1,1000);
                        $classID = 'j-filter-autocomplete-'.$key.'-'.$rand.'-id';
                        $classTitle = 'j-filter-autocomplete-'.$key.'-'.$rand.'-title';
                        $attr = array_merge(array(
                            'type'  => 'text',
                            'id'    => $classTitle,
                            'class' => 'form-control input-sm j-input',
                            'data-input' => 'autocomplete-title',
                            'placeholder' => (isset($v['title']) ? $v['title'] : ''),
                            'value' => ($value > 0 ? (isset($v['value-title']) ? $v['value-title'] : $value) : ''),
                        ), $attr);
                        $content = '<input '.HTML::attributes($attr, array('placeholder'=>(isset($v['title'])?$v['title']:''))).' />';
                        $attr2 = array_merge(array(
                            'type'  => 'hidden',
                            'class' => 'j-input',
                            'data-input' => 'autocomplete-value',
                            'data-text'  => $classTitle,
                            'name'  => $key,
                            'id'    => $classID,
                            'value' => $value,
                        ));
                        $content.= '<input '.HTML::attributes($attr2).' />';
                        $suggest = '{}';
                        if (!empty($v['suggest'])) {
                            if (is_string($v['suggest'])) {
                                $suggest = $v['suggest'];
                            } else if (is_callable($v['suggest'], true)) {
                                $suggest = func::php2js(call_user_func($v['suggest'], $v, $key));
                            } else if (is_array($v['suggest'])) {
                                $suggest = func::php2js($v['suggest']);
                            }
                        }
                        $content.= '<script type="text/javascript">
                            $(function(){
                                $(\'#'.$classTitle.'\').autocomplete(\''.$v['url'].'\', {
                                    valueInput: $(\'#'.$classID.'\'), 
                                    classes: {dropdown:\'dropdown-menu dropdown-menu-nolink\', parent:\'open\', parentProgress:\'autocomplete-progress\'},
                                    params:'.( ! empty($v['params']) ? func::php2js($v['params']) : '{}').',
                                    suggest:'.$suggest.',
                                    onSelect:'.( ! empty($v['onSelect']) ? func::php2js($v['onSelect']) : 'function(){ var $el = $(this.valueInput); $el.closest(\'form\').trigger(\'autocomplete.select\', $el.attr(\'name\')); }').'
                                });
                            });
                        </script>';
                    } break;
                    case 'select': # выпадающий список
                    {
                        if (!isset($v['options'])) {
                            continue 2;
                        } else if (is_callable($v['options'], true)) {
                            $v['options'] = call_user_func($v['options']);
                        }
                        $options = (!empty($v['options']) && is_array($v['options']) ? $v['options'] : array());
                        $options = HTML::selectOptions($options, $value,
                            (isset($v['empty']) ? $v['empty'] : false),
                            (isset($v['idKey']) ? $v['idKey'] : false),
                            (isset($v['titleKey']) ? $v['titleKey'] : false),
                            (isset($v['options.attr']) && is_array($v['options.attr']) ? $v['options.attr'] : array())
                        );
                        $content = '<select'.HTML::attributes(array_merge(array(
                            'class' => 'form-control input-sm j-input',
                            'data-input' => 'select',
                            'name'  => $key,
                        ), $attr)).'>'.$options.'</select>';
                    } break;
                    case 'checkbox': # галочка
                    {
                        $content = '<label class="btn btn-default btn-checkbox btn-sm l-box-filter-btn">';
                        $attr = array_merge(array(
                            'type'  => 'checkbox',
                            'class' => 'j-input',
                            'data-input' => 'checkbox',
                            'name'  => $key,
                        ), $attr);
                        if ($value) {
                            $attr[] = 'checked';
                        }
                        $content .= '<input'.HTML::attributes($attr).' />';
                        if (!empty($v['title']) && is_string($v['title'])) {
                            $content .= HTML::escape($v['title']);
                        }
                        $content.= '</label>';
                    } break;
                    case 'checkbox-options':
                    {
                        if (!isset($v['options'])) {
                            continue 2;
                        } else if (is_callable($v['options'], true)) {
                            $v['options'] = call_user_func($v['options']);
                        }
                        $options = (!empty($v['options']) && is_array($v['options']) ? $v['options'] : array());
                        if (empty($options)) continue 2;
                        if ( ! is_array($value)) {
                            $value = array($value);
                        }
                        $attr = array_merge(array(
                            'type'  => 'checkbox',
                            'class' => 'j-input',
                            'data-input' => 'checkbox',
                        ), $attr);
                        ?>
                        <div<?= $attrMore($v, 'form-group dropdown j-list-filter-item') ?>>
                            <a href="#" class="btn btn-default btn-sm l-box-filter-btn" data-toggle="dropdown"><?= (!empty($v['title']) && is_string($v['title'])) ? HTML::escape($v['title']) : '' ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu l-box-filter-dropdown mega-dropdown">
                                <?php foreach ($options as $k => $vv):
                                    $at = array_merge(array(
                                        'name'  => $key.'[]',
                                        'value' => isset($v['idKey']) && isset($vv[ $v['idKey'] ]) ? $vv[ $v['idKey'] ] : $k,
                                    ), $attr);
                                    if (in_array($at['value'], $value)) {
                                        $at['checked'] = 'checked';
                                    }
                                    ?>
                                <li>
                                    <label class="dropdown-menu-item">
                                        <input<?= HTML::attributes($at) ?>/> <?= isset($v['titleKey']) && isset($vv[ $v['titleKey'] ]) ? $vv[ $v['titleKey'] ] : $vv ?>
                                    </label>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <?php
                    } break;
                    case 'radio-btn-group':
                    {
                        if (!isset($v['options'])) {
                            continue 2;
                        } else if (is_callable($v['options'], true)) {
                            $v['options'] = call_user_func($v['options']);
                        }
                        $options = (!empty($v['options']) && is_array($v['options']) ? $v['options'] : array());
                        if (empty($options)) continue 2;

                        $attr = array_merge(array(
                            'type'  => 'radio',
                            'class' => 'j-input',
                            'data-input' => 'radio',
                            'name'  => $key,
                        ), $attr);
                        ?>
                        <div<?= $attrMore($v, 'form-group j-list-filter-item') ?>>
                            <div class="btn-group btn-group-sm" data-toggle="buttons">
                                <?php foreach ($options as $k => $vv):
                                    $at = array_merge(array(
                                        'value' => isset($v['idKey']) && isset($vv[ $v['idKey'] ]) ? $vv[ $v['idKey'] ] : $k,
                                    ), $attr);
                                    if ($at['value'] == $value) {
                                        $at['checked'] = 'checked';
                                    }
                                ?>
                                <label class="btn btn-primary<?= ! empty($at['checked']) ? ' active' : '' ?>">
                                    <input<?= HTML::attributes($at) ?>/> <?= isset($v['titleKey']) && isset($vv[ $v['titleKey'] ]) ? $vv[ $v['titleKey'] ] : $vv ?>
                                </label>
                                <?php endforeach; ?>
                            </div>
                            &nbsp;
                        </div>
                        <?php
                    } break;
                    case 'button':
                    {   if(empty($v['title'])) break;
                        $attr = array_merge(array(
                            'class' => 'btn btn-default btn-sm',
                        ), $v['attr']);
                        ?>
                            <div<?= $attrMore($v, 'form-group j-list-filter-item'.( ! empty($v['right']) ? ' pull-right' : '')) ?>>
                                <?php if ( ! empty($v['href'])): ?>
                                    <a href="<?= $v['href'] ?>"<?= HTML::attributes($attr) ?>><?= $v['title'] ?></a>
                                <?php else: ?>
                                    <button type="button"<?= HTML::attributes($attr) ?>><?= $v['title'] ?></button>
                                <?php endif; ?>
                            </div>
                        <?php
                    } break;
                }

                if ($content !== false) {
                    $attr = array('class'=>'form-group j-list-filter-item');
                    if ( ! empty($isAutocomplete)) {
                        $attr['class'] .= ' form-group-autocomplete';
                    }
                    if ($isMore && !$noMore) {
                        $attr['class'] .= ' j-more more-hide';
                        $moreCount++;
                    }
                    ?>
                    <div<?= HTML::attributes($attr); ?>>
                        <?= $content ?>
                    </div><?php
                }
            }
            if ($visibleCount > 0) { ?>
            <div class="form-group">
                <?php if (!$noMore && $moreCount > 0) { ?>
                    <button type="button" class="btn btn-default btn-sm j-button-more"><?= _t('', '+ more options') ?></button>
                <?php } ?>
                <?php if (empty($opts['no-submit'])) { ?>
                <button type="submit" class="btn btn-success btn-sm j-button-submit"><?= _t('', 'Select') ?></button>
                <?php } ?>
                <?php if (empty($opts['no-reset'])) { ?>
                    <button type="button" class="btn btn-default btn-sm j-button-reset"><?= _t('', 'Clear') ?></button>
                <?php } ?>
            </div>
            <?php } ?>

        </form>
    </div>

</div>