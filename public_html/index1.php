<?php
/**
 * Главная страница
 * @var $this Site
 * @var $titleh1 string заголовок H1
 * @var $centerBlock string центральный блок
 * @var $last string блок последних / премиум объявлений (HTML)
 * @var $seotext string SEO-текст  <?= $map ?>
 */
?>

<div class="row">
  <div class="col-md-8">
    <?php if (empty($map)) { ?>
      <div class="index-map__nomap"><?= _t('site','Для данного региона карта еще недоступна.') ?></div>
    <?php } else { ?>
      <div class="index-map index-map__ru hidden-xs mrgt15">
         <?= Geo::i()->regionMap(1000); ?>
<?= $map ?>
      </div>
    <?php } ?>
    <div class="row">
  
      <div class="col-sm-6">
   
      </div>
    
    </div>
 
  </div>
  <div class="col-md-4">
 
   
  </div>
</div>