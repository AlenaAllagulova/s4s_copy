/**
 * Форма поиска магазинов
 */
//
var objs = null;
var city_center_coords = null;
var user_coords = null;

var jShopsSearch = (function() {
    var inited = false,
        $form, $list, $pgn,
        listTypes = {
            list: 1,
            map: 3
        },
        url = document.location.pathname,
        o = {
            lang: {},
            ajax: true,
            geoMapZoom: 10,
	    filterDropdownMargin: 15,
	    isVertical:false
        };

    function init() {
        $form = $('#j-f-form');
        $list = $('#j-shops-search-list');
        $pgn = $('#j-shops-search-pgn');
        // devices
        desktop_tablet.init();
        phone.init();
        $(window).on('app-device-changed', function(e, device) {
            onSubmit({
                fade: false
            });
        });

        // contacts
        var contactsClass = '.j-contacts';
        var contactsShowList = function($link, _device) {
            if (_device == app.devices.phone) {
                $link.next().slideToggle();
            } else {
                $list.find(contactsClass + ':visible').not($link.next()).addClass('hide');
                $link.next().toggleClass('hide');
            }
        };
        $list.on('click', '.j-contacts-ex', function(e) {
            nothing(e);
            var $link = $(this),
                _device = $link.data('device');
            var $shop = $link.closest('.j-shop');
            if ($shop.hasClass('j-contacts-loaded')) {
                contactsShowList($link, _device);
            } else {
                bff.ajax(bff.ajaxURL('shops', 'shop-contacts-list'), {
                    ex: $shop.data('ex'),
                    hash: app.csrf_token,
                    lt: listTypes.list
                }, function(data, errors) {
                    if (data && data.success) {
                        $shop.addClass('j-contacts-loaded').find(contactsClass).html(data.html);
                        contactsShowList($link, _device);
                    } else if (errors) {
                        app.alert.error(errors);
                    }
                });
            }
        }).on('click', function(e) {
            var $target = $(e.target);
            if (!($target.is('a') || $target.parents('a').length ||
                    $target.parents(contactsClass).length || $target.is(contactsClass))) {
                $list.find(contactsClass + ':visible').addClass('hide');
            }
        });

        // list: type
        $list.find('#j-f-listtype').on('click', '.j-type', function() {
            if (!$(this).hasClass('active')) {
                onListType($(this).metadata());
            }
        });
        // pgn
        $pgn.on('keyup', '.j-pgn-goto', function(e) {
            if (e.hasOwnProperty('keyCode') && e.keyCode == 13) {
                onPage($(this).val(), true);
                nothing(e);
            }
        });
        if (o.ajax) {
            $pgn.on('click', '.j-pgn-page', function(e) {
                nothing(e);
                onPage($(this).data('page'), true);
            });
        }

        // history
        if (o.ajax) {
            var queryInitial = prepareQuery();
            $(window).bind('popstate', function(e) {
                var loc = history.location || document.location;
                var query = loc.search.substr(1);
                if (query.length == 0) query = queryInitial;
                $form.deserialize(query, true);
                desktop_tablet.onPopstate();
                phone.onPopstate();
                onSubmit({
                    popstate: true
                });
            });
        }

        // hook: init
        bff.hook('shops.search.init', $form, o);
    } //jShopsSearch

    function onListType(typeData) {
        var v = $form.get(0).elements['lt'];
        if (typeData) {
            v.value = typeData.id;
            o.ajax = false; // reload page on list type changes
            onSubmit();
            return typeData;
        }
        if (!o.listtype.hasOwnProperty(v.value)) {
            for (var i in o.listtype) {
                v.value = i;
                break;
            }
        }
        return {
            id: v.value,
            title: o.listtype[v.value].t
        };
    } //onListType

    function onPage(pageId, update) {
        pageId = intval(pageId);
        if (pageId <= 0) pageId = 0;
        var v = $form.get(0).elements['page'];
        if (pageId && intval(v.value) != pageId) {
            v.value = pageId;
            if (update) onSubmit({
                scroll: true
            });
        }
    } //onPage

    function onSubmit(ex) {
        ex = $.extend({
            popstate: false,
            scroll: false,
            fade: true
        }, ex || {});
        var query = prepareQuery();
        if (o.ajax) {
            bff.ajax(url, query, function(data) {
                if (data && data.success) {
                    if (ex.scroll) $.scrollTo($list, {
                        offset: -150,
                        duration: 500,
                        axis: 'y'
                    });
                    $pgn.html(data.pgn);
                    var list = $list.find('.j-list-' + app.device());
                    if (onListType().id == listTypes.map) {
                        list.find('.j-maplist').html(data.list);
                        o.items = data.items;
                        desktop_tablet.itemsToMap(o.items);
                        phone.itemsToMap(o.items);
                    } else {
                        list.html(data.list);
                    }
                    if (!ex.popstate) history.pushState(null, null, url + '?' + query);
                }
            }, function(p) {
                if (ex.fade) $list.toggleClass('disabled');
            });
        } else {
            bff.redirect(url + '?' + query);
        }
    } //onSubmit

    function prepareQuery() {
        var query = [];
        $.each($form.serializeArray(), function(i, field) {
            if (field.value && field.value != 0 && field.value != '') query.push(field.name + '=' + encodeURIComponent(field.value));
        });
        return query.join('&');
    } //prepareQuery

    var desktop_tablet = (function() {
        var inited = false,
            map = false,
	    $filter, filterButtons = {},
            mapClusterer, $mapItems = [],
            mapContent = {},
            bffmap = false,
            mapInfoWindow = false,
            mapMarkers = false;

        function init() {
            // map
            if (onListType().id == listTypes.map) {
                mapInit();
            }

///////////////
            $filter = $('#j-f-desktop'); 

	    if( ! $filter.length && ! o.isVertical) return; inited = true;

            var $form = $('#j-f-form');
            $filter.find('.j-button').each(function(){
                var b = $(this).metadata(); b.$button = $(this); b.$buttonCaret = b.$button.find('.j-button-caret');
                b.popup = app.popup('f-desktop-button-'+b.key, b.$button.prev(), b.$button, {pos:{top:b.$button.outerHeight()+intval(o.filterDropdownMargin), minRightSpace:200}, onInit: function($p){
                    var _this = this;
                    $p.on('click', '.j-submit', function(){
                        _this.hide();
                        onSubmit();
                    }).on('click', ':checkbox', function(){
                        onCheckbox($(this), b);
                        if ( $(this).hasClass('j-reset') ) {
                            _this.hide();
                            onSubmit();
                        }
                    }).on('keyup', '.j-from,.j-to', function(){
                        $p.find('.j-reset').prop({checked:false,disabled:false});
                        updateBlockButton(b);
                    }).on('click', '.j-catLink', function(e){
                        e.preventDefault();
                        _this.hide();
                        bff.redirect($(this).attr('href')+'?'+prepareQuery());
                    }).on('click', '.j-radiusLink', function(e){
			e.preventDefault();
			_this.hide();

			var r = $(this).attr('data-radius');
			if (r)
			{
			//	$('#center_radius').show();
				if (typeof window.radius !== "undefined")
				{
					window.radius(r);		                	
				}
				$('#id_radius').find('.j-value').attr('data-radius', r).html(r + " km");
			}
			else 
			{
				$('#center_radius').hide();
				if (window.radius_clr) window.radius_clr();
				$('#id_radius').find('.j-value').attr('data-radius', 0).html("Все");
			}
		    });

		    $('#j-center').click(function(){
			var r = $('#id_radius').find('.j-value').attr('data-radius');
			if (r)
			{
				$('#center_radius').show();
				if (typeof window.radius !== "undefined")
				{
					window.radius(r);		                	
				}
				$('#id_radius').find('.j-value').attr('data-radius', r).html(r + " km");
			}
			else 
			{
				$('#center_radius').hide();
				if (window.radius_clr) window.radius_clr();
				$('#id_radius').find('.j-value').attr('data-radius', 0).html("Все");
			}
		    });

                    if(b.type == 'price') {
                        $p.find('.j-curr-select').change(function(){
                            $p.find('.j-curr').val(' '+this.options[this.selectedIndex].text);
                            updateBlockButton(b);
                        });
                    } else if(b.type == 'metro') {
                        updateBlockButton(b);
                    }
                }, onShow: function($p){
                    $p.addClass('open').fadeIn(100);
                    b.$buttonCaret.toggleClass('fa-caret-up fa-caret-down');
					var subs_leigh = $('#dropdown-li-count li').length;
                    if(subs_leigh>1){ 
	                    $('#subs_search').removeClass('hide');
                    }else{
	                    $('#subs_search').addClass('hide');
                    }
                }, onHide: function($p){
                    $p.removeClass('open').fadeOut(100);
                    b.$buttonCaret.toggleClass('fa-caret-up fa-caret-down');
                }});
                b.$popup = b.popup.getPopup();
                filterButtons[b.key] = b;
            });
///////////////

        }

        function mapInit() {
            var $c = $list.find('.j-search-map-desktop');
            if (!$c.length) return;

            bffmap = app.map($c.get(0), o.defaultCoords, function(mmap) {
                if (this.isYandex()) {

                    map = mmap;

                    map.controls.remove('searchControl');
                    map.controls.remove('geolocationControl');
                    map.controls.remove('typeSelector');
                    map.controls.remove('trafficControl');
                    map.controls.add('routeButtonControl', {
                        size: 'small',
                        float: 'right'
                    });
                    map.controls.add('geolocationControl', {
                        float: 'right'
                    });
                    map.behaviors.enable('scrollZoom');
                    //geo
                    var geolocation = ymaps.geolocation;
                    var town = ymaps.geolocation.coordinates;

                    geolocation.get({
                        provider: 'auto',
                        mapStateAutoApply: false
                    }).then(function(result) {
			
			var city_name_default = result.geoObjects.get(0).properties.get('metaDataProperty.GeocoderMetaData.AddressDetails.Country.AdministrativeArea.Locality.LocalityName', 'Москва');

			//console.log(city_name_default);

			var city_name = $('#j-f-region-desktop-link span').text();
			if (city_name == "Все регионы") city_name = city_name_default;

			//console.log(city_name);

			ymaps.geocode(city_name, {results: 1}).then(function (res) {
                		city_center_coords = res.geoObjects.get(0).geometry.getCoordinates()
/*
                        	res.geoObjects.get(0).properties.set({
                            		balloonContentBody: ymaps.geolocation.coordinates
                        	});


				map.geoObjects.add(res.geoObjects);
*/
			});

                        result.geoObjects.options.set('preset', 'islands#redCircleIcon');
                      //  result.geoObjects.options.set('ImageHref', 'img/me.png');


              //          result.geoObjects.options.set('iconGlyph', 'user');
                        result.geoObjects.get(0).properties.set({
                            balloonContentBody: ymaps.geolocation.coordinates
                        });
                        map.geoObjects.add(result.geoObjects);
                        var coord = result.geoObjects.get(0).geometry.getCoordinates();
			
			user_coords = coord;
                        map.setCenter(coord, 10);

                        //   });
                        // end geo
                        // radius
                        function radius(coord) {
                            var listBoxItems = ['1км', '5км', '10км', '20км', '50км', 'сбросить']
                                .map(function(title) {
                                    return new ymaps.control.ListBoxItem({
                                        data: {
                                            content: title
                                        },
                                        state: {
                                            selected: false
                                        }
                                    });
                                });
/*
                                listBoxControl = new ymaps.control.ListBox({
                                    data: {
                                        content: 'Радиус поиска',
                                        title: 'Выберите радиус поиска'
                                    },
                                    options: {
                                        itemSelectOnClick: false
                                    },
                                    items: listBoxItems,
                                });
*/
                            var x = 0;
/*
                            map.controls.add(listBoxControl, {
                                float: 'right'
                            });
                            listBoxControl.state.set('expanded', false);
*/
                            var circle = new ymaps.Circle([coord, x], {}, {
                                geodesic: true
                            });
                            map.geoObjects.add(circle);

                            var _radius = function radius(x) {
                                var x1 = x * 1000;
                                //listBoxControl.data.set('content', x + "km");
                                //listBoxControl.data.set('title', "радиус поиска:" + x + "km");
                                
                                //global objs
                                if (objs == null)
                                	objs = map.geoObjects.get(0).getGeoObjects();                                								

                                if ($('#j-center:checked').length > 0)				
                                	coord = city_center_coords;				
                                else				
                                	coord = user_coords;				

                                //console.log(coord);

                                map.setCenter( coord ,10);

				map.geoObjects.get(0).removeAll();

                                map.geoObjects.remove(circle);
                                circle = new ymaps.Circle([coord, x1], {}, {
                                    fillColor: "#cccccc99"
                                });
                                map.container.fitToViewport();
                                map.setBounds(map.getBounds());

                                map.geoObjects.add(circle);

                                for (var i = 0, l = objs.length; i < l; i++) {
                                	//console.log(objs[i]);
                                	var coord2 = objs[i].geometry.getCoordinates();
                                	var distance = ymaps.coordSystem.geo.getDistance(coord, coord2);

                                	if (distance < x1)
                                	{	
                                		//console.log(objs[i].properties);
                                		objs[i].properties.set('iconContent', parseFloat( distance / 1000 ).toFixed(2) + " km");

                                		map.geoObjects.get(0).add(objs[i]);
                                		$('div[data-index="'+i+'"]').show();
                                		$('div[data-index="'+i+'"]').find('.sh-map-list-item-distance').html( parseFloat( distance / 1000 ).toFixed(2) + " km")
                                		//console.log($('div[data-index="'+i+'"]').find('.sh-map-list-item-distance'));
                                		//console.log('inside');
                                	}
                                	else
                                	{
                                		map.geoObjects.get(0).remove(objs[i]);
                                		$('div[data-index="'+i+'"]').hide();
                                		//console.log('outside');
                                	}
                                }

                                //listBoxControl.collapse();
                                //x=0;x1=0;
                            }


window.radius = _radius;

window.radius_clr = function () {
	//listBoxControl.data.set('content', 'Радиус поиска');
	//listBoxControl.data.set('title', 'выберите радиус поиска');
	map.setCenter( coord ,8);
	map.geoObjects.remove(circle);
	//listBoxControl.collapse();

	for (var i = 0, l = objs.length; i < l; i++) {
		map.geoObjects.get(0).add(objs[i]);
		$('div[data-index="'+i+'"]').show();
	}	
};// end radius

/*
                            //1km
                            listBoxControl.get(0).events.add('click', function() {
                                x = 1;
                                _radius(x);
                            });
                            //5km
                            listBoxControl.get(1).events.add('click', function() {
                                x = 5;
                                _radius(x);
                            });
                            //10km
                            listBoxControl.get(2).events.add('click', function() {
                                x = 10;
                                _radius(x);
                            });
                            //20km
                            listBoxControl.get(3).events.add('click', function() {
                                x = 20;
                                _radius(x);
                            });
                            //50km
                            listBoxControl.get(4).events.add('click', function() {
                                x = 50;
                                _radius(x);
                            });
                            //сброс
                            listBoxControl.get(5).events.add('click', function() {
                                listBoxControl.data.set('content', 'Радиус поиска');
                                listBoxControl.data.set('title', 'выберите радиус поиска');
                                map.setCenter(coord, 10);
                                map.geoObjects.remove(circle);
                                listBoxControl.collapse();

				for (var i = 0, l = objs.length; i < l; i++) {
					map.geoObjects.get(0).add(objs[i]);
					$('div[data-index="'+i+'"]').show();
				}

                            });
*/

                        } // end radius
                        //

                        radius(coord);
                    }); // end geo

                    function find_zoom(points) {
                        if (points.length == 1) {
                            map.setZoom(16, {
                                duration: 1000
                            });
                        } else {
                            map.setBounds(myMap.geoObjects.getBounds());
                        }
                    }


                    mapClusterer = new ymaps.Clusterer({
                        //                        preset: 'twirl#blueClusterIcons',
                        preset: 'twirl#redClusterIcons',

                        clusterBalloonWidth: 240,
                        clusterBalloonHeight: 250,
                        clusterBalloonLeftColumnWidth: 37,
                        clusterDisableClickZoom: false,
                        zoomMargin: 15

                    });
                } else if (this.isGoogle()) {
                    map = mmap;
                    mapInfoWindow = new google.maps.InfoWindow({});
                }
                var $mapToggler = $list.find('#j-search-map-toggler').
                on('click', 'a.j-search-map-toggler-link', function(e) {
                    var $link = $(this);
                    var $arr = $('.j-search-map-toggler-arrow', $mapToggler);
                    var list = $list.find('.j-maplist'),
                        listMap = $list.find('.j-map');
                    if (!$link.hasClass('active')) {
                        $link.text(o.lang.map_toggle_close).addClass('active');
                        $arr.html('&raquo;');
                        list.hide();
                        listMap.removeClass(listMap.data('short-class'));
                    } else {
                        $link.text(o.lang.map_toggle_open).removeClass('active');
                        $arr.html('&laquo;');
                        list.show();
                        listMap.addClass(listMap.data('short-class'));
                    }
                    if (bffmap.isYandex()) {
                        map.container.fitToViewport();
                        map.setBounds(map.getBounds());
                    } else if (bffmap.isGoogle()) {
                        if (mapClusterer) {
                            mapClusterer.fitMapToMarkers();
                        }
                        bffmap.refresh();
                    }
                    nothing(e);
                });
                desktop_tablet.itemsToMap(o.items);
            }, {
                controls: 'search'
            });
        }

        function mapListItemToggle($item) {
            $mapItems.filter('.active').removeClass('active');
            if ($item && $item.length) $item.addClass('active');
        }

        function mapBalloonOpen(point) {
            if (!bffmap || !bffmap.isYandex()) return;
            var geoObjectState = mapClusterer.getObjectState(point),
                cluster = geoObjectState.isClustered && geoObjectState.cluster;
            if (cluster) {
                geoObjectState.cluster.state.set('activeObject', point);
                mapBalloonContent(point);
                mapClusterer.balloon.open(geoObjectState.cluster);
            } else {
                if (!point.balloon.isOpen()) point.balloon.open();
            }
        }

        function mapBalloonContent(placemark, listItemToggle) {
            if (!bffmap || !bffmap.isYandex()) return;
            var index = placemark.properties.get('index');
            if (listItemToggle === true) mapListItemToggle($mapItems.filter('[data-index="' + index + '"]'));
            if (mapContent.hasOwnProperty(index)) return;
            placemark.properties.set('balloonContent', o.lang.map_content_loading);
            mapContent[index] = '';
            bff.ajax(bff.ajaxURL('shops', 'shop-contacts-list'), {
                ex: placemark.properties.get('ex'),
                hash: app.csrf_token,
                lt: listTypes.map,
                device: app.device()
            }, function(data, errors) {
                if (data && data.success) {
                    placemark.properties.set('balloonContent', (mapContent[index] = data.html));
                } else if (errors) {
                    app.alert.error(errors);
                }
            });
        }

        function popupMarker(itemID, markerClick) {
            if (!bffmap || !bffmap.isGoogle()) return;

            if (mapMarkers.hasOwnProperty(itemID)) {
                var m = mapMarkers[itemID];
                if (!m.hasOwnProperty('ballon')) {
                    bff.ajax(bff.ajaxURL('shops', 'shop-contacts-list'), {
                        ex: m.ex,
                        hash: app.csrf_token,
                        lt: listTypes.map,
                        device: app.device()
                    }, function(data, errors) {
                        if (data && data.success) {
                            mapMarkers[itemID].ballon = data.html;
                            popupMarker(itemID, markerClick);
                        } else if (errors) {
                            app.alert.error(errors);
                        }
                    });
                    return;
                }

                if (markerClick !== true) {
                    map.panTo(m.position);
                }
                mapInfoWindow.close();
                mapInfoWindow.setPosition(m.position);
                mapInfoWindow.setContent(m.ballon);
                mapInfoWindow.open(map);
            }
            return false;
        }

        return {
            init: init,
            onPopstate: function() {
                if (!inited) return;
            },
            itemsToMap: function(items) {
                if (!bffmap) return;
                if (bffmap.isYandex()) {
                    // items map: clear
                    if (map === false) return;
                    mapContent = {};
                    mapClusterer.removeAll();
                    map.geoObjects.remove(mapClusterer);

                    var itemsToCluster = [],
                        j = 0;
                    for (var i in items) {
                        var v = items[i];
                        v.point_desktop = itemsToCluster[j++] = new ymaps.Placemark([parseFloat(v.addr_lat), parseFloat(v.addr_lon)], {
                                index: i,
                                ex: v.ex,
                                num: v.num,
                                clusterCaption: v.num
                            },
                            //  {preset: 'twirl#blueIcon', openEmptyBalloon: true});
                            {
                                preset: 'islands#blackStretchyIcon', //'twirl#redIcon',
                                openEmptyBalloon: true
                            });

                        v.point_desktop.events.add('click', function(e) {
                            var placemark = e.get('target');
                            if ($mapItems.length) {
                                var itemIndex = placemark.properties.get('index');
                                mapListItemToggle($mapItems.filter('[data-index="' + itemIndex + '"]'));
                            }
                        });
                        v.point_desktop.events.add('balloonopen', function(e) {
                            mapBalloonContent(e.get('target'));
                        });
                    }
                    mapClusterer.add(itemsToCluster);
                    map.geoObjects.add(mapClusterer);
                    if (itemsToCluster.length > 1) {
                        var pos = ymaps.util.bounds.getCenterAndZoom(
                            mapClusterer.getBounds(), map.container.getSize(), map.options.get('projection')
                        );
                        map.setCenter(pos.center, pos.zoom);
                    } else {
                        if (itemsToCluster.length) {
                            map.setCenter(itemsToCluster[0].geometry.getCoordinates());
                        }
                    }
                    map.container.fitToViewport();
                    mapClusterer.events.once('objectsaddtomap', function() {
                        map.setBounds(mapClusterer.getBounds(), {
                            checkZoomRange: true
                        });
                    });
                    mapClusterer.events.add('balloonopen', function(e) {
                        var target = e.get('target');
                        if (target.getGeoObjects) {
                            var activeObject = target.state.get('activeObject');
                            mapBalloonContent(activeObject, true);
                            target.state.events.add('change', function() {
                                var newActiveObject = target.state.get('activeObject');
                                if (activeObject != newActiveObject) {
                                    activeObject = newActiveObject;
                                    mapBalloonContent(activeObject, true);
                                }
                            });
                        }
                    });
                } else if (bffmap.isGoogle()) {
                    if (mapClusterer) {
                        mapClusterer.clearMarkers();
                    }
                    mapMarkers = {};
                    var mapMarkersToCluster = [];
                    mapInfoWindow.close();
                    var v, j = 0;
                    for (var i in items) {
                        v = items[i];
                        var id = j++;
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(parseFloat(v.addr_lat), parseFloat(v.addr_lon))
                        });
                        marker.itemID = id;
                        mapMarkers[id] = {
                            position: marker.getPosition(),
                            ex: v.ex
                        };
                        mapMarkersToCluster.push(marker);
                        google.maps.event.addListener(marker, 'click', function() {
                            popupMarker(this.itemID, true);
                            mapListItemToggle($mapItems.filter('[data-index="' + this.itemID + '"]'));
                        });
                    }

                    mapClusterer = new MarkerClusterer(map, mapMarkersToCluster, {
                        imagePath: app.rootStatic + '/js/markerclusterer/images/m'
                    });
                    if (mapMarkersToCluster.length > 0) {
                        if (mapMarkersToCluster.length > 1) {
                            mapClusterer.fitMapToMarkers();
                        } else {
                            bffmap.panTo([v.addr_lat, v.addr_lon], {
                                delay: 10,
                                duration: 200
                            });
                        }
                    }
                    bffmap.refresh();
                }

                // items: list

                $mapItems = $list.find('.j-maplist .j-maplist-item').bind('click', function(e) {

                    var $item = $(this);
                    var itemIndex = $item.data('index');
                    if (!o.items.hasOwnProperty(itemIndex) || $(e.target).is('a') || $(e.target).parents('a').length) return;
                    mapListItemToggle($item);
                    if (bffmap.isYandex()) {
                        var itemPoint = o.items[itemIndex].point_desktop;
                        var state = mapClusterer.getObjectState(itemPoint),
                            cluster = state.cluster;
                        if ((itemPoint.getMap() || (cluster && cluster.getMap()))) {
                            mapBalloonOpen(itemPoint);
                        } else {
                            map.panTo(itemPoint.geometry.getCoordinates(), {
                                duration: 400,
                                delay: 0,
                                callback: function() {
                                    mapClusterer.events.once('objectsaddtomap', function() {
                                        mapBalloonOpen(itemPoint);
                                    });
                                }
                            });
                        }
                    } else if (bffmap.isGoogle()) {
                        popupMarker(itemIndex, false);
                    }
                });

            }
        };
    }());

    var phone = (function() {
        var inited = false,
            map, mapClusterer, mapContent, bffmap = false,
            mapInfoWindow = false,
            mapMarkers = false;


        function init() {
            // map
            if (onListType().id == listTypes.map) {
                mapInit();
            }
        }

        function mapInit() {
            var $c = $('.j-search-map-phone');
            if (!$c.length) return;

            bffmap = app.map($c.get(0), o.defaultCoords, function(mmap) {
                if (this.isYandex()) {
                    map = mmap;
                    // Создаем собственный макет с информацией о выбранном геообъекте.
                    var customItemContentLayout = ymaps.templateLayoutFactory.createClass('<div class="ballon_body">{{ properties.balloonContentBody|raw }}</div>');

                    mapClusterer = new ymaps.Clusterer({
                        preset: 'twirl#blueClusterIcons',
                        clusterBalloonContentLayoutWidth: 250,
                        clusterBalloonContentLayoutHeight: 100,
                        clusterBalloonLeftColumnWidth: 37,
                        clusterDisableClickZoom: false,
                        clusterBalloonItemContentLayout: customItemContentLayout,
                        zoomMargin: 15
                    });

                } else if (this.isGoogle()) {
                    map = mmap;
                    mapInfoWindow = new google.maps.InfoWindow({});
                }
                phone.itemsToMap(o.items);
            }, {
                controls: 'view',
                zoom: o.geoMapZoom
            });
        }

        function mapBalloonContent(placemark) {
            if (!bffmap || !bffmap.isYandex()) return;
            var index = placemark.properties.get('index');
            if (mapContent.hasOwnProperty(index)) return;
            placemark.properties.set('balloonContent', o.lang.map_content_loading);
            //placemark.properties.set('balloonContent', 'test');
            mapContent[index] = '';
            bff.ajax(bff.ajaxURL('shops', 'shop-contacts-list'), {
                ex: placemark.properties.get('ex'),
                hash: app.csrf_token,
                lt: listTypes.map,
                device: app.device()
            }, function(data, errors) {
                if (data && data.success) {
                    placemark.properties.set('balloonContent', (mapContent[index] = data.html));

                } else if (errors) {
                    app.alert.error(errors);
                }
            });
        }

        function popupMarker(itemID, markerClick) {
            if (!bffmap || !bffmap.isGoogle()) return;

            if (mapMarkers.hasOwnProperty(itemID)) {
                var m = mapMarkers[itemID];
                if (!m.hasOwnProperty('ballon')) {
                    bff.ajax(bff.ajaxURL('shops', 'shop-contacts-list'), {
                        ex: m.ex,
                        hash: app.csrf_token,
                        lt: listTypes.map,
                        device: app.device()
                    }, function(data, errors) {
                        if (data && data.success) {
                            mapMarkers[itemID].ballon = data.html;
                            popupMarker(itemID, markerClick);
                        } else if (errors) {
                            app.alert.error(errors);
                        }
                    });
                    return;
                }

                if (markerClick !== true) {
                    map.panTo(m.position);
                }
                mapInfoWindow.close();
                mapInfoWindow.setPosition(m.position);
                mapInfoWindow.setContent(m.ballon);
                mapInfoWindow.open(map);
            }
            return false;
        }

        return {
            init: init,
            onPopstate: function() {
                if (!inited) return;
            },
            itemsToMap: function(items) {
                if (!bffmap) return;
                if (bffmap.isYandex()) {
                    // items: map
                    if (map === false) return;
                    mapContent = {};
                    mapClusterer.removeAll();
                    map.geoObjects.remove(mapClusterer);
                    var itemsToCluster = [],
                        j = 0;
                    for (var i in items) {
                        var v = items[i];
                        var placemark = itemsToCluster[j++] = new ymaps.Placemark([parseFloat(v.addr_lat), parseFloat(v.addr_lon)], {
                            index: i,
                            ex: v.ex,
                            clusterCaption: v.num
                        }, {
                            preset: 'twirl#blueIcon',
                            openEmptyBalloon: true
                        });
                        placemark.events.add('balloonopen', function(e) {
                            mapBalloonContent(e.get('target'));
                        });
                    }
                    mapClusterer.add(itemsToCluster);
                    map.geoObjects.add(mapClusterer);
                    if (itemsToCluster.length > 1) {
                        var pos = ymaps.util.bounds.getCenterAndZoom(
                            mapClusterer.getBounds(), map.container.getSize(), map.options.get('projection')
                        );
                        map.setCenter(pos.center, pos.zoom);
                    } else {
                        if (itemsToCluster.length) {
                            map.setCenter(itemsToCluster[0].geometry.getCoordinates());
                        }
                    }
                    map.container.fitToViewport();

                    mapClusterer.events.add('balloonopen', function(e) {
                        var target = e.get('target');
                        if (target.getGeoObjects) {
                            var activeObject = target.state.get('activeObject');
                            mapBalloonContent(activeObject);
                            target.state.events.add('change', function() {
                                var newActiveObject = target.state.get('activeObject');
                                if (activeObject != newActiveObject) {
                                    activeObject = newActiveObject;
                                    mapBalloonContent(activeObject);
                                }
                            });
                        }
                    });
                } else if (bffmap.isGoogle()) {
                    if (mapClusterer) {
                        mapClusterer.clearMarkers();
                    }
                    mapMarkers = {};
                    var mapMarkersToCluster = [];
                    mapInfoWindow.close();
                    var v, j = 0;
                    for (var i in items) {
                        v = items[i];
                        var id = j++;
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(parseFloat(v.addr_lat), parseFloat(v.addr_lon))
                        });
                        marker.itemID = id;
                        mapMarkers[id] = {
                            position: marker.getPosition(),
                            ex: v.ex
                        };
                        mapMarkersToCluster.push(marker);
                        google.maps.event.addListener(marker, 'click', function() {
                            popupMarker(this.itemID, true);
                        });
                    }

                    mapClusterer = new MarkerClusterer(map, mapMarkersToCluster, {
                        imagePath: app.rootStatic + '/js/markerclusterer/images/m'
                    });
                    if (mapMarkersToCluster.length > 0) {
                        if (mapMarkersToCluster.length > 1) {
                            mapClusterer.fitMapToMarkers();
                        } else {
                            bffmap.panTo([v.addr_lat, v.addr_lon], {
                                delay: 10,
                                duration: 200
                            });
                        }
                    }
                    bffmap.refresh();
                }
            }
        };
    }());

    return {
        init: function(options) {
            if (inited) return;
            inited = true;
            o = bff.filter('shops.search.settings', $.extend(o, options || {}));
            $(function() {
                init();
            });
        }
    };
}());

$(function() {
    /**
     * category-filter (desktop)
     */
    app.popup('f-cat-desktop', '#j-f-cat-desktop-popup', '#j-f-cat-desktop-link', {
        onInit: function($p) {
            var _this = this;
            var $st1 = $p.find('#j-f-cat-desktop-step1');
            var $st2 = $p.find('#j-f-cat-desktop-step2'),
                st2cache = {};

            function doFilter(type, $link) {
                var f = $link.metadata();
                f['type'] = type;
                f['link'] = $link.attr('href');
                _this.getLink().children('.title').text(f.title);
                bff.redirect(f['link']);
            }

            function st2View(parentID, fromStep1) {
                if (st2cache.hasOwnProperty(parentID)) {
                    $st2.html(st2cache[parentID].html);
                    if (fromStep1) $st2.add($st1).toggleClass('hide');
                } else {
                    bff.ajax(bff.ajaxURL('shops', 'search&ev=catsList'), {
                        parent: parentID,
                        device: app.devices.desktop
                    }, function(data) {
                        if (data && data.success) {
                            st2cache[parentID] = data;
                            st2View(parentID, fromStep1);
                        }
                    });
                }
            }
            $st1.on('click', '.j-all', function() {
                _this.hide();
                doFilter('all', $(this));
                return false;
            });
            $st1.on('click', '.j-main', function() {
                var data = $(this).metadata();
                if (data.subs > 0) {
                    st2View(data.id, true);
                } else {
                    _this.hide();
                    doFilter('cat', $(this));
                }
                return false;
            });
            $st2.on('click', '.j-back', function() {
                var prevID = $(this).metadata().prev;
                if (prevID === 0) {
                    $st2.add($st1).toggleClass('hide');
                } else {
                    st2View(prevID, false);
                }
                return false;
            });
            $st2.on('click', '.j-parent', function() {
                _this.hide();
                doFilter('cat', $(this));
                return false;
            });
            $st2.on('click', '.j-sub', function() {
                var data = $(this).metadata();
                if (data.subs > 0) {
                    st2View(data.id, false);
                } else {
                    _this.hide();
                    doFilter('cat', $(this));
                }
                return false;
            });
        }
    });
    /**
     * category-filter (phone)
     */
    app.popup('f-cat-phone', '#j-f-cat-phone-popup', '#j-f-cat-phone-link', {
        onInit: function($p) {
            var _this = this;
            var $st1 = $p.find('#j-f-cat-phone-step1');
            var $st2 = $p.find('#j-f-cat-phone-step2'),
                st2cache = {};

            function doFilter(type, $link) {
                var f = $link.metadata();
                f['type'] = type;
                f['link'] = $link.attr('href');
                _this.getLink().children('.title').text(f.title);
                bff.redirect(f['link']);
            }

            function st2View(parentID, fromStep1) {
                if (st2cache.hasOwnProperty(parentID)) {
                    $st2.html(st2cache[parentID].html);
                    if (fromStep1) $st2.add($st1).toggleClass('hide');
                    $.scrollTo($st2, {
                        offset: -10,
                        duration: 400,
                        axis: 'y'
                    });
                } else {
                    bff.ajax(bff.ajaxURL('shops', 'search&ev=catsList'), {
                        parent: parentID,
                        device: app.devices.phone
                    }, function(data) {
                        if (data && data.success) {
                            st2cache[parentID] = data;
                            st2View(parentID, fromStep1);
                        }
                    });
                }
            }
            $st1.on('click', '.j-main', function() {
                var data = $(this).metadata();
                if (data.subs > 0) {
                    st2View(data.id, true);
                } else {
                    _this.hide();
                    doFilter('cat', $(this));
                }
                return false;
            });
            $st2.on('click', '.j-back', function() {
                var prevID = $(this).metadata().prev;
                if (prevID === 0) {
                    $st2.add($st1).toggleClass('hide');
                } else {
                    st2View(prevID, false);
                }
                return false;
            });
            $st2.on('click', '.j-parent', function() {
                _this.hide();
                doFilter('cat', $(this));
                return false;
            });
            $st2.on('click', '.j-sub', function() {
                var data = $(this).metadata();
                if (data.subs > 0) {
                    st2View(data.id, false);
                } else {
                    _this.hide();
                    doFilter('cat', $(this));
                }
                return false;
            });
        }
    });
});