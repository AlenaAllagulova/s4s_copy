$(function(){
var rules = {};
// en2ru
rules["~"]="Ё";
rules["`"]="ё";
rules["@"]="\"";
rules["#"]="№";
rules["q"]="й";
rules["w"]="ц";
rules["e"]="у";
rules["r"]="к";
rules["t"]="е";
rules["y"]="н";
rules["u"]="г";
rules["i"]="ш";
rules["o"]="щ";
rules["p"]="з";
rules["a"]="ф";
rules["s"]="ы";
rules["d"]="в";
rules["f"]="а";
rules["g"]="п";
rules["h"]="р";
rules["j"]="о";
rules["k"]="л";
rules["l"]="д";
rules["z"]="я";
rules["x"]="ч";
rules["c"]="с";
rules["v"]="м";
rules["b"]="и";
rules["n"]="т";
rules["m"]="ь";
rules["Q"]="Й";
rules["W"]="Ц";
rules["E"]="У";
rules["R"]="К";
rules["T"]="Е";
rules["Y"]="Н";
rules["U"]="Г";
rules["I"]="Ш";
rules["O"]="Щ";
rules["P"]="З";
rules["A"]="Ф";
rules["S"]="Ы";
rules["D"]="В";
rules["F"]="А";
rules["G"]="П";
rules["H"]="Р";
rules["J"]="О";
rules["K"]="Л";
rules["L"]="Д";
rules["Z"]="Я";
rules["X"]="Ч";
rules["C"]="С";
rules["V"]="М";
rules["B"]="И";
rules["N"]="Т";
rules["M"]="Ь";
rules["["]="х";
rules["{"]="Х";
rules["]"]="ъ";
rules["}"]="Ъ";
rules[";"]="ж";
rules[":"]="Ж";
rules["'"]="э";
rules["\""]="Э";
rules[","]="б";
rules["<"]="Б";
rules["."]="ю";
rules[">"]="Ю";
rules["@"]="\"";
rules["#"]="№";
rules["&"]="?";
rules["/"]=".";
// ru2en
rules["Ё"] = "~";
rules["ё"] = "`";
rules["\""] = "@";
rules["№"] = "#";
rules["й"] = "q";
rules["ц"] = "w";
rules["у"] = "e";
rules["к"] = "r";
rules["е"] = "t";
rules["н"] = "y";
rules["г"] = "u";
rules["ш"] = "i";
rules["щ"] = "o";
rules["з"] = "p";
rules["ф"] = "a";
rules["ы"] = "s";
rules["в"] = "d";
rules["а"] = "f";
rules["п"] = "g";
rules["р"] = "h";
rules["о"] = "j";
rules["л"] = "k";
rules["д"] = "l";
rules["я"] = "z";
rules["ч"] = "x";
rules["с"] = "c";
rules["м"] = "v";
rules["и"] = "b";
rules["т"] = "n";
rules["ь"] = "m";
rules["Й"] = "Q";
rules["Ц"] = "W";
rules["У"] = "E";
rules["К"] = "R";
rules["Е"] = "T";
rules["Н"] = "Y";
rules["Г"] = "U";
rules["Ш"] = "I";
rules["Щ"] = "O";
rules["З"] = "P";
rules["Ф"] = "A";
rules["Ы"] = "S";
rules["В"] = "D";
rules["А"] = "F";
rules["П"] = "G";
rules["Р"] = "H";
rules["О"] = "J";
rules["Л"] = "K";
rules["Д"] = "L";
rules["Я"] = "Z";
rules["Ч"] = "X";
rules["С"] = "C";
rules["М"] = "V";
rules["И"] = "B";
rules["Т"] = "N";
rules["Ь"] = "M";
rules["х"] = "[";
rules["Х"] = "{";
rules["ъ"] = "]";
rules["Ъ"] = "}";
rules["ж"] = ";";
rules["Ж"] = ":";
rules["э"] = "'";
rules["Э"] = "\"";
rules["б"] = ",";
rules["Б"] = "<";
rules["ю"] = ".";
rules["Ю"] = ">";
rules["?"] = "&";
rules["."] = "/";

    function setCharAt(str,index,chr) {
        if(index > str.length-1) return str;
        return str.substr(0,index) + chr + str.substr(index+1);
    }

    function _str_revert(str) {
	var text = str;	
	if (text.length > 0) {
		for (var i=0; i<text.length; i++) {
			var key = text.charAt(i);			
			if (rules.hasOwnProperty(key))
				text = setCharAt(text, i, rules[key]);				
		}
	}
	return text;
    }
    function onFilterRegion(data, submit)
    {
        if( submit ) {
            var $form = $('#j-f-form');
            var url = $form.attr('action').replace(app.hostSearch, data.link);
            if (typeof jBBSSearch === 'object') {
                var query = jBBSSearch.prepareQuery($form, true);
                window.location.href = url + (query.length > 0 ? '?'+query : '');
                return;
            }
            $form.attr('action', url);
            $form.append('<input type="hidden" name="region" value="'+data.id+'" />');
            $form.submit();
        }
    }

    /**
     * region-confirm (desktop)
     */
    var $confirmDesktop = $('#j-f-region-desktop-confirm');
    if ($confirmDesktop.length) {
        $confirmDesktop.on('click', '.j-confirm-yes', function(){
            var $link = $(this);
            bff.ajax(bff.ajaxURL('geo','filter-confirm-region'),{region_id:$link.data('id')},function(data){
                if (data && data.success) {
                    var redirect = $link.data('redirect');
                    if (redirect) {
                        bff.redirect(redirect);
                    } else if (data.hasOwnProperty('redirect')) {
                        bff.redirect(data.redirect)
                    } else {
                        location.reload();
                    }
                }
                $confirmDesktop.hide();
            });
            return false;
        }).on('click', '.j-confirm-no', function(){
            $confirmDesktop.hide();
            $('#j-f-region-desktop-link').text($(this).data('filter-text')).triggerHandler('click');
            return false;
        });
    }

    /**
     * region-filter (desktop)
     */
    app.popup('f-region-desktop', '#j-f-region-desktop-popup', '#j-f-region-desktop-link', {onInit: function($p){
        var _this = this;
        var $st1 = $p.find('#j-f-region-desktop-st1');
        var $st2 = $p.find('#j-f-region-desktop-st2'), st2cache = {}, st2citySel = '.f-navigation__region_change__links a, .f-navigation__region_change__links .hidden-link';
        function doFilter(type, $link)
        {
            var f = $link.metadata(); f['type'] = type;
            f['link'] = ($link.hasClass('hidden-link') ? $link.data('link') : $link.attr('href'));
            _this.getLink().text(f.title);
            onFilterRegion(f, true);
        }
        $st1.on('click', st2citySel, function(){
            var region = $(this).metadata();
            if( st2cache.hasOwnProperty(region.id) ) {
                $st2.html(st2cache[region.id].html).add($st1).toggleClass('hide');
            } else {
                bff.ajax(bff.ajaxURL('geo','filter-desktop-step2'), {region_id:region.id}, function(data){
                    if(data && data.success) {
                        $st2.html((st2cache[region.id] = data).html).add($st1).toggleClass('hide');
                    }
                });
            }
            return false;
        });
        var $st1q = $st1.find('#j-f-region-desktop-st1-q'), $st1v = false;
        $st1q.on('keyup',function(){ // filter regions by 'title'
            if( $st1v == false) $st1v = $st1.find('#j-f-region-desktop-st1-v');
            var q = $st1q.val().toLowerCase();
            var q2 = _str_revert(q);
            $st1v.find('ul, li').show();
            if( q == '' ) return false;
            $st1v.find('a').each(function(){
                if($(this).attr('title').toLowerCase().indexOf(q) == -1 && $(this).attr('title').toLowerCase().indexOf(q2) == -1) $(this).parent().hide();
            });
            $st1v.find('ul.rel').each(function(){
                if($(this).show().find('li:visible').length == 1) $(this).hide();
            });
            $st1v.find('li.span3').each(function(){
                if( ! $(this).show().find('ul:visible').length) $(this).hide();
            });
            return false;
        });
        $st1.on('click', '#j-f-region-desktop-all', function(e){ nothing(e);
            _this.hide(); // search in "all" (reset region filter)
            doFilter('all', $(this));
            return false;
        });
        $st2.on('click', st2citySel, function(e){ nothing(e);
            _this.hide(); // search in "city"
            var $link = $(this);
            $link.metadata().title = $link.attr('title');
            $link.addClass('active').siblings().removeClass('active');
            doFilter('city', $link);
            return false;
        });
        $st2.on('click', '.j-f-region-desktop-st2-region', function(e){ nothing(e);
            $st2.find(st2citySel).removeClass('active');
            _this.hide(); // search in "region"
            doFilter('region', $(this));
            return false;
        });
        $st2.on('click', '.j-f-region-desktop-back', function(e){ nothing(e);
            $st2.add($st1).toggleClass('hide'); // reset "city" filter
            return false;
        });
    }});

    /**
     * country-filter (desktop)
     */
    app.popup('f-country-desktop', '#j-f-country-desktop-popup', '#j-f-region-desktop-link', {onInit: function($p){
        var _this = this;
        function doFilter(type, $link)
        {
            var f = $link.metadata(); f['type'] = type;
            f['link'] = ($link.hasClass('hidden-link') ? $link.data('link') : $link.attr('href'));
            _this.getLink().text(f.title);
            onFilterRegion(f, true);
        }

        var $st0 = $p.find('#j-f-country-desktop-st0');
        var $st1 = $p.find('#j-f-region-desktop-st1'), st1cache = {};
        var $st1v = $st1.find('#j-f-region-desktop-st1-v');
        var $st1title = $st1.find('#j-f-region-desktop-country-title');
        var $country = false;
        $st0.on('click', '.f-navigation__country_change__links a', function(){
            $country = $(this);
            $st1title.text($country.text());
            var country = $country.metadata();
            if(intval(country.noregions)){
                $st1v.addClass('hide');
                if (st2cache.hasOwnProperty(country.id)) {
                    $st2.html(st2cache[country.id].html).add($st1).toggleClass('hide');
                    $st0.toggleClass('hide');
                } else {
                    bff.ajax(bff.ajaxURL('geo', 'filter-desktop-city-noregions'), {region_id: country.id}, function (data) {
                        if (data && data.success) {
                            $st2.html((st2cache[country.id] = data).html).add($st1).toggleClass('hide');
                            $st0.toggleClass('hide');
                        }
                    });
                }
            }else{
                $st1v.removeClass('hide');
                if (st1cache.hasOwnProperty(country.id)) {
                    $st1v.html(st1cache[country.id].html);
                    $st1.add($st0).toggleClass('hide');
                } else {
                    bff.ajax(bff.ajaxURL('geo', 'filter-desktop-step1'), {region_id: country.id}, function (data) {
                        if (data && data.success) {
                            $st1v.html((st1cache[country.id] = data).html);
                            $st1.add($st0).toggleClass('hide');
                        }
                    });
                }
            }
            return false;
        });
        $st1.on('click', '#j-f-country-desktop-all', function(e){ nothing(e);
            _this.hide(); // search in "all" (reset region filter)
            if( ! $country) {
                $country = $(this);
            }
            doFilter('country', $country);
            return false;
        });
        $st1.on('click', '.j-f-region-desktop-back', function(e){ nothing(e);
            $st0.toggleClass('hide');
            $st1.toggleClass('hide');
            $st2.addClass('hide');
            return false;
        });
        var $st1q = $st1.find('#j-f-region-desktop-st1-q');
        $st1q.on('keyup',function(){ // filter regions by 'title'
            var q = $st1q.val().toLowerCase();
            $st1v.find('ul, li').show();
            if( q == '' ) return false;
            $st1v.find('a').each(function(){
                if($(this).attr('title').toLowerCase().indexOf(q) == -1) $(this).parent().hide();
            });
            $st1v.find('ul.rel').each(function(){
                if($(this).show().find('li:visible').length == 1) $(this).hide();
            });
            $st1v.find('li.span3').each(function(){
                if( ! $(this).show().find('ul:visible').length) $(this).hide();
            });
            return false;
        });
        var $st2 = $p.find('#j-f-region-desktop-st2'), st2cache = {}, st2citySel = '.f-navigation__region_change__links a, .f-navigation__region_change__links .hidden-link';
        $st1.on('click', st2citySel, function(){
            var region = $(this).metadata();
            if( st2cache.hasOwnProperty(region.id) ) {
                $st2.html(st2cache[region.id].html).add($st1).toggleClass('hide');
            } else {
                bff.ajax(bff.ajaxURL('geo','filter-desktop-step2'), {region_id:region.id}, function(data){
                    if(data && data.success) {
                        $st2.html((st2cache[region.id] = data).html).add($st1).toggleClass('hide');
                    }
                });
            }
            return false;
        });
        $st2.on('click', '.j-f-region-desktop-back', function(e){ nothing(e);
            $st2.add($st1).toggleClass('hide'); // reset "city" filter
            return false;
        });
        $st0.on('click', '#j-f-region-desktop-all', function(e){ nothing(e);
            _this.hide(); // search in "all" (reset region filter)
            doFilter('all', $(this));
            return false;
        });
        $st2.on('click', st2citySel, function(e){ nothing(e);
            _this.hide(); // search in "city"
            var $link = $(this);
            $link.metadata().title = $link.attr('title');
            $link.addClass('active').siblings().removeClass('active');
            doFilter('city', $link);
            return false;
        });
        $st2.on('click', '.j-f-region-desktop-st2-region', function(e){ nothing(e);
            $st2.find(st2citySel).removeClass('active');
            _this.hide(); // search in "region"
            doFilter('region', $(this));
            return false;
        });
    }});

    /**
     * region-filter (phone)
     */
    app.popup('f-region-phone', '#j-f-region-phone-popup', '#j-f-region-phone-link', {onInit: function($p){
        var _this = this;
        var $q = $p.find('#j-f-region-phone-q');
        var $qList = $p.find('#j-f-region-phone-q-list'), qListPresuggest = $qList.html();
        function doFilter(data)
        {
            $q.val('');
            qList(qListPresuggest, '');
            _this.getLink().find('span').text(data.title);
            onFilterRegion(data, true);
        }
        function qList(data, q)
        {
            var $notFoundList = $qList.next();
            if( ! data.length ) {
                $qList.hide().html(qListPresuggest);
                $notFoundList.find('.word').text(q);
                $notFoundList.show();
            } else {
                $notFoundList.hide();
                $qList.html(data).show();
            }
        }
        $q.on('keyup',function(){
            var q = this.value.toLowerCase();
            if( ! q.length) {
                qList(qListPresuggest, '');
            } else if(q.length >=2 ) {
                bff.ajax(bff.ajaxURL('geo', 'filter-phone-suggest'),{q:q},function(data){
                    if(data) {
                        qList(data.html, q);
                    }
                });
            }
        }).next().on('click',function(e){ nothing(e);
            $q.trigger('keyup');
        });
        $qList.on('click', 'li', function(e){ nothing(e);
            _this.hide(); // search in "region/city/all"
            doFilter( $(this).metadata() );
            return false;
        });
    }});
});