* Зарегистрируйтесь на сайте https://onesignal.com/

* Создайте новое приложение (Add a new app) и получите необходимые ключи для дальнейшей настройки плагина.

![Создание нового приложения]({plugin:url}img/instruction-1.png "Создание нового приложения")

* Укажите название своего приложения.

![Название нового приложения]({plugin:url}img/instruction-2.png "Название нового приложения")

* Выберите платформу Web Push

![Выбор платформы]({plugin:url}img/instruction-3.png "Выбор платформы")

* Выберите тип интеграции "Custom Code"

![Выбор интеграции]({plugin:url}img/instruction-4.png "Выбор интеграции")

![Для HTTP сайтов]({plugin:url}img/instruction-5.png "Для HTTP сайтов")

* После нажатия на кнопку "Finish" перейдите во вкладку "Keys & IDs"

![Получение ключей]({plugin:url}img/instruction-6.png "Получение ключей")

* Скопируйте ключи и вставьте в соответствующие поля настроек плагина

![Ключи]({plugin:url}img/instruction-7.png "Ключи")

* Заполните остальные важные поля формы настроек плагина

* Включите плагин

Документация по API Onesignal https://documentation.onesignal.com/docs/web-push-setup
