<?php

class Plugin_User_Online_Do_p0bdd61 extends Plugin
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title'   => 'Статус пользователя «Онлайн»',
            'plugin_version' => '1.0.1',
            'plugin_alias'   => 'user_online_do',
            'extension_id'   => 'p0bdd613b95e07bfe54aa75f485ed8cc77929572',
        ));

        $settings = array(
            'onlineTime' => array(
                'title' => $this->langAdmin('Время'),
                'input' => 'number',
                'min' => 1,
                'tip' => $this->langAdmin('минут'),
                'default' => 5,
                'description' => $this->langAdmin('Таймаут активности пользователя в минутах.<br/>Если пользователь не проявляет активности больше указанного времени, то он считается офлайн.'),
            ),
            'page_itemView' => array(
                'title' => $this->langAdmin('Страницы'),
                'input' => 'checkbox',
                'type'  => TYPE_BOOL,
                'default' => true,
                'description' => $this->langAdmin('Просмотр объявления'),
            ),
            'page_itemShopView' => array(
                'input' => 'checkbox',
                'type'  => TYPE_BOOL,
                'default' => true,
                'description' => $this->langAdmin('Просмотр объявления от магазина'),
            ),
            'page_userProfile' => array(
                'input' => 'checkbox',
                'type'  => TYPE_BOOL,
                'default' => true,
                'description' => $this->langAdmin('Все объявления пользователя'),
            ),
            'page_cabinetMessages' => array(
                'input' => 'checkbox',
                'type'  => TYPE_BOOL,
                'default' => false,
                'description' => $this->langAdmin('Сообщения пользователя'),
            ),
            'page_cabinetMessagesChat' => array(
                'input' => 'checkbox',
                'type'  => TYPE_BOOL,
                'default' => false,
                'description' => $this->langAdmin('Переписка с пользователем'),
            ),
        );

        if ( ! bff::moduleExists('shops')) {
            unset($settings['page_itemShopView']);
        }

        $this->configSettings($settings);
    }

    protected function start()
    {
        bff::hookAdd($this->hookKey(), function($user_id, $page_key){
            echo $this->view($user_id, $page_key);
        });
    }

    public function hookKey()
    {
        return 'plugin.user_online_do.block';
    }

    /**
     * Формируем HTML блок "онлайн"
     * @param integer $user_id ID пользователя
     * @param string $page_key ключ страницы или пустая строка
     * @return string HTML
     */
    public function view($user_id, $page_key = '')
    {
        if (is_string($page_key) && !empty($page_key)) {
            if ( ! $this->config($page_key)) {
                return '';
            }
        }
        $data = Users::model()->userData($user_id, ['last_activity', 'user_id']);
        if (empty($data)) {
            return '';
        }

        $data['online'] = $this->isOnline($data['last_activity']);
        $data['last_activity'] = $this->lang('Был на сайте: [date]', [
            'date' => tpl::date_format2($data['last_activity'], true),
        ]);
        $data['page_key'] = $page_key;

        return $this->viewPHP($data, 'tpl/view');
    }

    /**
     * Сравниваем дату и время с текущим
     * @param string $datetime дата
     * @return bool
     */
    protected function isOnline($datetime)
    {
        if (empty($datetime)) {
            return false;
        }
        if (is_string($datetime)) {
            $datetime = strtotime($datetime);
        }
        return ((BFF_NOW - $datetime) < $this->config('onlineTime', 5) * 60);
    }
}

if ( ! function_exists('plugin_user_online_do_block')) {
    function plugin_user_online_do_block($user_id, $page_key = '')
    {
        /** @var Plugin_User_Online_Do_p0bdd61 */
        $plugin = bff::plugin('user_online_do');
        if ($plugin !== false) {
            return $plugin->view($user_id, $page_key);
        }
        return '';
    }
}