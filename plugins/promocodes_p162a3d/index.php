<?php

class Plugin_Promocodes_p162a3d extends Plugin
{
    const TABLE_PROMOCODES = 'promocodes';

    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title'   => 'Промокоды',
            'plugin_version' => '1.0.0',
            'extension_id'   => 'p162a3dcf99f5409394ce81f93ff44da837c6c56',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            //
        ));
    }

    function tablePromocodes() {
        return DB_PREFIX . self::TABLE_PROMOCODES;
    }

    public function install()
    {
        $this->db->exec("CREATE TABLE IF NOT EXISTS `" . $this->tablePromocodes() . "` (
            `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
            `user_id` int(11) unsigned NOT NULL DEFAULT '0',
            `amount` decimal(11,2) unsigned NOT NULL DEFAULT '0',
            `code` varchar(150) NOT NULL DEFAULT '',
            `created` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `expired` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `used` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            PRIMARY KEY (`id`)
            )ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");

        return parent::install();
    }

    public function uninstall()
    {
        $this->db->exec("DROP TABLE " . $this->tablePromocodes() . ";");

        return parent::uninstall();
    }

    protected function start()
    {
        $this->adminMenu('Промокоды', 'Список промокодов', 'listing');
        $this->adminMenu('Промокоды', 'Добавление промокодов', 'add');

        bff::hookAdd('users.header.user.menu', function($menu, $aData) {
            $menu['promocode'] = array(
                'i'   => 'fa fa-money-bill',
                't'   => _t('promocodes', 'Промокоды'),
                'url' => '/cabinet/promocode/',
                'priority' => 6,
                'callback' => array($this, 'my_promo'),
            );

            return $menu;
        });
        bff::hookAdd('users.cabinet.tabs', function($tabs, $aData) {
            $tabs['promocode'] = array(
                'i'   => 'fa fa-money-bill',
                't'   => _t('promocodes', 'Промокоды'),
                'priority' => 5,
                'url' => '/cabinet/promocode/',
                'callback' => array($this, 'my_promo'),
            );

            return $tabs;
        });
        bff::hookAdd('users.user.register', function($nUserID, $aData) {
            $sPromoCode = $this->input->postget('promocode', TYPE_STR);
            $this->usePromoCode($sPromoCode, $nUserID);
        });
    }

    public function my_promo() {
        if (Request::isPOST()) { # ajax
            $aResponse = array();
            $sPromoCode = $this->input->post('promocode', TYPE_STR);
            $result = $this->usePromoCode($sPromoCode);

            if ($result) {
                $aResponse['msg'] = _t('promocodes', 'Промокод успешно активирован!');
            } else {
                $this->errors->set(_t('promocodes', 'Промокод не найден либо уже использован!'));
            }

            $this->ajaxResponseForm($aResponse);
        }

        $aData = [];
        return $this->viewPHP($aData, 'tpl/my.promo');
    }

    static public function generate($mask = 'XXXX-XXXX-XXXX') {
        $uppercase    = ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'];
        $numbers      = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        $characters   = array_merge($numbers, $uppercase);
        $coupon = '';

        for ($i = 0; $i < strlen($mask); $i++) {
            if ($mask[$i] === 'X') {
                $coupon .= $characters[mt_rand(0, count($characters) - 1)];
            } else {
                $coupon .= $mask[$i];
            }
        }

        return $coupon;
    }

    private function checkIfNotExist($sCode) {
        return empty($this->promoCodeData($sCode)) ? true : false;
    }

    private function promoCodeData($sCode) {
        return $this->db->one_array('SELECT * FROM ' . $this->tablePromocodes() . ' WHERE code = :code', array(':code' => $sCode));
    }

    public function createPromoCodes($nAmount = 5, $nNumber = 10, $nPeriod = 90) {
        if (empty($nAmount)) {
            $this->errors->set(_t('promocodes', 'Номинал промокода(-ов) не указан!'));
            return false;
        }
        if (empty($nNumber)) {
            $this->errors->set(_t('promocodes', 'Количество промокодов для генерации не указано!'));
            return false;
        }
        if (empty($nPeriod)) {
            $this->errors->set(_t('promocodes', 'Период действия промокодов не указан!'));
            return false;
        }
        $aCodes = [];
        for ($i = 0; $i < $nNumber; $i++) {
            do {
                $sCode = self::generate();
            } while (!$this->checkIfNotExist($sCode));
            $aCodes[] = $sCode;
        }
        $mFrom = strtotime($this->db->now());
        $mFormat = 'Y-m-d H:i:s';
        $expired =  date($mFormat, strtotime('+' . $nPeriod . ' days', $mFrom));

        foreach ($aCodes as $code) {
            $this->savePromoCode([
                'amount' => $nAmount,
                'code' => $code,
                'expired' => $expired
            ]);
        }

        return true;
    }

    public function savePromoCode($aData) {
        if (empty($aData['amount'])) {
            return false;
        }
        if (empty($aData['code'])) {
            return false;
        }
        if (empty($aData['expired'])) {
            return false;
        }
        $aData['created'] = $this->db->now();

        return $this->db->insert($this->tablePromocodes(), $aData);
    }

    public function checkPromoCode($sCode) {
        if (empty($sCode)) {
            return false;
        }

        $aData = $this->promoCodeData($sCode);
        if (empty($aData)) {
            return false;
        } else {
            $now = time();
            $expired = strtotime($aData['expired']);
            if (!empty($expired) && (!empty($aData['user_id']) || $expired <= $now)) {
                return false;
            }
        }

        return true;
    }

    private function checkPromoCodeAsUsed($nPromoCodeID, $nUserID = 0) {
        $nUserID = empty($nUserID) ? User::id() : $nUserID;
        $this->db->update($this->tablePromocodes(), [
            'user_id' => $nUserID,
            'used' => $this->db->now()
        ], array('id' => $nPromoCodeID));
    }

    public function usePromoCode($sCode, $nUserID = 0) {
        if (!$this->checkPromoCode($sCode)) {
            return false;
        }

        $nUserID = empty($nUserID) ? User::id() : $nUserID;
        $aCodeData = $this->promoCodeData($sCode);

        $aUser = Users::model()->userData($nUserID, ['balance']);
        Bills::i()->updateUserBalance($nUserID, $aCodeData['amount'], true);
        Bills::i()->createBill_InGift($nUserID, ($aUser['balance'] + $aCodeData['amount']), $aCodeData['amount'], 'Использование промокода');

        $this->checkPromoCodeAsUsed($aCodeData['id'], $nUserID);

        return true;
    }

    public function promoCodesListing(array $filter, array $opts = array()) {
        func::array_defaults($opts, array(
            'limit'    => 25,
            'offset'   => 0,
            'orderBy'  => 'id DESC'
        ));

        if ($opts['offset'] > 0) {
            $opts['limit'] = array($opts['offset'], $opts['limit']);
        }

        if (isset($filter[':query'])) {
            $filter[':query'] = array(
                'code LIKE (:query)',
                ':query' => '%' . $filter[':query'] . '%',
            );
        }

        $select = $this->db->select_prepare($this->tablePromocodes(), array('*'), $filter, $opts);

        $aList = $this->db->select($select['query'], $select['bind']);
        foreach ($aList as $k => $v) {
            $now = time();
            $expired = strtotime($v['expired']);
            if ($v['user_id']> 0 || $expired <= $now) {
                $aList[$k]['status'] = 0;
            } else {
                $aList[$k]['status'] = 1;
            }
        }

        return $aList;
    }

    public function listing()
    {
        $f = $this->input->postgetm(array(
            'page'          => TYPE_UINT,
            'title'         => array(TYPE_NOTAGS, 'len' => 150),
            'status'        => TYPE_UINT
        ));

        # формируем фильтр списка промокодов
        $limit = 25;
        $sql = array();

        if ( ! empty($f['title'])) {
            $sql[':query'] = $f['title'];
        }

        $nCount = $this->db->one_data('SELECT COUNT(*) FROM ' . $this->tablePromocodes() . ' WHERE 1');
        $aData['f'] = $f;

        $oPgn = new Pagination($nCount, $limit, '#', 'jPromoCodes.page(' . Pagination::PAGE_ID . '); return false;');
        $aData['pgn'] = $oPgn->view();
        $aData['list'] = $this->promoCodesListing($sql, array(
            'limit'   => $oPgn->getLimit(),
            'offset'  => $oPgn->getOffset()
        ));
        $aData['list'] = $this->viewPHP($aData, 'tpl/admin.codes.listing.ajax');

        if (Request::isAJAX()) {
            $this->ajaxResponse(array(
                'list'   => $aData['list'],
                'pgn'    => $aData['pgn'],
                'filter' => $f,
            ));
        }

        return $this->viewPHP($aData, 'tpl/admin.codes.listing');
    }

    public function add()
    {
        if (Request::isPOST()) { # ajax
            $aResponse = array();
            $nAmount = $this->input->post('amount', TYPE_UNUM);
            $nNumber = $this->input->post('number', TYPE_UINT);
            $nPeriod = $this->input->post('period', TYPE_UINT);
            $result = $this->createPromoCodes($nAmount, $nNumber, $nPeriod);

            if ($result) {
                $aResponse['msg'] = _t('promocodes', 'Промокоды успешно созданы!');
            }

            $this->ajaxResponseForm($aResponse);
        }

        $aData = [];

        return $this->viewPHP($aData, 'tpl/admin.add');
    }
}