<? foreach($list as $k=>$v) { $id = $v['id']; ?>
<tr <? if(empty($v['status'])) { ?>style="background-color: #ebcccc;"<? } else { ?>style="background-color: #CBFFD1;"<? } ?>>
    <td><span class="small"><?= $id ?></span></td>
    <td class="left">
        <?= $v['code'] ?>
    </td>
    <td>
        <?= number_format($v['amount'], 2) ?>
    </td>
    <td>
        <span><?= tpl::date_format3($v['expired']); ?></span>
    </td>
    <td>
        <span><?= tpl::date_format3($v['used']); ?></span>
    </td>
    <td>
        <span><?= tpl::date_format3($v['created']); ?></span>
    </td>
</tr>
<? } if(empty($list)) { ?>
<tr class="norecords">
    <td colspan="6"><?= _t('', 'Nothing found') ?></td>
</tr>
<? }