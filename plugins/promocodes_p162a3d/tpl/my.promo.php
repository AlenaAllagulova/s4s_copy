<div class="u-cabinet__settings">
    <div class="u-cabinet__settings__block">
        <form class="form-horizontal rel" action="" id="j-promocode-form">
            <div class="u-cabinet__settings__block">
                <div class="u-cabinet__settings__block__title"><span><?= _t('promocodes', 'Ввод промокода') ?></span></div>
                <div class="u-cabinet__settings__block__content rel">
                    <div class="u-cabinet__settings__block__form rel">
                        <div class="control-group">
                            <div class="controls">
                                <input type="text" name="promocode" value="" class="input-block-level" maxlength="50" id="promocode"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <input type="button" class="btn btn-success j-submit" value="<?= _t('promocodes', 'Ввести!') ?>" onclick="jPromoCodes.submit();"/>
                                <span class="i-formpage__cancel_mobile btn-link cancel" onclick="history.back();"><?= _t('', 'Отмена') ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    <? js::start(); ?>

    var jPromoCodes = (function()
    {
        var $progress, $form, $code;
        var _processing = false;

        $(function(){
            $progress = $('#progress-items');
            $form = $('#j-promocode-form');
            $code = $('#promocode');
        });

        function isProcessing()
        {
            return _processing;
        }

        function updateList()
        {
            if(isProcessing()) return;
            _processing = true;
            var f = $form.serialize();
            bff.ajax('', f, function(resp, errors){
                if (resp && resp.success) {
                    $code.value = '';
                    app.alert.success(resp.msg);
                } else {
                    app.alert.error(errors);
                }

                _processing = false;
            }, $progress);
        }

        return {
            submit: function()
            {
                if(isProcessing()) return false;
                updateList();
                return true;
            },
        };
    }());

    <? js::stop(); ?>
</script>
