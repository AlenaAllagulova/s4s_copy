<?php
    /**
     * @var $this BBS
     */
    tpl::includeJS(array('datepicker'), true);

    $aTabs = bff::filter('admin.codes.list.tabs', array(
        0 => array('t'=>_t('promocodes', 'Все промокоды')),
    ));

    tplAdmin::adminPageSettings(array(
        'link'=>array('title'=>_t('promocodes', '+ добавить промокоды'), 'href'=>$this->adminLink('add')),
    ));
?>

<div class="tabsBar" id="items-status-tabs">
    <? foreach($aTabs as $k=>$v) { ?>
    <span class="tab<?= $k==$f['status'] ? ' tab-active' : '' ?>"><a href="#" onclick="return jPromoCodes.onStatus(<?= $k ?>, this);"<?= (!empty($v['c']) ? $v['c'] : '') ?>><?= $v['t'] ?><? if(! empty($v['counter'])){ ?> (<span class="j-counter"><?= $v['counter'] ?></span>)<? } ?></a></span>
    <? } ?>
    <span class="progress pull-right" style="display:none; position: absolute; right: 10px; top: 20px;" id="progress-items"></span>
</div>

<div class="actionBar">
    <form action="" method="get" name="filters" id="items-filters" class="form-inline" onsubmit="return false;">
        <input type="hidden" name="page" value="<?= $f['page'] ?>" />
        <input type="hidden" name="status" value="<?= $f['status'] ?>" />
        <div class="controls controls-row">
            <div class="left">
                <input type="text" maxlength="150" name="title" id="items-title-or-id" placeholder="<?= _te('promocodes', 'Поиск промокода по его части'); ?>" value="<?= HTML::escape($f['title']) ?>" style="width: 125px;" />
            </div>
            <div class="left" style="margin-left: 4px;">
                <div class="btn-group">
                <input type="submit" class="btn btn-small" onclick="jPromoCodes.submit(false);" value="<?= _te('', 'найти'); ?>" />
                <a class="btn btn-small" onclick="jPromoCodes.submit(true); return false;" title="<?= _te('', 'сбросить'); ?>"><i class="disabled icon icon-refresh"></i></a>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</div>
<table class="table table-condensed table-hover admtbl">
<thead>
    <tr>
        <th width="30"><?= _t('', 'ID'); ?></th>
        <th class="left" style="padding-left: 10px;"><?= _t('', 'Промокод'); ?></th>
        <th width="130"><?= _t('promocodes', 'Сумма'); ?></th>
        <th width="130"><?= _t('promocodes', 'Годен до'); ?></th>
        <th width="130"><?= _t('promocodes', 'Использован'); ?></th>
        <th width="130"><?= _t('', 'Created'); ?></th>
    </tr>
</thead>
<tbody id="items-list">
<?= $list ?>
</tbody>
</table>
<div id="items-pgn"><?= $pgn; ?></div>

<script type="text/javascript">
var jPromoCodes = (function()
{
    var $progress, $list, $listPgn, filters, $tabs;
    var url = '<?= $this->adminLink('listing'); ?>';
    var status = intval(<?= $f['status'] ?>);
    var _processing = false; 
    
    $(function(){
        $progress = $('#progress-items');
        $list     = $('#items-list');
        $listPgn  = $('#items-pgn');
        filters   = $('#items-filters').get(0);
        $tabs     = $('#items-status-tabs');
    });
    
    function isProcessing()
    {
        return _processing;
    }
    
    function setPage(id)
    {
        filters.page.value = intval(id);
    }

    function updateList()
    {
        if(isProcessing()) return;
        _processing = true;
        $list.addClass('disabled');
        var f = $(filters).serialize();
        bff.ajax(url, f, function(data){
            if(data) {
                $list.html( data.list );
                $listPgn.html( data.pgn );
                if(bff.h) {
                    window.history.pushState({}, document.title, url + '&' + f);
                }
            }
            $list.removeClass('disabled');

            _processing = false;
        }, $progress);
    }

    return {
        submit: function(resetForm)
        {
            if(isProcessing()) return false;
            setPage(1);
            if(resetForm) {
                filters.title.value = '';
            }
            updateList();
            return true;
        },
        refresh: function()
        {
            updateList();
        },
        page: function (id)
        {
            if(isProcessing()) return false;
            setPage(id);
            updateList();
            return true;
        },
        onStatus: function(statusNew, link)
        {
            if(isProcessing() || status == statusNew) return false;
            status = statusNew;
            setPage(1);
            filters.status.value = statusNew;
            updateList();
            $(link).parent().addClass('tab-active').siblings().removeClass('tab-active');
            return false;
        },
    };
}());
</script>