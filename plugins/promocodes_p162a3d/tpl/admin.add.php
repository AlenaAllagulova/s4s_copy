<div class="u-cabinet__settings">
    <div class="u-cabinet__settings__block">
        <form class="form-horizontal rel" action="" id="j-promocode-form">
            <div class="u-cabinet__settings__block">
                <div class="u-cabinet__settings__block__title"><span><?= _t('promocodes', 'Генерация промокодов') ?></span></div>
                <div class="u-cabinet__settings__block__content rel">
                    <div class="u-cabinet__settings__block__form rel">
                        <div class="control-group">
                            <label class="control-label"><?= _t('promocodes', 'Сумма (в валюте доски)') ?><span class="required-mark">*</span></label>
                            <div class="controls">
                                <input type="number" name="amount" value="5" class="input-mini" pattern="[0-9\.,\-]*" min="1" max="5000">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"><?= _t('promocodes', 'Количество промокодов') ?><span class="required-mark">*</span></label>
                            <div class="controls">
                                <input type="number" name="number" value="10" class="input-mini" pattern="[0-9]*" min="1" max="5000">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"><?= _t('promocodes', 'Период действия промокодов, дней') ?><span class="required-mark">*</span></label>
                            <div class="controls">
                                <input type="number" name="period" value="90" class="input-mini" pattern="[0-9]*" min="1" max="730">
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="controls">
                                <input type="button" class="btn btn-success j-submit" value="<?= _t('promocodes', 'Ввести!') ?>" onclick="jPromoCodes.submit();"/>
                                <span class="i-formpage__cancel_mobile btn-link cancel" onclick="history.back();"><?= _t('', 'Отмена') ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    var jPromoCodes = (function()
    {
        var $progress, $form;
        var _processing = false;

        $(function(){
            $progress = $('#progress-items');
            $form = $('#j-promocode-form');
        });

        function isProcessing()
        {
            return _processing;
        }

        function updateList()
        {
            if(isProcessing()) return;
            _processing = true;
            var f = $form.serialize();
            bff.ajax('', f, function(resp){
                if (resp.data.success) {
                    app.alert.success(resp.data.msg);
                } else {
                    app.alert.error(resp.errors);
                }

                _processing = false;
            }, $progress);
        }

        return {
            submit: function()
            {
                if(isProcessing()) return false;
                updateList();
                return true;
            },
        };
    }());
</script>