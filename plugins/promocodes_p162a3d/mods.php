<?php

return array(
    array(
        'file' => '/modules/users/tpl/def/auth.register.php',
        'search' => '<? if($captcha_on) { ?>',
        'replace' => '<div class="control-group">
                    <label class="control-label" for="j-u-register-promocode"><?= _t(\'promocodes\', \'У вас есть промокод?\') ?></label>
                    <div class="controls">
                        <input type="text" name="promocode" id="j-u-register-promocode" autocomplete="off" placeholder="<?= _te(\'promocodes\', \'Введите ваш промокод тут\') ?>" maxlength="100" />
                    </div>
                </div>',
        'position' => 'before',
        'index' => 1,
        'offset' => 1,
    ),
);

