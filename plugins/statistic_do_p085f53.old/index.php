<?php

class Plugin_Statistic_Do_p085f53 extends Plugin
{
    const TYPE_BBS_ITEMS_CREATED    = 1;
    const TYPE_USERS_CREATED        = 2;
    const TYPE_SHOPS_CREATED        = 3;
    const TYPE_USAGE_IMAGES         = 4;
    const TYPE_USAGE_CACHE          = 5;
    const TYPE_USAGE_CODE           = 6;
    const TYPE_USAGE_DB             = 7;

    /** @var \Plugin_Statistic_Do_p085f53_model объект модели */
    public $model;

    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title'   => 'Dashboard с общей статистикой',
            'plugin_version' => '1.0.0',
            'plugin_alias'   => 'statistic_do',
            'extension_id'   => 'p085f5316a2d40e165571547f4bd275e69edc0ad',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            'calculate_drive_size' => array(
                'title' => $this->langAdmin('Обновление статистики'),
                'input' => 'select',
                'options' => array(
                    '27 * * * *'    => array('title'=>$this->langAdmin('раз в час')),
                    '27 */3 * * *'  => array('title'=>$this->langAdmin('раз в 3 часа')),
                    '27 */6 * * *'  => array('title'=>$this->langAdmin('раз в 6 часов')),
                    '27 3 * * *'    => array('title'=>$this->langAdmin('раз в день')),
                    '27 3 */3 * *'  => array('title'=>$this->langAdmin('раз в 3 дня')),
                ),
                'default' => '27 3 * * *',
            ),
            'show_first' => array(
                'title' => '',
                'input' => 'checkbox',
                'default' => true,
                'description' => $this->langAdmin('Отображать статистику при заходе в панель администратора'),
            ),
            'bbs_oneday_ignore' => array(
                'title' => '',
                'input' => 'checkbox',
                'default' => true,
                'description' => $this->langAdmin('Не учитывать объявления удаленные в течении суток после добавления'),
            ),
            'bbs_import_admin_ignore' => array(
                'title' => '',
                'input' => 'checkbox',
                'default' => true,
                'description' => $this->langAdmin('Не учитывать объявления добавленные посредством импорта инициированного администратором'),
            ),
            'users_fake_ignore' => array(
                'title' => '',
                'input' => 'checkbox',
                'default' => true,
                'description' => $this->langAdmin('Не учитывать сгенерированные аккаунты пользователей'),
            ),
            'updated_drive' => array(
                'input' => 'custom',
            ),
        ), array('titleRow' => 160));

        # Модель
        require_once $this->path('model.php');
        $this->model = new Plugin_Statistic_Do_p085f53_model($this);
    }

    protected function start()
    {
        # Добавляем пункт меню в админ. панели
        $tabMain = $this->langAdmin('Статистика');
        bff::hookAdd('admin.menu.tabs', function($tabs, CMenu $menu) use ($tabMain){
            $tabs[$this->getName()] = array(
                'title' => $tabMain,
                'priority' => $this->config('show_first') ? -1 : 1000,
            );
            return $tabs;
        });
        bff::hookAdd('admin.menu.build', function(CMenu $menu) use ($tabMain){
            $menu->assign($tabMain, $this->langAdmin('Результат'), $this->getName(), 'result', true, 1);
        });

        # Изменение счетчиков статистики
        # добавление объявления
        bff::hookAdd('bbs.item.create', function($id, $params) {
            $created = time();
            if ( ! empty($params['data']['created'])) $created = strtotime($params['data']['created']);
            if ( ! empty($params['data']['import']) && $this->isImportAdmin($params['data']['import'])) return;
            $this->model->increment(static::TYPE_BBS_ITEMS_CREATED, $created);
        });

        # Добавим поле import в данные при удалении объявления
        bff::hookAdd('db.query.bbs-items-delete-data.data', function($data){
            if (isset($data['fields'])) {
                $data['fields'][] = 'I.import';
            }
            return $data;
        });

        # удаление объявления
        bff::hookAdd('bbs.item.delete', function($data) {
            if (empty($data['created'])) return;
            if ( ! empty($data['import']) && $this->isImportAdmin($data['import'])) return;
            $created = strtotime($data['created']);
            if ($created + 60 > time()) {
                # удалено в течении минуты после добавление - произошла ошибка при добавлении
                $this->model->decrement(static::TYPE_BBS_ITEMS_CREATED, $created);
                return;
            }
            if ($this->config('bbs_oneday_ignore')) {
                if ($created + 86400 > time()) {
                    $this->model->decrement(static::TYPE_BBS_ITEMS_CREATED, $created);
                    return;
                }
            }
        });

        # Добавление пользователя
        bff::hookAdd('users.user.create', function($id, $params) {
            $created = time();
            if ( ! empty($params['data']['created'])) $created = strtotime($params['data']['created']);
            if ( ! empty($params['data']['fake']) && $this->config('users_fake_ignore')) {
                return;
            }
            $this->model->increment(static::TYPE_USERS_CREATED, $created);
        });

        if (bff::shopsEnabled()){
            # Добавление магазина
            bff::hookAdd('shops.shop.create', function($id, $params){
                $created = time();
                if ( ! empty($params['data']['created'])) $created = strtotime($params['data']['created']);
                $this->model->increment(static::TYPE_SHOPS_CREATED, $created);
            });
        }
    }

    /**
     * Проверка запущен ли импорт администратором
     * @param $importID
     * @return bool|mixed
     */
    public function isImportAdmin($importID)
    {
        static $cache = array();
        do {
            if ( ! $this->config('bbs_import_admin_ignore')) break;
            if (empty($importID)) break;
            if (isset($cache[$importID])) return $cache[$importID];
            $data = BBS::model()->importData($importID);
            if (empty($data['is_admin'])) {
                $cache[$importID] = false;
                break;
            }
            $cache[$importID] = true;
            return true;
        } while(false);
        return false;
    }

    /**
     * Включение плагина
     * @return bool
     */
    protected function enable()
    {
        if ( ! parent::enable()) return false;
        bff::cronManager()->executeOnce($this->getName(), 'cronOnEnabled', array(), false, array('plugin' => 1));
        return true;
    }

    /**
     * Отображение статистики
     * @return string
     */
    public function result()
    {
        $act = $this->input->postget('act', TYPE_STR);
        $response = array();
        switch($act) {
            case 'chart-data':
                $data = $this->input->postm(array(
                    'module' => TYPE_STR,
                    'time'   => TYPE_STR,
                    'type'   => TYPE_STR,
                ));
                if ( ! in_array($data['type'], array('bar', 'line'))) {
                    $this->errors->impossible();
                    break;
                }

                $time = '';
                switch($data['time']) {
                    case 'week': $time = '-6 days'; break;
                    case 'month': $time = '-31 days'; break;
                    case 'quarter': $time = '-91 days'; break;
                    case 'year': $time = '-1 year'; break;
                }
                if (empty($time)) {
                    $this->errors->impossible();
                    break;
                }

                $type = 0;
                switch($data['module']) {
                    case 'bbs': $type = static::TYPE_BBS_ITEMS_CREATED; break;
                    case 'users': $type = static::TYPE_USERS_CREATED; break;
                    case 'shops': $type = static::TYPE_SHOPS_CREATED; break;
                }
                if (empty($type)) {
                    $this->errors->impossible();
                    break;
                }
                $result = $this->model->data($type, strtotime($time));
                if ($data['type'] == 'bar') {
                    if ($data['time'] == 'quarter') {
                        $t = array();
                        foreach($result as $v) {
                            $d = strtotime($v['d']);
                            $w = date('W', $d);
                            if ( ! isset($t[ $w ])) {
                                $v['f'] = date('d', $d);
                                $t[ $w ] = $v;
                            } else {
                                $t[ $w ]['c'] += $v['c'];
                                $t[ $w ]['d'] = $v['d'];
                            }
                        }
                        $result = array();
                        foreach($t as $v) {
                            $result[] = $v;
                        }
                    } else if ($data['time'] == 'year') {
                        $t = array();
                        foreach($result as $v) {
                            $d = strtotime($v['d']);
                            $m = date('Ym', $d);
                            if ( ! isset($t[ $m ])) {
                                $v['m'] = date('d', $d);
                                $t[ $m ] = $v;
                            } else {
                                $t[ $m ]['c'] += $v['c'];
                                $t[ $m ]['d'] = $v['d'];
                            }
                        }
                        $result = array();
                        foreach($t as $v) {
                            $result[] = $v;
                        }
                    }
                }

                $response['data'] = $result;
                break;
            default:
                $response = false;
        }
        if ($response !== false) {
            if (Request::isAJAX()) $this->ajaxResponseForm($response);
        }

        $data = $this->statistic();
        tpl::includeCSS($this->url('analytics.css'), false);
        tpl::includeJS('d3.v3.min', false);
        return $this->viewPHP($data, 'tpl/admin.result');
    }

    /**
     * Подготовка данных для вывода статистики
     * @return array
     */
    public function statistic()
    {
        $result = array();

        # Объявления
        $counts = array();
        $counts[] = array(
            't'     => $this->langAdmin('Опубликовано'),
            'c'     => '#74b31b',
            'count' => BBS::model()->itemsList(array(
                'is_publicated' => 1,
                'status' => BBS::STATUS_PUBLICATED,
            ), true, array('context' => 'statistic')),
            'url' => $this->adminLink('listing', 'bbs'),
        );

        $counts[] = array(
            't' => $this->langAdmin('На модерации'),
            'c' => '#ffcd33',
            'count' => BBS::model()->itemsList(array(
                'is_moderating' => 1,
            ), true, array('context' => 'statistic')),
            'url' => $this->adminLink('listing&status=3', 'bbs'),
        );

        $counts[] = array(
            't' => $this->langAdmin('Снято с публикации'),
            'c' => '#E5625C',
            'count' => BBS::model()->itemsList(array(
                'is_publicated' => 0,
                'status' => BBS::STATUS_PUBLICATED_OUT,
            ), true, array('context' => 'statistic')),
            'url' => $this->adminLink('listing&status=2', 'bbs'),
        );

        $counts[] = array(
            't' => $this->langAdmin('Не активировано'),
            'c' => '#23b7e5',
            'count' => BBS::model()->itemsList(array(
                'is_publicated' => 0,
                'status' => BBS::STATUS_NOTACTIVATED,
            ), true, array('context' => 'statistic')),
            'url' => $this->adminLink('listing&status=4', 'bbs'),
        );

        $counts[] = array(
            't' => $this->langAdmin('Заблокировано'),
            'c' => '#9b48cd',
            'count' => BBS::model()->itemsList(array(
                'is_publicated' => 0,
                'status' => BBS::STATUS_BLOCKED,
            ), true, array('context' => 'statistic')),
            'url' => $this->adminLink('listing&status=5', 'bbs'),
        );

        $counts[] = array(
            't' => $this->langAdmin('Удалено'),
            'c' => '#e04545',
            'count' => BBS::model()->itemsList(array(
                'is_publicated' => 0,
                'status' => BBS::STATUS_DELETED,
            ), true, array('context' => 'statistic')),
            'url' => $this->adminLink('listing&status=6', 'bbs'),
        );

        $result['bbs'] = array(
            'module' => 'bbs',
            'title' => $this->langAdmin('Объявления'),
            'counts' => $counts,
            'url' => $this->adminLink('listing', 'bbs'),
            'week'=> $this->model->data(static::TYPE_BBS_ITEMS_CREATED, strtotime('-6 days')),
        );

        # Пользователи
        $counts = array();
        $counts[] = array(
            't'     => $this->langAdmin('Всего'),
            'c'     => '#74b31b',
            'count' => Users::model()->usersList(array(), array(), true),
            'url' => $this->adminLink('listing', 'users'),
        );

        $counts[] = array(
            't'     => $this->langAdmin('Активные'),
            'c'     => '#ffcd33',
            'count' => Users::model()->usersList(array(
                ':last' => array('last_login >= :last', ':last' => date('Y-m-d', strtotime('-1 month'))),
            ), array(), true),
            'url' => $this->adminLink('listing&a_from='.date('d-m-Y', strtotime('-1 month')), 'users'),
            'tooltip' => $this->langAdmin('<nobr>Заходили хоть раз</nobr> <div>за 30 последние дней</div>'),
        );

        $result['users'] = array(
            'module' => 'users',
            'title' => $this->langAdmin('Пользователи'),
            'counts' => $counts,
            'url' => $this->adminLink('listing', 'users'),
            'week'=> $this->model->data(static::TYPE_USERS_CREATED, strtotime('-6 days')),
        );

        # Магазины
        if (bff::shopsEnabled()) {
            $counts = array();
            $counts[] = array(
                't'     => $this->langAdmin('Всего'),
                'c'     => '#74b31b',
                'count' => $this->model->shopsCount(),
                'url' => $this->adminLink('listing', 'shops'),
            );

            $counts[] = array(
                't'     => $this->langAdmin('Активные'),
                'c'     => '#ffcd33',
                'count' => $this->model->shopsCount(array(
                    ':last' => array('items_last >= :last', ':last' => date('Y-m-d', strtotime('-1 month'))),
                )),
                'url' => $this->adminLink('listing', 'shops'),
                'tooltip' => $this->langAdmin('<nobr>Есть хотя бы одно объявление</nobr> <nobr>за 30 последние дней</nobr>'),
            );
            $result['shops'] = array(
                'module' => 'shops',
                'title' => $this->langAdmin('Магазины'),
                'counts' => $counts,
                'url' => $this->adminLink('listing', 'shops'),
                'week'=> $this->model->data(static::TYPE_SHOPS_CREATED, strtotime('-6 days')),
            );
        }

        # Место на диске
        $usage = $this->model->data(array(static::TYPE_USAGE_IMAGES, static::TYPE_USAGE_CODE, static::TYPE_USAGE_CACHE, static::TYPE_USAGE_DB), 0,
            array('type as t', 'value as s', 'dte AS d'), false);
        $usage = func::array_transparent($usage, 't', true);
        $result['usage'] = $usage;

        $counts = array();
        $counts[] = array(
            't' => $this->langAdmin('Изображения'),
            'c' => '#74b31b',
            's' => isset($usage[static::TYPE_USAGE_IMAGES]) ? $usage[static::TYPE_USAGE_IMAGES]['s'] : 0,
        );
        $counts[] = array(
            't' => $this->langAdmin('БД'),
            'c' => '#23b7e5',
            's' => isset($usage[static::TYPE_USAGE_DB]) ? $usage[static::TYPE_USAGE_DB]['s'] : 0,
        );
        $counts[] = array(
            't' => $this->langAdmin('Кеш'),
            'c' => '#f08c17',
            's' => isset($usage[static::TYPE_USAGE_CACHE]) ? $usage[static::TYPE_USAGE_CACHE]['s'] : 0,
        );
        $counts[] = array(
            't' => $this->langAdmin('Код сайта'),
            'c' => '#e04545',
            's' => isset($usage[static::TYPE_USAGE_CODE]) ? $usage[static::TYPE_USAGE_CODE]['s'] : 0,
        );
        $used = 0;
        foreach ($counts as $v) {
            $used += $v['s'];
        }

        $free = disk_free_space(PATH_BASE);
        $total = disk_total_space(PATH_BASE);
        $summary = $used + $free;

        $counts[] = array(
            't' => $this->langAdmin('Другое (ОС)'),
            'c' => '#bcb4c4',
            's' => $total - $summary,
        );


        $result['size'] = array(
            'legend'    => $counts,
            'free'      => $free,
            'used'      => $used,
            'total'     => $total,
            'summary'   => $summary,
        );
        $bar = array();
        foreach ($counts as $v) {
            $bar[] = array(
                'c' => $v['c'],
                'p' => str_replace(',', '.', round(($v['s'] / $total) * 100, 2)),
            );
        }
        $result['size']['bar'] = $bar;

        $result['updated_drive'] = $this->config('updated_drive');

        $demo = config::sysAdmin('plugin.statistic_do2.demo', false);
        if (is_callable($demo)) {
            return $demo($result);
        }

        return $result;
    }

    /**
     * Расчет статистики используемого места на диске
     */
    public function cronCalculateDriveSize()
    {
        if ( ! bff::cron()) {
            return;
        }

        $result = array(
            static::TYPE_USAGE_IMAGES => 0,
            static::TYPE_USAGE_CACHE => 0,
            static::TYPE_USAGE_CODE => 0,
        );
        $prefix = array(
            static::TYPE_USAGE_IMAGES => array(
                'files'.DIRECTORY_SEPARATOR.'images',
            ),
            static::TYPE_USAGE_CACHE => array(
                'files'.DIRECTORY_SEPARATOR.'cache',
                'files'.DIRECTORY_SEPARATOR.'logs',
                'files'.DIRECTORY_SEPARATOR.'min',
            ),
        );

        $calcRecursive = function($directory) use( & $calcRecursive, & $result, & $prefix) {
            $directory = rtrim($directory, '\\/');
            $dir = opendir($directory);
            if ( ! $dir) return;
            while (($file = readdir($dir)) !== false) {
                if ($file === '.' || $file === '..') {
                    continue;
                }
                $full = $directory.DIRECTORY_SEPARATOR.$file;
                if (is_file($full)) {
                    $size = filesize($full);
                    foreach($prefix as $k => $v) {
                        foreach($v as $vv) {
                            if (mb_strpos($full, $vv)) {
                                $result[$k] += $size;
                                continue 3;
                            }
                        }
                    }
                    $result[static::TYPE_USAGE_CODE] += $size;
                } else if (is_dir($full)){
                    $calcRecursive($full);
                }
            }
            closedir($dir);
        };
        $calcRecursive(PATH_BASE);

        foreach($result as $k => $v) {
            $this->model->save($k, 0, $v);
        }
        $this->model->save(static::TYPE_USAGE_DB, 0, $this->model->dbUsage());
        $this->configUpdate('updated_drive', time());
    }

    /**
     * Пересчет статистики при включении плагина
     */
    public function cronOnEnabled()
    {
        if ( ! bff::cron()) {
            return;
        }

        $year = strtotime('-1 year');
        $data = $this->model->data(static::TYPE_BBS_ITEMS_CREATED, $year, array(), false);
        if (empty($data)) {
            $this->model->fillCreated(static::TYPE_BBS_ITEMS_CREATED, TABLE_BBS_ITEMS, array('import'), function($row){
                if ( ! empty($row['import']) && $this->isImportAdmin($row['import'])) return false;
                return true;
            });
        }

        $data = $this->model->data(static::TYPE_USERS_CREATED, $year, array(), false);
        if (empty($data)) {
            $this->model->fillCreated(static::TYPE_USERS_CREATED, TABLE_USERS, array('fake'), function($row){
                if ( ! empty($row['fake']) && $this->config('users_fake_ignore')) return false;
                return true;
            });
        }

        if (bff::shopsEnabled()) {
            Shops::i();
            $data = $this->model->data(static::TYPE_SHOPS_CREATED, $year, array(), false);
            if (empty($data)){
                $this->model->fillCreated(static::TYPE_SHOPS_CREATED, TABLE_SHOPS);
            }
        }

        $this->cronCalculateDriveSize();

        $this->configUpdate('updated_drive', time());
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {
        return array(
            'cronCalculateDriveSize' => array('period' => $this->config('calculate_drive_size')),
        );
    }

}