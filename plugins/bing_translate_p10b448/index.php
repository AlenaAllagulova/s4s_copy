<?php

class Plugin_Bing_translate_p10b448 extends Plugin
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'extension_id'   => 'p10b448e35a795a3fd43aef696af1b48c7bd23d8',
            'plugin_title'   => 'Bing Translate',
            'plugin_version' => '1.0.0',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            'api_key' => array(
                'title' => 'Api ключ',
                'input' => 'password',
                'description' => '32-х значный циферно-буквенный ключ (например: 0838465C81BE484DB9A9E681574DEAEB), 
                                  который можно найти в личном кабинете системы'
            ),
        ));
    }

    protected function start()
    {
        BbsHooks::itemTranslateProvidersList(function ($list){
            $list['bing'] = ['title'=> 'Bing Translate'];
            return $list;
        });

        BbsHooks::itemTranslateProvider(function($data, $lang, $languages, $provider){

            if (empty($provider) || ! method_exists($this, $provider)) {
                bff::log('provider not found "'.$provider.'"');
                return false;
            }

            return  $this->bing($data, $lang, $languages);
        });

    }


    public function bing($data, $lang, $languages = array())
    {
        $key = $this->config('api_key');

        if (empty($key)) {
            bff::log('bing-translate.key "'.$key.'" not found');
            return false;
        }
        if (empty($languages)) {
            $languages = $this->locale->getLanguages();
        }
        $k = array_search($lang, $languages);
        if ($k !== false) {
            unset($languages[$k]);
        }
        if (empty($languages)) {
            bff::log('locale set error');
            return false;
        }


        $host = "https://api.cognitive.microsofttranslator.com";
        $path = "/translate?api-version=3.0";

        // Translate to langs, example &to=ru&to=it.
        $params = "&to=ru&to=it";
        $params = "";
        foreach ($languages as $lng_title){
            $params .= '&to='.$lng_title;
        }

        $fields = array();
        $result = array();
        foreach ($data as $field => $item_text) {
            $fields[] = $field;

            $requestBody = [
                [
                    'text' => $item_text,
                ],
            ];
            $content = json_encode($requestBody);

            $resp = $this->bing_translate($host, $path, $key, $params, $content);

            if(!$resp){
                bff::log('bing translate error');
                return $result;
            }else{
                // Note: We convert result, which is JSON, to and from an object so we can pretty-print it.
                // We want to avoid escaping any Unicode characters that result contains. See:
                // http://php.net/manual/en/function.json-encode.php
                $json = json_encode(json_decode($resp), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
                $decoded = json_decode($json, true);
                if(is_array($decoded)){
                    $decoded = reset($decoded);
                }else{
                    bff::log('bing data translations not found');
                    return $result;
                }

                if (isset($decoded['translations'])) {
                    foreach ($decoded['translations'] as $lng_item) {

                        if (!isset($lng_item['to']) || !in_array($lng_item['to'], $languages)) {
                            continue;
                        }
                        if (!isset($lng_item['text'])) continue;

                        $result[$lng_item['to']][$field] = $lng_item['text'];
                    }
                } else {
                    bff::log('bing data translations not found');
                }
            }
        }
        return $result;

    }

    public function com_create_guid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
            mt_rand( 0, 0xffff ),
            mt_rand( 0, 0x0fff ) | 0x4000,
            mt_rand( 0, 0x3fff ) | 0x8000,
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    public function bing_translate($host, $path, $key, $params, $content) {

        $headers = "Content-type: application/json\r\n" .
            "Content-length: " . strlen($content) . "\r\n" .
            "Ocp-Apim-Subscription-Key: $key\r\n" .
            "X-ClientTraceId: " . $this->com_create_guid() . "\r\n";

        // NOTE: Use the key 'http' even if you are making an HTTPS request. See:
        // http://php.net/manual/en/function.stream-context-create.php
        $options = array (
            'http' => array (
                'header' => $headers,
                'method' => 'POST',
                'content' => $content
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($host . $path . $params, false, $context);
        return $result;
    }

}