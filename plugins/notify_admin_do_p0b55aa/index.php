<?php

class Plugin_Notify_Admin_Do_p0b55aa extends Plugin
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title'   => 'Уведомления администратору',
            'plugin_version' => '1.0.0',
            'extension_id'   => 'p0b55aa7fdc13b77f88d544489c2fd2795cdf6cb',
        ));

        $adminEmail = config::sysAdmin('mail.admin', 'admin@'.SITEHOST);
        $macrosTip = function(array $list) {
            $template = '<div class="well well-small">'.$this->langAdmin('Доступные макросы:<br/>[macros_list]', array('items_macros'=>'<code>{items}</code>')).'</div>';
            $macrosAvailable = array();
            foreach ($list as $k=>$v) {
                if ($v === 'host') {
                    $macrosAvailable[] = $this->langAdmin('[host_macros] - домен проекта', array('host_macros'=>'<code>{host}</code>'));
                } else if (is_string($k) && is_string($v)) {
                    $macrosAvailable[] = '<code>{'.$k.'}</code> - '.$v;
                }
            }
            if (empty($macrosAvailable)) {
                return '';
            }
            return strtr($template, array('[macros_list]'=>join('<br />', $macrosAvailable)));
        };

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
                # Объявления
                'itemMailTo' => array(
                    'title' => $this->langAdmin('Email получателя'),
                    'input' => 'text',
                    'placeholder' => $adminEmail,
                    'default' => $adminEmail,
                    'tab' => 'items',
                ),

                'D1' => array(
                    'input' => 'divider',
                    'tab' => 'items',
                ),

                'itemCreate' => array(
                    'title' => '<b>'.$this->langAdmin('Новые объявления').'</b>',
                    'input' => 'checkbox',
                    'type'  => TYPE_BOOL,
                    'default' => true,
                    'description' => $this->langAdmin('отправлять уведомления о новых объявлениях ожидающих модерации'),
                    'tab' => 'items',
                ),
                'itemCheck' => array(
                    'title' => $this->langAdmin('Период отправки'),
                    'input' => 'select',
                    'options' => array(
                        '* * * * *'     => array('title'=>'1 минута'),
                        '*/5 * * * *'   => array('title'=>'5 минут'),
                        '*/15 * * * *'  => array('title'=>'15 минут'),
                        '*/30 * * * *'  => array('title'=>'30 минут'),
                        '0 * * * *'     => array('title'=>'1 час'),
                        '0 */3 * * *'   => array('title'=>'3 часа'),
                        '0 */6 * * *'   => array('title'=>'6 часов'),
                        '0 */12 * * *'  => array('title'=>'12 часов'),
                        '0 0 * * *'     => array('title'=>'24 часа')
                    ),
                    'default' => '0 * * * *',
                    'tab' => 'items',
                ),
                'itemCreateImmediately' => array(
                    'input' => 'checkbox',
                    'type'  => TYPE_BOOL,
                    'default' => false,
                    'description' => $this->langAdmin('немедленно'),
                    'tab' => 'items',
                ),
                'itemCreateMailSubject' => array(
                    'title' => $this->langAdmin('Тема письма'),
                    'input' => 'text',
                    'default' => $this->langAdmin('Появились новые объявления ({items})'),
                    'tab' => 'items',
                ),
                'itemCreateMailBody' => array(
                    'title' => $this->langAdmin('Сообщение'),
                    'input' => 'textarea',
                    'default' => $this->langAdmin('На сайте {host} {items} ожидают модерации.'),
                    'description' => $macrosTip(array(
                        'items' => $this->langAdmin('количество объявлений на модерации'),
                        'host',
                    )),
                    'tab' => 'items',
                ),
                'itemsPrevСounter' => array(
                    'input' => 'temp',
                    'type' => TYPE_UINT,
                ),

                # Пользователи
                'userMailTo' => array(
                    'title' => $this->langAdmin('Email получателя'),
                    'input' => 'text',
                    'placeholder' => $adminEmail,
                    'default' => $adminEmail,
                    'tab' => 'users',
                ),

                'D2' => array(
                    'input' => 'divider',
                    'tab' => 'users',
                ),

                'userRegister' => array(
                    'title' => '<b>'.$this->langAdmin('Регистрация').'</b>',
                    'input' => 'checkbox',
                    'type'  => TYPE_BOOL,
                    'default' => false,
                    'description' => $this->langAdmin('отправлять уведомления о регистрации нового пользователя'),
                    'tab' => 'users',
                ),
                'userRegisterMailSubject' => array(
                    'title' => $this->langAdmin('Тема письма'),
                    'input' => 'text',
                    'default' => $this->langAdmin('Регистрация нового пользователя'),
                    'tab' => 'users',
                ),
                'userRegisterMailBody' => array(
                    'title' => $this->langAdmin('Сообщение'),
                    'input' => 'textarea',
                    'default' => $this->langAdmin('На сайте {host} зарегистрирован новый пользователь!'),
                    'description' => $macrosTip(array(
                        'host',
                        'email' => $this->langAdmin('E-mail пользователя'),
                    )),
                    'tab' => 'users',
                ),

                'D3' => array(
                    'input' => 'divider',
                    'tab' => 'users',
                ),

                'userActivate' => array(
                    'title' => '<b>'.$this->langAdmin('Активация').'</b>',
                    'input' => 'checkbox',
                    'type'  => TYPE_BOOL,
                    'default' => false,
                    'description' => $this->langAdmin('отправлять уведомления об активации нового пользователя'),
                    'tab' => 'users',
                ),
                'userActivateMailSubject' => array(
                    'title' => $this->langAdmin('Тема письма'),
                    'input' => 'text',
                    'default' => $this->langAdmin('Активация нового пользователя'),
                    'tab' => 'users',
                ),
                'userActivateMailBody' => array(
                    'title' => $this->langAdmin('Сообщение'),
                    'input' => 'textarea',
                    'default' => $this->langAdmin('Новый пользователь активировал свой аккаунт!'),
                    'description' => $macrosTip(array(
                        'host',
                    )),
                    'tab' => 'users',
                ),

                # Админ
                'adminMailTo' => array(
                    'title' => $this->langAdmin('Email получателя'),
                    'input' => 'text',
                    'placeholder' => $adminEmail,
                    'default' => $adminEmail,
                    'tab' => 'admin',
                ),

                'D4' => array(
                    'input' => 'divider',
                    'tab' => 'admin',
                ),

                'adminLogin' => array(
                    'title' => '<b>'.$this->langAdmin('Авторизация').'</b>',
                    'input' => 'checkbox',
                    'type'  => TYPE_BOOL,
                    'default' => false,
                    'description' => $this->langAdmin('отправлять уведомления о входе в панель администратора'),
                    'tab' => 'admin',
                ),
                'adminMailSubject' => array(
                    'title' => $this->langAdmin('Тема письма'),
                    'input' => 'text',
                    'default' => $this->langAdmin('Вход в панель администратора {host} ({ip})'),
                    'tab' => 'admin',
                ),
                'adminMailBody' => array(
                    'title' => $this->langAdmin('Сообщение'),
                    'input' => 'textarea',
                    'default' => $this->langAdmin("По соображениям безопасности мы информируем вас о каждой авторизации в панель администратора.\nЕсли вы получили это уведомление, но не входили в панель администратора, срочно примите меры! {userinfo}"),
                    'description' => $macrosTip(array(
                        'ip' => $this->langAdmin('IP адрес пользователя'),
                        'userinfo' => $this->langAdmin('информация о пользователе'),
                        'host',
                    )),
                    'tab' => 'admin',
                ),
            ), 
            array(
                'tabs' => array(
                    'items' => array('title' => $this->langAdmin('Объявления')),
                    'users' => array('title' => $this->langAdmin('Пользователи')),
                    'admin' => array('title' => $this->langAdmin('Панель администратора')),
                ),
                'titleRow' => 130,
            )
        );
    }

    protected function start()
    {
        # Объявления (немедленно)
        if ($this->config('itemCreate') && $this->config('itemMailTo')  && $this->config('itemCreateImmediately')) {
            bff::hookAdd('bbs.item.status.new.created', function($itemID, $itemData) {
                $total = BBS::model()->itemsModeratingCounter();
                $this->itemCreateMail($total);
            });
            bff::hookAdd('bbs.item.status.new.activated', function($itemID, $itemData) {
                $total = BBS::model()->itemsModeratingCounter();
                $this->itemCreateMail($total);
            });
        }

        # Пользователи: Регистрация
        if ($this->config('userRegister') && $this->config('userMailTo')) {
            bff::hookAdd('users.user.register', function($userID, $userData){
                $userEmail = ( ! empty($userData['email']) ? $userData['email'] : '?');
                $subject = strtr($this->config('userRegisterMailSubject'), array(
                    '{host}' => SITEHOST,
                    '{email}' => $userEmail,
                ));
                $body = strtr($this->config('userRegisterMailBody'), array(
                    '{host}' => SITEHOST,
                    '{email}' => $userEmail,
                ));
                if ( ! empty($body)) {
                    bff::sendMail($this->config('userMailTo'), $subject, $body);
                }
            });
        }
        # Пользователи: Активация
        if ($this->config('userActivate') && $this->config('userMailTo')) {
            bff::hookAdd('users.user.activated', function(){
                $subject = strtr($this->config('userActivateMailSubject'), array(
                    '{host}' => SITEHOST,
                ));
                $body = strtr($this->config('userActivateMailBody'), array(
                    '{host}' => SITEHOST,
                ));
                if ( ! empty($body)) {
                    bff::sendMail($this->config('userMailTo'), $subject, $body);
                }
            });
        }

        # Панель администратора: Авторизация
        if ($this->config('adminLogin') && $this->config('adminMailTo')) {
            bff::hookAdd('users.admin.login.step2', function(){
                if ( ! $this->errors->no()) return;

                $userIP = \Request::remoteAddress();

                # Формируем массив с данными о пользователе по IP
                $userInfo = \bff\utils\Files::downloadFile('https://ipapi.co/' . $userIP . '/json/', false);
                if ( ! empty($userInfo)) {
                    $userInfo = json_decode($userInfo, true);
                }
                $userInfo = array_merge(array('ip'=>'', 'country'=>'', 'country_name'=>'', 'city'=>'', 'org'=>''), is_array($userInfo) ? $userInfo : array());

                # Макросы
                $macrosUserInfo = array();
                $macrosUserInfo['IP'] = $userIP;
                if ( ! empty($userInfo['country_name'])) {
                    $macrosUserInfo[$this->langAdmin('Страна')] = $userInfo['country_name'] . ' (' . $userInfo['country'] . ')';
                }
                if ( ! empty($userInfo['city'])) {
                    $macrosUserInfo[$this->langAdmin('Город')] = $userInfo['city'];
                }
                if ( ! empty($userInfo['org'])) {
                    $macrosUserInfo[$this->langAdmin('Провайдер')] = $userInfo['org'];
                }
                $macrosUserInfo[$this->langAdmin('Браузер')] = \Request::userAgent();
                $macrosUserInfoHTML = '<p>';
                foreach ($macrosUserInfo as $k=>$v) {
                    $macrosUserInfoHTML .= '<b>'.$k.'</b>: '.$v.'<br />';
                }
                $macrosUserInfoHTML.= '</p>';

                $subject = strtr($this->config('adminMailSubject'), array(
                    '{ip}' => $userIP,
                    '{host}' => SITEHOST,
                ));
                $body = strtr($this->config('adminMailBody'), array(
                    '{ip}' => $userIP,
                    '{userinfo}' => $macrosUserInfoHTML,
                    '{host}' => SITEHOST,
                ));
                
                # Отправка письма
                if ( ! empty($body)) {
                    bff::sendMail($this->config('adminMailTo'), $subject, $body);
                }
            });
        }
    }

    /**
     * Отправка сообщения о новых объявлениях на модерации
     */
    public function cronItemCreate()
    {
        if ($this->config('itemCreate') && $this->config('itemMailTo')  && ! $this->config('itemCreateImmediately'))
        {
            # Количество объявлений на модерации:
            $total = BBS::model()->itemsModeratingCounter();

            if ($this->config('itemsPrevСounter') != $total && $total > 0)
            {
                $this->itemCreateMail($total);
                $this->configUpdate('itemsPrevСounter', $total); # Помечаем последний счетчик кол-ва
            }
        }
    }

    protected function itemCreateMail($total)
    {
        if ( ! $total) {
            return;
        }

        # Ссылка на страницу списка объявлений на модерации в админ. панели
        $link = SITEURL.\tpl::adminLink('listing&status=3', 'bbs');

        # Тема письма
        $subject = strtr($this->config('itemCreateMailSubject'), array(
            '{items}' => $total,
            '{host}'  => SITEHOST,
        ));
        # Тело письма
        $body = strtr($this->config('itemCreateMailBody'), array(
            '{items}' => '<a href="'.$link.'">'.tpl::declension($total, $this->langAdmin('объявление;объявления;объявлений')).'</a>',
            '{host}'  => SITEHOST,
        ));
        if ( ! empty($body)) {
            # Отправка письма
            bff::sendMail($this->config('itemMailTo'), $subject, $body);
        }
    }

    /**
     * Отправка напоминания
     */
    public function reminderItemCreate()
    {
        $this->configUpdate('itemsPrevСounter', 0); # Обнуление счетчика
    }

    /**
     * Расписание запуска крон задач
     * @return array
     */
    public function cronSettings()
    {
        $settings = array(
            'reminderItemCreate' => array('period' => '0 0 * * *'),
        );
        if ( ! $this->config('itemCreateImmediately')) {
            $settings['cronItemCreate'] = array('period' => $this->config('itemCheck'));
        }
        return $settings;
    }
}