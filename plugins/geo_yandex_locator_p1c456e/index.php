<?php

use bff\extend\Hooks;

class Plugin_Geo_yandex_locator_p1c456e extends Plugin
{
    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'extension_id'   => 'p1c456e066db171db68bdfc7046dcda7cfc59b56',
            'plugin_title'   => 'Geo Yandex Locator',
            'plugin_version' => '1.0.0',
        ));

        /**
         * Настройки заполняемые в админ. панели
         */
        $this->configSettings(array(
            'api_key' => array(
                'title' => 'Api ключ',
                'input' => 'password',
                'description' => 'циферно-буквенный ключ (например: ACG9uVsBAAAAWJ9MXAMA7CszD9QfDRLJHUAueKV6xG8K_-oAAAAAAAAAAABY5kGoMVFg4M4Q1V36LCFL1gSuEg==), 
                                  который можно найти в личном кабинете системы'
            ),
        ));
    }

    protected function start()
    {
        GeoHooks::ipLocationProvidersList(function ($list){
            $list['yandex_locator'] = ['title'=> 'Yandex Locator'];
            return $list;
        });

        bff::hooks()->geoIpLocationRegion(function($data, $lang, $provider){

            $key = $this->config('api_key');

            if (empty($key)) {
                bff::log('Yandex locator key "'.$key.'" not found');
                return Geo::model()->regionData(array('id' => Geo::defaultCountry()));
            }

            $curl = curl_init('http://api.lbs.yandex.net/geolocation');


            $data = [
                'common' => [
                    'version' => '1.0',
                    'api_key' => $key,
                ],
                'ip' => [
                    'address_v4' => $_SERVER['REMOTE_ADDR'],
                ],
            ];

            $value = 'json=' . json_encode($data);

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $value);


            $data = curl_exec($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            $curlError = curl_error($curl);

            if (!empty($curlError)) {
                bff::log($curlError);
                return Geo::model()->regionData(array('id' => Geo::defaultCountry()));
            }

            if ($code != 200) {
                bff::log('Yandex locator is temporary unavailable');
                return Geo::model()->regionData(array('id' => Geo::defaultCountry()));
            }

            $curlAnswer = json_decode($data, true);

            if (!empty($curlAnswer['error'])) {
                bff::log('Yandex locator: '.$curlAnswer['error']['message']);
                return Geo::model()->regionData(array('id' => Geo::defaultCountry()));
            }


            if (empty($curlAnswer['position']['latitude']) && empty($curlAnswer['position']['longitude'])) {
                bff::log('Yandex locator: location is not detected');
                return Geo::model()->regionData(array('id' => Geo::defaultCountry()));
            }

            $aRegionsYcoords = Geo::model()->db->select('
                SELECT id, ycoords 
                FROM '.TABLE_REGIONS.' 
                WHERE ycoords IS NOT NULL AND ycoords != ""');

            $aRegionsYcoordsFormated = [];
            foreach ($aRegionsYcoords as $reg) {
                list($lat, $lon) = explode(',', $reg['ycoords']);

                $aRegionsYcoordsFormated[$reg['id']] = [
                    'latitude' => $lat,
                    'longitude' => $lon,
                ];
            }

            $searchData = [
                'latitude' => (string) $curlAnswer['position']['latitude'],
                'longitude' => (string) $curlAnswer['position']['longitude'],
            ];

            $findRegion = $this->recurs($aRegionsYcoordsFormated, 0, $searchData);

            if (empty($findRegion)) {
                return Geo::model()->regionData(array('id' => Geo::defaultCountry()));
            }

            return  Geo::model()->regionData(array('id' => array_keys($findRegion)[0]));
        });
    }

    private function recurs($aRegionsYcoords, $number, $search)
    {
        $newRegionsYcoords = [];

        $searchLatitude = $search['latitude'];
        $searchLongitude = $search['longitude'];

        foreach ($aRegionsYcoords as $regId => $regData) {


            $regionLatitude = (string)$regData['latitude'];
            $regionLongitude = (string)$regData['longitude'];

            if ($searchLatitude{$number} == $regionLatitude{$number} && $searchLongitude{$number} == $regionLongitude{$number}) {
                $newRegionsYcoords[$regId] = $regData;
            }
        }

        if (count($newRegionsYcoords) == 1 || empty($newRegionsYcoords)) {
            return $newRegionsYcoords;
        }

        if ($number == 1) {
            $number += 2;
        } else {
            $number++;
        }

        return $this->recurs($newRegionsYcoords, $number, $search);
    }

}