<?php
    /**
     * Предложить свою цену: форма для тем Start + Nelson
     * @var $this Plugin_BBS_Item_Price_Suggest_p090994
     * @var $data array данные об объявлении
     */
     $this->css('css/style.start.css');
     $this->js('js/form.js');
     $captcha = false;
?>
<div class="ad-author-in rel bbs-item-suggest-price">
    <a href="javascript:void(0);" class="btn btn-block btn-info j-v-<?= $this->jsPrefix(); ?>-link"><?= $this->lang('Предложите свою цену') ?></a>
    <div class="dropdown-menu ad-top-func-dropdown v-popup j-v-<?= $this->jsPrefix(); ?>-popup" style="display:none;">
        <div class="dropdown-menu-heading">
            <div class="dropdown-menu-heading-title"><?= $this->lang('Предложите свою цену') ?></div>
        </div>
        <div class="dropdown-menu-in">
            <form action="">
                <input type="hidden" name="item_id" value="<?= $data['id'] ?>" />
                <div class="form-group">
                    <input type="text" name="price" class="form-control input-inline price-input j-required" placeholder="<?= $this->lang('Ваша цена'); ?>" maxlength="10" />
                    <select name="price_currency" class="form-control input-inline"><?= Site::currencyOptions($data['price_curr']); ?></select>
                </div>
                <div class="form-group">
                    <input type="tel" name="phone_number" class="form-control input-inline j-required j-phone" placeholder="<?= $this->lang('Ваш номер телефона'); ?>" maxlength="14" />
                </div>
                <? if($this->captcha()): if (Site::captchaCustom($this->getName())): ?>
                    <div class="form-group">
                        <? bff::hook('captcha.custom.view', $this->getName(), __FILE__) ?>
                    </div>
                <? else: $captcha = true; ?>
                <div class="form-group">
                    <div><p><?= _t('', 'Введите результат с картинки') ?></p></div>
                    <input type="text" name="captcha" class="input-small j-required" value="" pattern="[0-9]*" /> <img src="" alt="" class="j-captcha" data-url="<?= tpl::captchaURL('math', array('key'=>$this->captchaCookieKey(), 'bg'=>'FFFFFF', 'rnd'=>'')) ?>" />
                </div>
                <? endif; endif; ?>
                <div>
                    <button type="submit" class="btn btn-info send-button j-submit">
                        <?= $this->lang('Предложить'); ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
    <? js::start(); ?>
        $(function(){
            jPluginBBSItemPriceSuggest.init(<?= func::php2js(array(
                'prefix' => $this->jsPrefix(),
                'plugin_name' => $this->getName(),
                'captcha' => $captcha,
            )) ?>);
        });
    <? js::stop(); ?>
    </script>
</div>
