1. Установите и Включите плагин в [списке плагинов](?s=site&ev=extensionsManager&type=1).

2. Заполните шаблон уведомления ["Объявления: предложение покупателем своей цены"](?s=sendmail&ev=template_edit&tpl=plugin_bbs_item_price_suggest&group=bbs).

{theme:file:/modules/bbs/tpl/def/item.view.php}
3. Добавьте в файл шаблона темы **{theme:file:path}** следующий код:
```
<?php bff::hook('plugin_bbs_item_price_suggest_block_desktop', array('data'=>&$aData)); ?>
```
Рекомендуемое расположение - перед строкой: `<?= $this->viewPHP($aData, 'item.view.owner') ?>`
{/theme:file}

{theme:file:/modules/bbs/tpl/def/item.view.php}
4. Для мобильной версии добавьте в файл шаблона темы **{theme:file:path}** следующий код:
```
<?php bff::hook('plugin_bbs_item_price_suggest_block_mobile', array('data'=>&$aData)); ?>
```
Рекомендуемое расположение - перед строкой: `<div id="j-view-contact-mobile-block">`
{/theme:file}
