<?php

class Plugin_Captcha_recaptcha_p00d266 extends Plugin
{
    protected $pages = array();

    public function init()
    {
        parent::init();

        $this->setSettings(array(
            'plugin_title'   => 'Google reCAPTCHA',
            'plugin_version' => '1.0.2',
            'plugin_alias'   => 'captcha_recaptcha',
            'extension_id'   => 'p00d2665b67b002f29801f7c6f33c0a9a11e199c',
        ));

        switch (BFF_PRODUCT) {
            case 'freelance':
                $this->pages = array(
                    'users-auth-register' => array(
                        't'=>$this->langAdmin('Регистрация пользователя'),
                    ),
                    'contacts-write' => array(
                        't'=>$this->langAdmin('Связаться с администрацией'),
                    ),
                );
                break;
            case 'do':
                $this->pages = array(
                    'users-auth-register' => array(
                        't'=>$this->langAdmin('Регистрация пользователя'),
                        'c'=>config::sysAdmin('users.register.captcha', false, TYPE_BOOL),
                    ),
                    'bbs-item-view' => array(
                        't'=>$this->langAdmin('Просмотр объявления: блок пожаловаться'),
                    ),
                    'users-write-form' => array(
                        't'=>$this->langAdmin('Просмотр объявления: блок связаться с автором'),
                        'c'=>config::sysAdmin('users.write.form.captcha', false, TYPE_BOOL),
                    ),
                    'shops-view' => array(
                        't'=>$this->langAdmin('Просмотр магазина: блок пожаловаться'),
                    ),
                    'contacts-write' => array(
                        't'=>$this->langAdmin('Связаться с администрацией'),
                        'c'=>config::sysAdmin('contacts.captcha', true, TYPE_BOOL),
                    ),
                );
                if ( ! bff::moduleExists('shops')) {
                    unset($this->pages['shops-view']);
                }
                break;
        }

        /**
         * Настройки заполняемые в админ. панели
         */
        $settings = array(
            'site_key' => array(
                'title' => $this->langAdmin('Site key'),
                'input' => 'text',
                'placeholder' => $this->langAdmin('пример: [code]', array('code'=>'6LcDeVMUAAAAANUH-iChaCkARYwzjUpW5fwNVG5y')),
            ),
            'secret_key' => array(
                'title' => $this->langAdmin('Secret key'),
                'input' => 'text',
                'placeholder' => $this->langAdmin('пример: [code]', array('code'=>'6LcDeVMUAAAAANRt4_agG0M0nzebzA1E1QRN4hjl')),
            ),
            'captcha_type' => array(
                'title' => $this->langAdmin('Тип'),
                'input' => 'select',
                'type'  => TYPE_STR,
                'default' => 'v2',
                'options' => array(
                    'v2' => array('title' => 'reCAPTCHA v2'),
                    'invisible' => array('title' => 'Invisible reCAPTCHA'),
                ),
            ),
        );
        # Страницы:
        $settings['D1'] = array(
            'input' => 'divider',
        );
        $i = 1;
        foreach ($this->pages as $k => $page) {
            $settings['page_'.$k] = array(
                'title' => ($i == 1 ? $this->langAdmin('Страницы') : ''),
                'input' => 'checkbox',
                'description' => (isset($page['c']) ? $page['t'] . ( ! $page['c'] ?  ' ('.$this->langAdmin('включите капчу в настройках').')' : '' ) : $page['t']),
            ); $i++;
        }
        $this->configSettings($settings, array('titleRow'=>120));
    }

    protected function start()
    {
        $this->pages = bff::filter('captcha.recaptcha.pages', $this->pages, $this);

        bff::hookAdd('captcha.custom.active', array($this, 'active'));
        bff::hookAdd('captcha.custom.view', function($page, $file, $opts = array()){
            echo $this->view($page, $file, $opts);
        });
        bff::hookAdd('captcha.custom.check', function(){
            if ( ! $this->errors->no()) return;
            if ( ! $this->check()) {
                $this->errors->set($this->lang('Подтвердите, пожалуйста, что Вы не робот'));
            }
        });
    }

    /**
     * Проверка, включена ли капча для указанной страницы
     * @param string $page keyword страницы
     * @return bool
     */
    public function active($page = '')
    {
        # Проверка настроек плагина
        $siteKey = $this->config('site_key');
        $secretKey = $this->config('secret_key');
        if (empty($siteKey) || empty($secretKey)) {
            if (empty($siteKey)) $this->log($this->langAdmin('Не указан Site key'));
            if (empty($secretKey)) $this->log($this->langAdmin('Не указан Secret key'));
            return false;
        }
        # Включена ли страница
        if (isset($this->pages[$page]) && ! $this->config('page_'.$page)) {
            return false;
        }
        return true;
    }

    /**
     * Вывод капчи
     * @param string $page keyword страницы
     * @param string $file путь к файлу
     * @param array $opts параметры ['wrap' => '<div class="controls">{form}</div>'] обвертка для формы
     * @return string HTML
     */
    public function view($page = '', $file = '', array $opts = array())
    {
        $invisible = ($this->config('captcha_type') === 'invisible');
        $captchaHTML = '<div class="j-google-recaptcha" style="margin-bottom: 5px;" '.($invisible ? ' data-size="invisible" data-callback="jReCaptchaOnExecuted"' : '').'></div>';

        if ( ! empty($opts['wrap']) && mb_strpos($opts['wrap'], '{form}')) {
            $captchaHTML = str_replace('{form}', $captchaHTML, $opts['wrap']);
        } else {
            $data = array(
                'page'        => $page,
                'captchaHTML' => $captchaHTML
            );
            $captchaHTML = $this->viewPHP($data, 'tpl/form');
        }

        static $inited;
        if ($inited) {
            return $captchaHTML;
        }
        $inited = true;
        tpl::includeJS('https://www.google.com/recaptcha/api.js?onload=onloadReCaptcha&render=explicit');
        $data = array(
            'captchaHTML' => $captchaHTML,
            'siteKey' => $this->config('site_key'),
            'invisible' => $invisible,
        );
        return $this->viewPHP($data, 'tpl/view');
    }

    /**
     * Проверка капчи
     * @return boolean true - проверка пройдена, false - не прошли проверку
     */
    public function check()
    {
        $secretKey = $this->config('secret_key');
        $response = $this->input->post('g-recaptcha-response', TYPE_STR);
        do {
            if (empty($response)) break;

            $data = array(
                'secret'   => $secretKey,
                'response' => $response
            );

            $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($data));

            $status = curl_exec($handle);
            curl_close($handle);
            if (empty($status)) break;

            $status = json_decode($status, true);
            if (empty($status)) break;

            if ( ! empty($status['error-codes'])) {
                $this->log($status);
            }

            if (empty($status['success'])) break;

            return true;

        } while(false);

        return false;
    }

}